/* cache.c - APT2 cache.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include <apt/cache.h>
#include "cache-internal.h"

/* For apt_package_file_is_up_to_date() */
#include <errno.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib/gi18n-lib.h>

/**
 * SECTION:cache
 * @short_description: Access to the package cache.
 * @title: AptCache
 * @include: apt.h
 * @stability: Unstable
 *
 * The cache provides access to all packages known to APT and information
 * about them.
 **/

#define cache_at_offset(c, o) ((gpointer)((char*) (c)->header + (o)))

struct _AptPackage {
    AptCache *cache;
    AptCachePackage *package;
    gint ref_count;
};

struct _AptDependency {
    AptCache *cache;
    AptCachePackage *package;
    AptCacheDependency *dependency;
    gint ref_count;
};

struct _AptProvides {
    AptCache *cache;
    AptCachePackage *package;
    AptCacheProvides *provides;
    gint ref_count;
};
struct _AptDescription {
    AptCache *cache;
    AptCachePackage *package;
    AptCacheDescription *description;
    gint ref_count;
};
struct _AptPackageFile {
    AptCache *cache;
    AptCachePackage *package;
    AptCachePackageFile *packagefile;
    gint ref_count;
};

struct _AptDescriptionIter {
    AptCache *cache;
    cindex i;
    gboolean complete;
};

struct _AptPackageIter {
    AptCache *cache;
    AptCacheGroupMember *members;
    cindex i;
    cindex n;
    gboolean complete;
    AptCacheFindFlags flags;
};

/**
 * APT_CACHE_ERROR:
 *
 * The error domain of the APT cache.
 **/
GQuark apt_cache_error_quark(void)
{
    return g_quark_from_static_string("apt_cache_error");
}

/**
 * apt_cache_new:
 * @filename: The path to the file containing the cache.
 * @error: return location for a #GError, or %NULL
 *
 * Open the cache file at the given location.
 *
 * Maps the file at the given location into memory and deserializes
 * it. If an error occured while mapping the file, the function returns
 * %NULL and sets @error. In such a case, errors are most likely
 * from the #AptCacheError or #GFileError domains.
 *
 * Return Value: a newly allocated #AptCache which must be unref'd with
 *               apt_cache_unref(), or %NULL if an error occured.
 **/
AptCache *apt_cache_new(const gchar *filename, GError **error)
{
    AptCache *cache;
    gsize length;

    g_return_val_if_fail(filename != NULL, NULL);
    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    cache = g_slice_new(AptCache);
    cache->mapping = g_mapped_file_new(filename, FALSE, error);

    if (cache->mapping == NULL)
        goto error;

    length = g_mapped_file_get_length(cache->mapping);
    cache->data = g_mapped_file_get_contents(cache->mapping);
    cache->header = cache->data;

    if (length < sizeof(AptCacheHeader) ||
        memcmp(cache->header->magic_bytes, "APT", 4) != 0) {
        g_set_error(error, APT_CACHE_ERROR, APT_CACHE_ERROR_CORRUPT,
                    _("Corrupt: Invalid header"));
        goto error;
    }

    if (cache->header->end_of_file != length) {
        g_set_error(error, APT_CACHE_ERROR, APT_CACHE_ERROR_CORRUPT,
                    _("Corrupt: Cache is %lu bytes large, %lu required"),
                    (gulong) length, (gulong) cache->header->end_of_file);
        goto error;
    }

    if (cache->header->major_version != APT_CACHE_MAJOR_VERSION ||
        cache->header->minor_version != APT_CACHE_MINOR_VERSION) {
        g_set_error(error, APT_CACHE_ERROR, APT_CACHE_ERROR_UNSUPPORTED,
                    _("Unsupported cache version '%u.%u', '%u.%u' required"),
                    cache->header->major_version,
                    cache->header->minor_version,
                    APT_CACHE_MAJOR_VERSION, APT_CACHE_MINOR_VERSION);
        goto error;
    }

    cache->strings = cache_at_offset(cache, cache->header->begin_strings);
    cache->packages = cache_at_offset(cache, cache->header->begin_packages);
    cache->dependencies = cache_at_offset(cache,
                                          cache->header->begin_dependencies);
    cache->provides = cache_at_offset(cache, cache->header->begin_provides);
    cache->files = cache_at_offset(cache, cache->header->begin_files);
    cache->descriptions = cache_at_offset(cache,
                                          cache->header->begin_descriptions);
    cache->group_members = cache_at_offset(cache,
                                           cache->header->begin_group_members);
    cache->groups = cache_at_offset(cache, cache->header->begin_groups);
    cache->ref_count = 1;

    return cache;
  error:
    g_prefix_error(error, "%s: ", filename);
    g_slice_free(AptCache, cache);
    return NULL;
}

/**
 * apt_cache_ref:
 * @cache: An #AptCache instance
 *
 * Increments the reference count of @cache by one. It is safe to call this
 * function from any thread.
 *
 * Return Value: The passed in #AptCache.
 */
AptCache *apt_cache_ref(AptCache *cache)
{
    g_return_val_if_fail(cache != NULL, NULL);
    return g_atomic_int_inc(&cache->ref_count), cache;
}

/**
 * apt_cache_unref:
 * @cache: An #AptCache instance
 *
 * Decrements the reference count of @cache by one. If the reference count
 * drops to 0, the object is freed.
 **/
void apt_cache_unref(AptCache *cache)
{
    g_return_if_fail(cache != NULL);
    if (g_atomic_int_dec_and_test(&cache->ref_count)) {
        if (cache->mapping)
            g_mapped_file_unref(cache->mapping);
        else
            g_free(cache->data);
        g_slice_free(AptCache, cache);
    }
}

/**
 * apt_cache_get_format_version:
 * @cache: An #AptCache instance
 *
 * Returns the format version of the cache.
 *
 * This integer is increased every time the format changes in an incompatible
 * way.
 *
 * Return value: The format version.
 */
gint32 apt_cache_get_format_version(AptCache *cache)
{
    g_return_val_if_fail(cache != NULL, 0);
    return cache->header->major_version;
}

/**
 * apt_cache_get_system_name:
 * @cache: An #AptCache instance.
 *
 * Gets the system name stored in the cache, such as 'deb'.
 *
 * Return value: A pointer to the system name.
 * TODO: No system name yet
 **/
const gchar *apt_cache_get_system_name(AptCache *cache)
{
    g_return_val_if_fail(cache != NULL, NULL);
    return NULL;
}

/**
 * apt_cache_n_packages:
 * @cache: An #AptCache instance.
 *
 * Returns the number of packages available in the cache.
 *
 * Return value: The number of packages in @cache.
 **/
gsize apt_cache_n_packages(AptCache *cache)
{
    g_return_val_if_fail(cache != NULL, 0);
    return cache->header->n_packages;
}

/**
 * apt_cache_n_packagefiles:
 * @cache: An #AptCache instance.
 *
 * Returns the number of package files available in the cache.
 *
 * Return value: The number of package files in @cache.
 **/
gsize apt_cache_n_packagefiles(AptCache *cache)
{
    g_return_val_if_fail(cache != NULL, 0);
    return cache->header->n_files;
}

/**
 * apt_cache_iterator:
 * @cache: An #AptCache
 *
 * Provides an iterator over all packages in the cache.
 *
 * Return value: a newly allocated #AptPackageIter.
 */
AptPackageIter *apt_cache_iterator(AptCache *cache)
{
    AptPackageIter *iter;

    g_return_val_if_fail(cache != NULL, NULL);
    g_return_val_if_fail(cache->packages != NULL, NULL);

    iter = g_slice_new0(AptPackageIter);
    iter->cache = apt_cache_ref(cache);
    iter->n = apt_cache_n_packages(cache);

    return iter;
}

/**
 * apt_cache_get_package:
 * @cache: An #AptCache instance.
 * @id: The numeric identifier of the package.
 *
 * Gets a package from the cache based on its ID.
 *
 * Return value: A reference to an AptPackage object.
 **/
AptPackage *apt_cache_get_package(AptCache *cache, gsize id)
{
    AptPackage *package;

    g_return_val_if_fail(cache != NULL, NULL);
    g_return_val_if_fail(id < apt_cache_n_packages(cache), NULL);

    package = g_slice_new(AptPackage);
    package->cache = apt_cache_ref(cache);
    package->package = cache->packages + id;
    package->ref_count = 1;

    return package;
}

/**
 * apt_cache_get_package_file:
 * @cache: An #AptCache instance.
 * @id: The numeric identifier of the package.
 *
 * Gets a package from the cache based on its ID.
 *
 * Return value: A reference to an AptPackage object.
 **/
AptPackageFile *apt_cache_get_package_file(AptCache *cache, guint8 id)
{
    AptPackageFile *packagefile;

    g_return_val_if_fail(cache != NULL, NULL);
    g_return_val_if_fail(id < apt_cache_n_packagefiles(cache), NULL);

    packagefile = g_slice_new(AptPackageFile);
    packagefile->cache = apt_cache_ref(cache);
    packagefile->packagefile = cache->files + id;
    packagefile->ref_count = 1;

    return packagefile;
}

static AptCacheGroup *cache_find_group(AptCache *cache, const gchar *name)
{
    guint32 hash = g_str_hash(name);
    guint32 h = cache->header->h_buckets[hash % APT_CACHE_N_BUCKETS];

    while (h != CINDEX_NONE) {
        AptCacheGroup *group = cache->groups + h;
        if (group->hash == hash && strcmp(cache->strings + group->name,
                                          name) == 0)
            return group;
        h = group->h_next;
    }
    return NULL;
}

static gboolean member_matches_flags(const AptCache *cache,
                                     const AptCacheGroupMember *member,
                                     AptCacheFindFlags flags)
{
    const AptCachePackage *package = cache->packages + member->package_id;

    if ((flags & APT_CACHE_FIND_INSTALLED_ONLY) && !package->state_current)
        return FALSE;
    switch (member->type) {
    case APT_CACHE_GROUP_MEMBER_BINARY:
        return (flags & APT_CACHE_FIND_BY_BINARY) != 0;
    case APT_CACHE_GROUP_MEMBER_PROVIDES:
        return (flags & APT_CACHE_FIND_BY_PROVIDES) != 0;
    case APT_CACHE_GROUP_MEMBER_DEPENDS:
        return (flags & APT_CACHE_FIND_BY_DEPENDS) != 0;
    case APT_CACHE_GROUP_MEMBER_SOURCE:
        return (flags & APT_CACHE_FIND_BY_SOURCE) != 0;
    default:
        g_return_val_if_reached(FALSE);
    }
}

/**
 * apt_cache_find:
 * @cache: An #AptCache instance
 * @name: The name of the package to be found.
 * @flags: The #AptCacheFindFlags determining what to query.
 *
 * Finds all packages with the given name by looking them up in the
 * internal hashtable and returns an #AptPackageIter.
 *
 * Return value: An #AptPackageIter of all matching packages or %NULL if none.
 **/
AptPackageIter *apt_cache_find(AptCache *cache, const gchar *name,
                               AptCacheFindFlags flags)
{
    cindex i;
    AptCacheGroup *group;
    AptPackageIter *iter;
    gboolean found = FALSE;

    g_return_val_if_fail(cache != NULL, NULL);
    g_return_val_if_fail(name != NULL, NULL);
    g_return_val_if_fail(flags != 0, NULL);

    group = cache_find_group(cache, name);

    if (group == NULL)
        return NULL;

    for (i = group->begin_members; i != CINDEX_NONE;) {
        AptCacheGroupMember *member = cache->group_members + i;
        if ((found = member_matches_flags(cache, member, flags)))
            break;
        i = member->next;
    }

    if (!found)
        return NULL;

    iter = g_slice_new(AptPackageIter);
    iter->cache = apt_cache_ref(cache);
    iter->members = cache->group_members;
    iter->i = i;
    iter->n = CINDEX_NONE;
    iter->complete = FALSE;
    iter->flags = flags;
    return iter;
}

/**
 * apt_cache_is_up_to_date:
 * @cache: An #AptCache
 *
 * Checks whether @cache is up to date.
 *
 * Return value: %TRUE if @cache is up to date, %FALSE otherwise.
 **/
gboolean apt_cache_is_up_to_date(AptCache *cache)
{
    guint i;
    guint n;
    gboolean result = TRUE;

    g_return_val_if_fail(cache != NULL, FALSE);

    n = apt_cache_n_packagefiles(cache);

    for (i = 0; i < n && result; i++) {
        AptPackageFile *file = apt_cache_get_package_file(cache, i);
        result = apt_package_file_is_up_to_date(file);
        apt_package_file_unref(file);
    }

    return result;
}

/**
 * apt_package_unref:
 * @package: An #AptPackage
 *
 * Decreases the reference count of @package by one. If the reference count
 * reaches 0, free the memory used by @package.
 */
void apt_package_unref(AptPackage *package)
{
    g_return_if_fail(package != NULL);
    if (g_atomic_int_dec_and_test(&package->ref_count)) {
        apt_cache_unref(package->cache);
        g_slice_free(AptPackage, package);
    }
}

/**
 * apt_package_ref:
 * @package: An #AptPackage
 *
 * Increases the reference count of @package by one.
 */
AptPackage *apt_package_ref(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);
    g_atomic_int_inc(&package->ref_count);
    return package;
}

/**
 * apt_package_get_id:
 * @package: An #AptPackage
 *
 * Gets the unique numeric identifier of the package in the cache.
 *
 * Return value: An unsigned 32-bit identifier.
 **/
guint32 apt_package_get_id(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, G_MAXUINT32);
    return package->package - package->cache->packages;
}

/**
 * apt_package_get_name:
 * @package: An #AptPackage
 *
 * Gets the name of the package.
 *
 * Return value: An UTF-8 encoded string with the name of the package.
 **/
const gchar *apt_package_get_name(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);
    g_return_val_if_fail(package->package != NULL, NULL);
    g_return_val_if_fail(package->package->name != 0, NULL);
    g_return_val_if_fail(package->cache != NULL, NULL);
    g_return_val_if_fail(package->cache->strings != NULL, NULL);

    return package->cache->strings + package->package->name;
}

/**
 * apt_package_get_version:
 * @package: An #AptPackage
 *
 * Retrieve the version of this package as an UTF-8 encoded string.
 *
 * Return Value: A constant, utf-8 encoded string.
 **/
const gchar *apt_package_get_version(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);
    return package->cache->strings + package->package->version;
}

/**
 * apt_package_get_architecture:
 * @package: An #AptPackage
 *
 * Retrieve the name of the architecture this package is built for.
 *
 * Return Value: A constant, utf-8 encoded string.
 **/
const gchar *apt_package_get_architecture(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);
    return package->cache->strings + package->package->architecture;
}

/**
 * apt_package_get_multi_arch:
 * @package: An #AptPackage
 *
 * Gets information about how the package behaves in systems with packages
 * from mixed architectures; that is, multi-arch environments.
 *
 * Return value: An #AptMultiArch value determining the multi-arch state of
 *               the package.
 **/
AptMultiArch apt_package_get_multi_arch(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->multi_arch;
}

/**
 * apt_package_get_size:
 * @package: An #AptPackage
 *
 * Gets the size of the package file in bytes.
 *
 * Return value: The size of the file in bytes.
 **/
goffset apt_package_get_size(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->size;
}

/**
 * apt_package_get_installed_size:
 * @package: An #AptPackage
 *
 * Gets the estimated size of the package when unpacked
 * in bytes. This size is not the amount of disk space
 * the package will consume, as this depends on factors
 * such as block sizes, filesystems, and files created
 * in post installation scripts.
 *
 * Return value: The size of the unpacked package in bytes.
 **/
goffset apt_package_get_installed_size(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->installed_size;
}

/**
 * apt_package_get_checksum:
 * @package: An #AptPackage
 *
 * Gets the checksum of the package, if one is stored in the cache, or %NULL
 * if no checksum is stored. The checksum may either be MD5, SHA1, or SHA256;
 * the type of the checksum is determined by looking at the length of the
 * string.
 *
 * Return value: A constant UTF-8 encoded string containing the checksum's
 *               hexadecimal representation; or %NULL if the checksum is
 *               unknown.
 **/
const gchar *apt_package_get_checksum(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);

    if (package->package->checksum == 0)
        return NULL;
    return package->cache->strings + package->package->checksum;
}

/**
 * apt_package_get_priority:
 * @package: An #AptPackage
 *
 * Gets the priority of the package. If no correct package priority is set,
 * the return value will be %APT_PRIORITY_UNKNOWN. To convert the result to
 * a string, use the function apt_priority_to_string().
 *
 * Return value: An #AptPriority describing the package's priority.
 **/
AptPriority apt_package_get_priority(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->priority;
}

/**
 * apt_package_get_section:
 * @package: An #AptPackage
 *
 * Gets the section of the package if one is stored in the cache, or %NULL
 * if none is stored. Some package management solutions may have no notion
 * of sections and may thus always cause this function to return %NULL.
 *
 * Return value: A constant UTF-8 encoded string containing the package's
 *               section; or %NULL if the package has no section.
 **/
const gchar *apt_package_get_section(AptPackage *package)
{
    coffset section;

    g_return_val_if_fail(package != NULL, NULL);

    section = package->package->section;
    return section ? package->cache->strings + section : NULL;
}

/**
 * apt_package_get_dependency:
 * @package: An #AptPackage
 * @index: The index of the dependency.
 *
 * Gets the requested dependency of the package. It is an error to call this
 * function with a value for @index that is larger than the number returned
 * by apt_package_n_dependencies().
 *
 * Return value: An #AptDependency.
 **/
AptDependency *apt_package_get_dependency(AptPackage *package, gsize index)
{
    AptDependency *dependency;

    g_return_val_if_fail(package != NULL, NULL);
    g_return_val_if_fail(index < apt_package_n_dependencies(package), NULL);

    if (package->package->begin_dependencies == CINDEX_NONE)
        return NULL;

    dependency = g_slice_new(AptDependency);
    dependency->cache = apt_cache_ref(package->cache);
    dependency->package = package->package;
    dependency->dependency = (package->cache->dependencies +
                              package->package->begin_dependencies + index);
    dependency->ref_count = 1;

    return dependency;
}

/**
 * apt_package_n_dependencies:
 * @package: An #AptPackage
 *
 * Gets the number of dependencies this package has.
 *
 * Return value: The number of dependencies @package has.
 **/
gsize apt_package_n_dependencies(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);

    return (package->package->end_dependencies -
            package->package->begin_dependencies);
}

/**
 * apt_package_get_provides:
 * @package: An #AptPackage
 * @index: The index of the provides.
 *
 * Gets the requested provides of the package. It is an error to call this
 * function with a value for @index that is larger than the number returned
 * by apt_package_n_provides().
 *
 * Return value: An #AptProvides.
 **/
AptProvides *apt_package_get_provides(AptPackage *package, gsize index)
{
    AptProvides *provides;

    g_return_val_if_fail(package != NULL, NULL);
    g_return_val_if_fail(index < apt_package_n_provides(package), NULL);

    if (package->package->begin_provides == CINDEX_NONE)
        return NULL;

    provides = g_slice_new(AptProvides);
    provides->cache = apt_cache_ref(package->cache);
    provides->package = package->package;
    provides->provides = (package->cache->provides +
                          package->package->begin_provides + index);
    provides->ref_count = 1;

    return provides;
}

/**
 * apt_package_n_provides:
 * @package: An #AptPackage
 *
 * Gets the number of virtual packages provided by @package.
 *
 * Return value: The number of virtual packages provided by @package.
 **/
gsize apt_package_n_provides(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);

    return package->package->end_provides - package->package->begin_provides;
}

/**
 * apt_package_get_source_name:
 * @package: An #AptPackage
 *
 * Gets the name of the source package this package is built from.
 *
 * Return value: The name of the source package.
 **/
const gchar *apt_package_get_source_name(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);

    if (package->package->source_name == 0)
        return apt_package_get_name(package);

    return package->cache->strings + package->package->source_name;
}

/**
 * apt_package_get_source_version:
 * @package: An #AptPackage
 *
 * Gets the version of the source package this package is built from.
 *
 * Return value: The version of the source package.
 **/
const gchar *apt_package_get_source_version(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, NULL);

    if (package->package->source_version == 0)
        return apt_package_get_version(package);
    return package->cache->strings + package->package->source_version;
}

/**
 * apt_package_get_current_state:
 * @package: An #AptPackage
 *
 * Gets the package state.
 **/
AptCurrentState apt_package_get_current_state(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->state_current;
}

/**
 * apt_state_get_selection:
 * @package: An #AptPackage
 *
 * Gets the selection state as stored in @package.
 **/
AptSelectionState apt_package_get_selection_state(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->state_selection;
}

/**
 * apt_package_get_flags:
 * @package: An #AptPackage
 *
 * Gets the flags as stored in @package.
 **/
AptPackageFlags apt_package_get_flags(AptPackage *package)
{
    g_return_val_if_fail(package != NULL, 0);
    return package->package->flags;
}

/**
 * apt_package_get_descriptions:
 * @package: The package for which the descriptions should be looked up.
 *
 * Finds the descriptions associated with the given package.
 *
 * Return value: An iterator over #AptDescription objects.
 **/
AptDescriptionIter *apt_package_get_descriptions(AptPackage *package)
{
    AptDescriptionIter *iter;

    g_return_val_if_fail(package != NULL, NULL);

    iter = g_slice_new(AptDescriptionIter);
    iter->cache = apt_cache_ref(package->cache);
    iter->i = package->package->begin_descriptions;
    iter->complete = FALSE;

    return iter;
}

/**
 * apt_package_iter_next_value:
 * @iter: An #AptPackageIter
 *
 * Gets the next package in the iterator or %NULL if there are no further
 * packages. The return value of this function must be unref'd by the user
 * when no longer needed.
 *
 * Return value: The next package in the iterator or %NULL if the end of the
 *               iteration is reached.
 */
AptPackage *apt_package_iter_next_value(AptPackageIter *iter)
{
    AptCacheGroupMember *member;
    AptPackage *package;

    g_return_val_if_fail(iter != NULL, NULL);
    g_return_val_if_fail(!iter->complete, NULL);
    g_return_val_if_fail(iter->i <= iter->n, NULL);

    if (iter->members != NULL) {
        while (iter->i != iter->n && !member_matches_flags(iter->cache,
                                                           iter->members +
                                                           iter->i,
                                                           iter->flags)) {
            iter->i = iter->members[iter->i].next;
        }
    }

    g_assert(iter->i <= iter->n);

    if (iter->i == iter->n) {
        iter->complete = TRUE;
        return NULL;
    }

    member = iter->members ? iter->members + iter->i : NULL;
    package = g_slice_new(AptPackage);
    package->cache = apt_cache_ref(iter->cache);
    package->ref_count = 1;
    package->package = (member ? iter->cache->packages + member->package_id
                        : (iter->cache->packages + iter->i));
    iter->i = member ? member->next : iter->i + 1;
    return package;
}

/**
 * apt_package_iter_free:
 * @iter: An #AptPackageIter
 *
 * Free the package iterator @iter.
 **/
void apt_package_iter_free(AptPackageIter *iter)
{
    g_return_if_fail(iter != NULL);
    apt_cache_unref(iter->cache);
    g_slice_free(AptPackageIter, iter);
}

/**
 * apt_dependency_get_name:
 * @dependency: An #AptDependency
 *
 * Gets the name of the dependency's target package.
 *
 * Return value: A constant UTF-8 encoded string.
 **/
const gchar *apt_dependency_get_name(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, NULL);
    return dependency->cache->strings + dependency->dependency->target_name;
}

/**
 * apt_dependency_get_version:
 * @dependency: An #AptDependency
 *
 * Gets the version of the dependency's target if there is one. If the
 * dependency is not limited to a version, this function returns %NULL.
 *
 * Return value: The target version or %NULL if all versions are satisfying.
 **/
const gchar *apt_dependency_get_version(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, NULL);
    if (dependency->dependency->target_version == 0)
        return NULL;
    return dependency->cache->strings + dependency->dependency->target_version;
}

/**
 * apt_dependency_get_architecture:
 * @dependency: An #AptDependency
 *
 * Get the target architecture. If the return value is %NULL, it means
 * that the dependency can be satisfied only by architecture-independent
 * packages or by packages for the same architecture or by packages with
 * %APT_MULTI_ARCH_FOREIGN. If the return value is "any", the dependency
 * may be satisfied by packages with %APT_MULTI_ARCH_ALLOWED. In other
 * cases, the behavior is undefined; but should result in the dependency
 * not being resolved at all.
 *
 * Return value: %NULL or "any".
 **/
const gchar *apt_dependency_get_architecture(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, NULL);
    g_return_val_if_fail(dependency != NULL, NULL);
    if (dependency->dependency->target_architecture == 0)
        return NULL;
    return (dependency->cache->strings
            + dependency->dependency->target_architecture);
}

/**
 * apt_dependency_get_comparison:
 * @dependency: An #AptDependency.
 *
 * Gets the type of comparison this dependency requires. For example, if the
 * dependency were "a (>= 1.0)", the return value would be the
 * #AptComparisonType equivalent of >=; that is %APT_COMPARISON_GE.
 *
 * Return value: An #AptComparisonType describing the comparison operator.
 **/
AptComparisonType apt_dependency_get_comparison(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, 0);
    return dependency->dependency->comparison;
}

/**
 * apt_dependency_get_type:
 * @dependency: An #AptDependency.
 *
 * Gets the type of dependency as an #AptDependencyType value.
 *
 * Return value: An #AptDependencyType describing the type of dependency.
 **/
AptDependencyType apt_dependency_get_type(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, 0);
    return dependency->dependency->type;
}

/**
 * apt_dependency_get_next_is_or:
 * @dependency: An #AptDependency.
 *
 * Determines whether the next dependency is ORed to this one. For example,
 * in a dependency group 'a | b', this function returns %TRUE for the
 * #AptDependency corresponding to 'a' and %FALSE for the #AptDependency
 * corresponding to 'b'.
 *
 * Return value: %TRUE if the next dependency is ORed to the current one,
 *               %FALSE otherwise.
 **/
gboolean apt_dependency_get_next_is_or(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, 0);
    return dependency->dependency->next_is_or;
}

/**
 * apt_dependency_ref:
 * @dependency: An #AptDependency.
 *
 * Increases the reference count of @dependency by one.
 *
 * Return value: A new reference to @dependency.
 **/
AptDependency *apt_dependency_ref(AptDependency *dependency)
{
    g_return_val_if_fail(dependency != NULL, NULL);
    g_atomic_int_inc(&dependency->ref_count);
    return dependency;
}

/**
 * apt_dependency_unref:
 * @dependency: An #AptDependency.
 *
 * Decreases the reference count of @dependency by one.
 **/
void apt_dependency_unref(AptDependency *dependency)
{
    g_return_if_fail(dependency != NULL);
    if (g_atomic_int_dec_and_test(&dependency->ref_count)) {
        apt_cache_unref(dependency->cache);
        g_slice_free(AptDependency, dependency);
    }
}

/**
 * apt_provides_unref:
 * @provides: An #AptProvides
 *
 * Decreases the reference count of @provides by one. If the reference count
 * drops to 0, the memory allocated by @provides is freed.
 **/
void apt_provides_unref(AptProvides *provides)
{
    g_return_if_fail(provides != NULL);
    if (g_atomic_int_dec_and_test(&provides->ref_count)) {
        apt_cache_unref(provides->cache);
        g_slice_free(AptProvides, provides);
    }
}

/**
 * apt_provides_ref:
 * @provides: An #AptProvides
 *
 * Increases the reference count of @provides by one.
 *
 * Return value: @provides
 **/
AptProvides *apt_provides_ref(AptProvides *provides)
{
    g_return_val_if_fail(provides != NULL, NULL);
    g_atomic_int_inc(&provides->ref_count);
    return provides;
}

/**
 * apt_provides_get_name:
 * @provides: An #AptProvides
 *
 * Gets the name of the virtual package provided by @provides.
 *
 * Return value: A constant UTF-8 encoded string containing the name.
 */
const gchar *apt_provides_get_name(AptProvides *provides)
{
    g_return_val_if_fail(provides != NULL, NULL);
    return provides->cache->strings + provides->provides->name;
}

/**
 * apt_provides_get_version:
 * @provides: An #AptProvides
 *
 * Gets the version of the virtual package provided by @provides, if it
 * provides a version; or %NULL if the virtual package is not versioned.
 *
 * Return value: A constant UTF-8 encoded string containing the version, or
 *               %NULL if the virtual package has no version.
 */
const gchar *apt_provides_get_version(AptProvides *provides)
{
    g_return_val_if_fail(provides != NULL, NULL);

    if (provides->provides->version == 0)
        return 0;
    return provides->cache->strings + provides->provides->version;
}

/**
 * apt_package_file_get_filename:
 * @packagefile: An #AptPackageFile
 *
 * Gets the file name of the package file; that is, the path to the local
 * copy of the index file in the Dir::State::lists directory.
 *
 * Return value: UTF-8 encoded string with the absolute path to the file.
 */
const gchar *apt_package_file_get_filename(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    return packagefile->cache->strings + packagefile->packagefile->filename;
}

/**
 * apt_package_file_get_archive:
 * @packagefile: An #AptPackageFile
 *
 * Gets the archive of the package file. For Debian-based systems, this
 * is equal to the "Suite" field stored in Release files. If no archive
 * is defined, the function returns %NULL.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_archive(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->archive == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->archive;
}

/**
 * apt_package_file_get_codename:
 * @packagefile: An #AptPackageFile
 *
 * Gets the codename of the package file. For Debian-based systems, this
 * is equal to the "Codename" field stored in Release files. If no codename
 * is defined, the function returns %NULL.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_codename(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->codename == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->codename;
}

/**
 * apt_package_file_get_component:
 * @packagefile: An #AptPackageFile
 *
 * Gets the component of the package file. The component can be used to
 * separate certain parts of an archive and equals the fourth field (and
 * following ones) in sources.list files. For Debian, this is mostly "main",
 * "contrib" or "non-free". Components are optional, and as such, the function
 * may return %NULL if no component is set.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_component(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->component == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->component;
}

/**
 * apt_package_file_get_version:
 * @packagefile: An #AptPackageFile
 *
 * Gets the version of the package file. This should be the version of the
 * distribution this package files belongs to. For Debian-based systems, this
 * field is retrieved from the 'Version' field in the Release file. This field
 * is optional, and the function may thus return %NULL if no version is set.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_version(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->version == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->version;
}

/**
 * apt_package_file_get_origin:
 * @packagefile: An #AptPackageFile
 *
 * Gets the origin of the package file. The origin is a short string
 * describing the origin of the packages in this repository. For
 * Debian-based systems, this is extracted from the "Origin" field in
 * the Release file. Examples here are "Debian" for official Debian
 * packages and "Ubuntu" for official Ubuntu packages. Specifying
 * an origin is not required, if there is no origin, the function
 * returns %NULL.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_origin(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->origin == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->origin;
}

/**
 * apt_package_file_get_label:
 * @packagefile: An #AptPackageFile
 *
 * Gets the label of the package file. The label should be a short string
 * describing the repository. It is optional and if not existent, the
 * function returns %NULL.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_label(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->label == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->label;
}

/**
 * apt_package_file_get_site:
 * @packagefile: An #AptPackageFile
 *
 * Gets the site associated with the package file, if it was fetched via
 * network from a server, or %NULL if the package file belongs to a local
 * repository.
 *
 * Return value: UTF-8 encoded string or %NULL.
 */
const gchar *apt_package_file_get_site(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->site == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->site;
}

/**
 * apt_package_file_get_type:
 * @packagefile: An #AptPackageFile
 *
 * Gets the type of the package file. This should be a human-readable string
 * describing the type of the index file. This function is considered unstable
 * and may be changed to return a enumeration value in a future update. Do not
 * use it.
 *
 * Stability: unstable
 * Return value: UTF-8 encoded human-readable string.
 */
const gchar *apt_package_file_get_type(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    return packagefile->cache->strings + packagefile->packagefile->type;
}

/**
 * apt_package_file_get_base_uri:
 * @packagefile: An #AptPackageFile
 *
 * Gets the base URI of the archive. Appending the filename of a package from
 * this package file to this URI will produce the URI at which the package is
 * located. If the package file refers to a file which can not act as a source
 * for packages, such as dpkg's <filename>/var/lib/dpkg/status</filename>, this
 * function returns %NULL.
 *
 * Return value: UTF-8 encoded URI string or %NULL if there is no base URI.
 */
const gchar *apt_package_file_get_base_uri(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    if (packagefile->packagefile->base_uri == 0)
        return NULL;
    return packagefile->cache->strings + packagefile->packagefile->base_uri;
}

/**
 * apt_package_file_get_mtime:
 * @packagefile: An #AptPackageFile
 *
 * Gets the last time this package file was modified before it was added
 * to the cache. If this value is older than the modification time of the
 * file in the file system, the cache should be considered outdated.
 *
 * Return value: The last time of modification as #time_t.
 */
time_t apt_package_file_get_mtime(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, 0);
    return packagefile->packagefile->mtime;
}

/**
 * apt_package_file_get_flags:
 * @packagefile: An #AptPackageFile
 *
 * Gets the flags associated with this package file as combination of
 * #AptPackageFileFlags values. See the description of #AptPackageFileFlags
 * for information on the meanings of the flags.
 *
 * Return value: The flags associated with this file as #AptPackageFileFlags.
 **/
AptPackageFileFlags apt_package_file_get_flags(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, 0);
    return packagefile->packagefile->flags;
}

/**
 * apt_package_file_unref:
 * @packagefile: An #AptPackageFile
 *
 * Decrease the reference count of @packagefile by one. If the reference count
 * drops to zero, free the object.
 **/
void apt_package_file_unref(AptPackageFile *packagefile)
{
    g_return_if_fail(packagefile != NULL);
    if (g_atomic_int_dec_and_test(&packagefile->ref_count)) {
        apt_cache_unref(packagefile->cache);
        g_slice_free(AptPackageFile, packagefile);
    }
}

/**
 * apt_package_file_ref:
 * @packagefile: An #AptPackageFile
 *
 * Increase the reference count of @packagefile by one.
 *
 * Return value: @packagefile
 */
AptPackageFile *apt_package_file_ref(AptPackageFile *packagefile)
{
    g_return_val_if_fail(packagefile != NULL, NULL);
    g_atomic_int_inc(&packagefile->ref_count);
    return packagefile;
}

/**
 * apt_package_file_is_up_to_date:
 * @packagefile: An #AptPackageFile
 *
 * Checks whether @packagefile is up to date; that is, whether the
 * file on the disk has not been modified since it was cached.
 *
 * Return value: %TRUE if the file is up to date, %FALSE otherwise.
 **/
gboolean apt_package_file_is_up_to_date(AptPackageFile *packagefile)
{
    struct stat buf;

    g_return_val_if_fail(packagefile != NULL, FALSE);

    if (stat(apt_package_file_get_filename(packagefile), &buf) == 0)
        return (buf.st_mtime == apt_package_file_get_mtime(packagefile));

    g_critical(_("Could not stat() %s: %s"),
               apt_package_file_get_filename(packagefile), g_strerror(errno));
    return FALSE;
}

/**
 * apt_description_ref:
 * @description: An #AptDescription
 *
 * Increments the reference count of @description.
 *
 * Return value: @description
 **/
AptDescription *apt_description_ref(AptDescription *description)
{
    g_return_val_if_fail(description != NULL, NULL);
    g_atomic_int_inc(&description->ref_count);
    return description;
}

/**
 * apt_description_unref:
 * @description: An #AptDescription
 *
 * Decrements the reference count of @description. If the reference count
 * drops to 0, the memory pointed to by @description is freed.
 **/
void apt_description_unref(AptDescription *description)
{
    g_return_if_fail(description != NULL);
    if (g_atomic_int_dec_and_test(&description->ref_count)) {
        apt_cache_unref(description->cache);
        g_slice_free(AptDescription, description);
    }
}

/**
 * apt_description_get_package_file_id:
 * @description: An #AptDescription
 *
 * Gets the ID of the package file associated with this #AptDescription. The
 * return value may be passed to apt_cache_get_package_file() in order to
 * receive a corresponding #AptPackageFile.
 */
guint8 apt_description_get_package_file_id(AptDescription *description)
{
    g_return_val_if_fail(description != NULL, G_MAXUINT8);
    return description->description->file_id;
}

/**
 * apt_description_get_filename:
 * @description: An #AptDescription
 *
 * Gets the name of the package archive (i.e. the deb/rpm/... file). In case
 * this object is not related to a complete package record, but only to a
 * translated description, the return value will be %NULL.
 *
 * Return value: The filename of the package or %NULL.
 **/
const gchar *apt_description_get_filename(AptDescription *description)
{
    g_return_val_if_fail(description != NULL, NULL);
    if (description->description->filename == 0)
        return 0;
    return description->cache->strings + description->description->filename;
}

/**
 * apt_description_get_md5:
 * @description: An #AptDescription
 *
 * Gets the hexadecimal representation of the untranslated description's
 * MD5 checksum. This value is used to locate the translations of the
 * description.
 *
 * Return value: The hexadecimal representation of the MD5 checksum as a
 *               constant string.
 **/
const gchar *apt_description_get_md5(AptDescription *description)
{
    g_return_val_if_fail(description != NULL, NULL);

    if (description->description->desc_hash == 0)
        return NULL;
    return description->cache->strings + description->description->desc_hash;
}

/**
 * apt_description_get_language:
 * @description: An #AptDescription
 *
 * Gets the language of the description if it is a translated description, as
 * an ISO 639 language code such as 'en' and optionally an ISO 3166 country
 * code such as 'US'. In this example, the resulting language code would be
 * 'en_US'. If the description is not translated, the function shall return
 * %NULL.
 *
 * Return value: The language code if translated or %NULL if the description
 *               is not a translation.
 **/
const gchar *apt_description_get_language(AptDescription *description)
{
    g_return_val_if_fail(description != NULL, NULL);
    if (description->description->language == 0)
        return NULL;
    return description->cache->strings + description->description->language;
}

/**
 * apt_description_get_offset:
 * @description: An #AptDescription
 *
 * Gets the offset of the package record in the packagefile. While the
 * return type of this value is a #goffset, the offset is stored internally
 * as an unsigned 32 bit integer; since it is not expected that package
 * files reach sizes > 4 GB.
 *
 * Returns: The offset as a #goffset.
 **/
goffset apt_description_get_offset(AptDescription *description)
{
    g_return_val_if_fail(description != NULL, 0);
    return description->description->desc_offset;
}

/**
 * apt_description_iter_next_value:
 * @iter: An #AptDescriptionIter
 *
 * Gets the next #AptDescription in @iter or %NULL if there are no further
 * descriptions.
 *
 * Return value: The next #AptDescription or %NULL if there are no more ones.
 **/
AptDescription *apt_description_iter_next_value(AptDescriptionIter *iter)
{
    AptDescription *description;

    g_return_val_if_fail(iter != NULL, NULL);
    g_return_val_if_fail(!iter->complete, NULL);

    if (iter->i == CINDEX_NONE) {
        iter->complete = TRUE;
        return NULL;
    }

    description = g_slice_new(AptDescription);
    description->cache = apt_cache_ref(iter->cache);
    description->description = iter->cache->descriptions + iter->i;
    description->ref_count = 1;
    iter->i = description->description->next;
    return description;
}

/**
 * apt_description_iter_free:
 * @iter: An #AptDescriptionIter
 *
 * Free @iter.
 **/
void apt_description_iter_free(AptDescriptionIter *iter)
{
    apt_cache_unref(iter->cache);
    g_slice_free(AptDescriptionIter, iter);
}

/**
 * apt_priority_to_string:
 * @priority: An #AptPriority
 *
 * Converts the #AptPriority @priority to a string describing the
 * priority.
 *
 * Return value: A constant string describing the priority which consists
 *               only of the lowercase letters a-z.
 **/
const gchar *apt_priority_to_string(AptPriority priority)
{
    switch (priority) {
    case APT_PRIORITY_REQUIRED:
        return "required";
    case APT_PRIORITY_IMPORTANT:
        return "important";
    case APT_PRIORITY_STANDARD:
        return "standard";
    case APT_PRIORITY_OPTIONAL:
        return "optional";
    case APT_PRIORITY_EXTRA:
        return "extra";
    default:
        g_return_val_if_reached("unknown");
    }
}

/**
 * apt_dependency_type_rate:
 * @type: An #AptDependencyType
 *
 * Rates the given dependency type, determining whether the dependency
 * will cause the installation of a package (positive) or whether it
 * prevents a package from being installed.
 *
 * Return value: <0 if the dependency is negative,
 *                0 if the dependency does not have any effect in itself.
 *               >0 if the dependency is positive.
 **/
gint apt_dependency_type_rate(AptDependencyType type)
{
    switch (type) {
    case APT_DEPENDENCY_BREAKS:
    case APT_DEPENDENCY_CONFLICTS:
        return -1;

    case APT_DEPENDENCY_ENHANCES:
    case APT_DEPENDENCY_REPLACES:
        return 0;

    case APT_DEPENDENCY_PREDEPENDS:
    case APT_DEPENDENCY_DEPENDS:
    case APT_DEPENDENCY_RECOMMENDS:
    case APT_DEPENDENCY_SUGGESTS:
        return 1;
    default:
        g_return_val_if_reached(0);
    }
}

/**
 * apt_dependency_type_to_string:
 * @type: An #AptDependencyType
 *
 * Converts @type to a human-readable string that is the name of the
 * Debian dependency type which equals @type.
 *
 * Return value: A constant string describing @type.
 **/
const gchar *apt_dependency_type_to_string(AptDependencyType type)
{
    switch (type) {
    case APT_DEPENDENCY_BREAKS:
        return "Breaks";
    case APT_DEPENDENCY_CONFLICTS:
        return "Conflicts";
    case APT_DEPENDENCY_DEPENDS:
        return "Depends";
    case APT_DEPENDENCY_ENHANCES:
        return "Enhances";
    case APT_DEPENDENCY_PREDEPENDS:
        return "Pre-Depends";
    case APT_DEPENDENCY_RECOMMENDS:
        return "Recommends";
    case APT_DEPENDENCY_REPLACES:
        return "Replaces";
    case APT_DEPENDENCY_SUGGESTS:
        return "Suggests";
    default:
        g_return_val_if_reached("X-Invalid");
    }
}

/**
 * apt_multi_arch_to_string:
 * @multi_arch: An #AptMultiArch value
 *
 * Converts the #AptMultiArch value @multi_arch to a string as defined
 * in Debian's Multi-Arch field.
 *
 * Return value: A constant string describing @multi_arch which consists
 *               only of the lowercase letters a-z.
 **/
const gchar *apt_multi_arch_to_string(AptMultiArch multi_arch)
{
    switch (multi_arch) {
    case APT_MULTI_ARCH_ALLOWED:
        return "allowed";
    case APT_MULTI_ARCH_FOREIGN:
        return "foreign";
    case APT_MULTI_ARCH_NONE:
        return "none";
    case APT_MULTI_ARCH_SAME:
        return "same";
    default:
        g_return_val_if_reached("INVALID");
    }
}

/**
 * apt_comparison_type_to_string:
 * @type: The type that shall be converted to a string
 *
 * Converts the given comparison type to the operator used
 * in Debian package files. If @type is APT_COMPARISON_NONE,
 * an empty string is returned.
 *
 * Return value: A pointer to a constant string of 0 - 2 characters.
 **/
const gchar *apt_comparison_type_to_string(AptComparisonType type)
{
    switch (type) {
    case APT_COMPARISON_NONE:
        return "";
    case APT_COMPARISON_LT:
        return "<<";
    case APT_COMPARISON_LE:
        return "<=";
    case APT_COMPARISON_EQ:
        return "=";
    case APT_COMPARISON_NE:
        return "!=";
    case APT_COMPARISON_GE:
        return ">=";
    case APT_COMPARISON_GT:
        return ">>";
    }
    g_return_val_if_reached("");
}
