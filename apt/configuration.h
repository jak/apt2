/* configuration.h - APT configuration system.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_CONFIGURATION_H__
#define __APT_LIB_CONFIGURATION_H__

#include <glib.h>

G_BEGIN_DECLS

#define APT_CONFIGURATION_ERROR apt_configuration_error_quark()
GQuark apt_configuration_error_quark(void);

/**
 * AptConfigurationError:
 * @APT_CONFIGURATION_ERROR_FAILED: Something failed.
 *
 * Error values for the configuration subsystem.
 **/
typedef enum {
    APT_CONFIGURATION_ERROR_FAILED
} AptConfigurationError;

typedef struct _AptConfiguration AptConfiguration;

AptConfiguration *apt_configuration_new(void);
AptConfiguration *apt_configuration_ref(AptConfiguration *configuration);
void apt_configuration_unref(AptConfiguration *configuration);
const gchar **apt_configuration_get_list(AptConfiguration *configuration,
                                         const gchar *name, gsize *length);
const gchar *apt_configuration_get(AptConfiguration *configuration,
                                   const gchar *name);
void apt_configuration_set(AptConfiguration *configuration,
                           const gchar *name, const gchar *value);
gint apt_configuration_get_int(AptConfiguration *configuration,
                               const gchar *name);
gchar *apt_configuration_get_file(AptConfiguration *configuration,
                                  const gchar *name);
gboolean apt_configuration_get_boolean(AptConfiguration *configuration,
                                       const gchar *name);
gboolean apt_configuration_parse_file(AptConfiguration *configuration,
                                      const gchar *filename, GError **error);
gboolean apt_configuration_parse_dir(AptConfiguration *configuration,
                                     const gchar *dirname, GError **error);
gboolean apt_configuration_parse_text(AptConfiguration *configuration,
                                      const gchar *text, GError **error);

gboolean apt_configuration_init_defaults(AptConfiguration *configuration,
                                         GError **error);
gchar *apt_configuration_dumps(AptConfiguration *configuration);

GOptionGroup *apt_configuration_get_option_group(AptConfiguration
                                                 *configuration);

G_END_DECLS
#endif                          /* __APT_LIB_CONFIGURATION_H__ */
