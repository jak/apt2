/* session.c - High-level application session API.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <apt/session.h>
#include <apt/cache-internal.h>

#include <glib.h>

G_GNUC_INTERNAL AptCache *apt_cache_import(AptConfiguration *configuration);

/**
 * SECTION:session
 * @short_description: Easy high-level API for APT2 programs.
 * @title: AptSession
 * @include: apt.h
 * @stability: Unstable
 *
 * When writing a program using the APTLib API, the correct initialization
 * of the classes, reading configuration, etc can become slightly
 * annoying. The #AptSession API is provided to make this setup as easy
 * as possible. It takes care of reading the configuration files, loading
 * the system, opening the cache, creating a new cache if none exists,
 * updating the cache if it is outdated.
 *
 * Programs using APTLib are expected to create a single #AptSession at
 * the beginning of the program execution and retrieve pointers to the
 * #AptSession members by calling the getter functions documented in this
 * section.
 **/

/**
 * AptSession:
 *
 * #AptSession is an opaque type which may only be accessed by the following
 * functions.
 **/
struct _AptSession {
    AptConfiguration *configuration;
    AptCache *cache;
    AptSystem *system;
    AptPolicy *policy;
    AptSourcesList *sources;
    gint ref_count;
};

/*
 * Initialization functions that get called from apt_session_new()
 * to initialize the corresponding members
 */

static gboolean apt_session_init_cache(AptSession *session, GError **error)
{
    GError *e = NULL;
    gchar *cachefile;

    g_assert(session != NULL);
    g_assert(error == NULL || *error == NULL);

    cachefile = apt_configuration_get_file(session->configuration,
                                           "dir::cache::cache");
    session->cache = apt_cache_new(cachefile, &e);
    g_free(cachefile);
    if (session->cache && !apt_cache_is_up_to_date(session->cache)) {
        apt_cache_unref(session->cache);
        session->cache = NULL;
    }
    if (session->cache == NULL) {
        session->cache = apt_cache_import(session->configuration);
        if (session->cache == NULL) {
            g_propagate_error(error, e);
            return FALSE;
        }
        g_clear_error(&e);
    }
    return TRUE;
}

static gboolean apt_session_init_policy(AptSession *session, GError **error)
{
    gboolean res = FALSE;
    const gchar *default_release;
    gchar *pref, *prefdir;

    g_assert(session != NULL);
    g_assert(error == NULL || *error == NULL);

    pref = apt_configuration_get_file(session->configuration, "Dir::Etc::"
                                      "preferences");
    prefdir = apt_configuration_get_file(session->configuration, "Dir::Etc::"
                                         "preferencesparts");
    default_release = apt_configuration_get(session->configuration,
                                            "APT::Default-Release");

    session->policy = apt_policy_new(session->cache,
                                     apt_system_get_version_compare_func
                                     (session->system));

    if (default_release != NULL)
        apt_policy_set_default_release(session->policy, default_release);
    if (!apt_policy_parse_file(session->policy, pref, error))
        goto out;
    if (!apt_policy_parse_dir(session->policy, prefdir, error))
        goto out;
    res = TRUE;

  out:
    g_free(pref);
    g_free(prefdir);
    return res;
}

static gboolean apt_session_init_sources(AptSession *session, GError **error)
{
    gboolean res = FALSE;
    gchar *sources, *sourcesdir;

    g_assert(session != NULL);
    g_assert(error == NULL || *error == NULL);

    session->sources = apt_sources_list_new(session->system);
    sources = apt_configuration_get_file(session->configuration, "Dir::Etc::"
                                         "sourcelist");
    sourcesdir = apt_configuration_get_file(session->configuration, "Dir::Etc"
                                            "::sourceparts");

    if (!apt_sources_list_parse_file(session->sources, sources, error))
        goto out;
    if (!apt_sources_list_parse_dir(session->sources, sourcesdir, error))
        goto out;

    res = TRUE;
  out:
    g_free(sources);
    g_free(sourcesdir);
    return res;
}

static gboolean apt_session_init_system(AptSession *session, GError **error)
{
    g_assert(session != NULL);
    g_assert(error == NULL || *error == NULL);

    session->system = apt_system_new("deb", session->configuration, error);
    return (session->system != NULL);
}

/**
 * apt_session_new:
 * @error: Location to store a #GError.
 *
 * Parses the configuration files, constructs the needed objects and creates
 * a new #AptSession.
 *
 * Return value: A newly allocated #AptSession with reference count 1.
 **/
AptSession *apt_session_new(GError **error)
{
    GError *e = NULL;
    AptSession *session;

    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    session = g_slice_new0(AptSession);
    session->ref_count = 1;
    session->configuration = apt_configuration_new();

    if (!apt_configuration_init_defaults(session->configuration, &e))
        goto error;
    if (!apt_session_init_system(session, &e))
        goto error;
    if (!apt_session_init_sources(session, &e))
        goto error;
    if (!apt_session_init_cache(session, &e))
        goto error;
    if (!apt_session_init_policy(session, &e))
        goto error;
    return session;
  error:
    if (e != NULL)
        g_propagate_error(error, e);
    apt_session_unref(session);
    return NULL;
}

/**
 * apt_session_ref:
 * @session: An #AptSession
 *
 * Increments the reference count of @session.
 *
 * Return value: @session.
 */
AptSession *apt_session_ref(AptSession *session)
{
    g_return_val_if_fail(session != NULL, NULL);
    return (g_atomic_int_inc(&session->ref_count), session);
}

/**
 * apt_session_unref:
 * @session: An #AptSession
 *
 * Decrements the reference count of @session. If the reference count drops
 * to 0, release all references held to the members and free the memory
 * @session points to.
 */
void apt_session_unref(AptSession *session)
{
    g_return_if_fail(session != NULL);
    if (g_atomic_int_dec_and_test(&session->ref_count)) {
        if (session->configuration)
            apt_configuration_unref(session->configuration);
        if (session->cache)
            apt_cache_unref(session->cache);
        if (session->system)
            apt_system_unref(session->system);
        if (session->policy)
            apt_policy_unref(session->policy);
        if (session->sources)
            apt_sources_list_unref(session->sources);
        g_slice_free(AptSession, session);
    }
}

/**
 * apt_session_get_configuration:
 * @session: An #AptSession
 *
 * Gets an unowned reference to the #AptConfiguration of @session. Do not
 * call apt_configuration_unref() on it, unless you previously called
 * apt_configuration_ref() on it.
 *
 * Return value: An unowned reference to the #AptConfiguration of @session.
 **/
AptConfiguration *apt_session_get_configuration(AptSession *session)
{
    g_return_val_if_fail(session != NULL, NULL);
    return session->configuration;
}

/**
 * apt_session_get_cache:
 * @session: An #AptSession
 *
 * Gets an unowned reference to the #AptCache contained in @session. You must
 * not call apt_cache_unref() on the return value, unless you previously called
 * apt_cache_ref() on it.
 *
 * Return value: An unowned reference to the #AptCache of @session.
 **/
AptCache *apt_session_get_cache(AptSession *session)
{
    g_return_val_if_fail(session != NULL, NULL);
    return session->cache;
}

/**
 * apt_session_get_system:
 * @session: An #AptSession
 *
 * Gets an unowned reference to the #AptSystem contained in @session. You must
 * not call apt_system_unref() on the return value, unless you previously
 * called apt_system_ref() on it.
 *
 * Return value: An unowned reference to the #AptSystem of @session.
 **/
AptSystem *apt_session_get_system(AptSession *session)
{
    g_return_val_if_fail(session != NULL, NULL);
    return session->system;
}

/**
 * apt_session_get_policy:
 * @session: An #AptPolicy
 *
 * Gets an unowned reference to the #AptPolicy contained in @session. You must
 * not call apt_policy_unref() on the return value, unless you previously
 * called apt_policy_ref() on it.
 *
 * Return value: An unowned reference to the #AptPolicy of @session.
 **/
AptPolicy *apt_session_get_policy(AptSession *session)
{
    g_return_val_if_fail(session != NULL, NULL);
    return session->policy;
}
