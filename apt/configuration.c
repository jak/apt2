/* configuration.c - APT configuration system.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include <apt/utils.h>
#include <apt/configuration.h>

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n-lib.h>

/**
 * SECTION:configuration
 * @short_description: Reading APT configuration files.
 * @title: AptConfiguration
 * @include: apt.h
 * @stability: Unstable
 *
 * The APT configuration system is basically a tree of options. Each option
 * can have zero or more sub-options and these suboptions can either all
 * have a name or no name (which makes them list elements). The levels of
 * the tree are seperated by two colons, like C++.
 **/

static void apt_config_scanner_message(GScanner *scanner, const gchar *message,
                                       gboolean error);

/*
 * AptConfigurationEntry:
 * @name: The name of the option, or a regular expression if @type is uppercase
 * @type: The type of the option: 's' (string), 'f' (file), 'd' (directory),
 *        'b' (boolean), 'i' (int), 'u' (unsigned), 'r' (regexp). Can also be
 *        the uppercase versions thereof, then @name is treated as a regex.
 * @default_value: The default value. If %NULL, the option has no default
 *
 * #AptConfigurationEntry describes an option entry in APT2's typed
 * configuration system.
 */
typedef struct _AptConfigurationEntry {
    const gchar *name;
    gchar type;
    const gchar *default_value;
} AptConfigurationEntry;

/*
 * defaults:
 *
 * All known options with their types and defaults.
 */
static AptConfigurationEntry defaults[] = {
    {"apt::system", 's', "deb"},
    {"apt::architecture", 's', APT_ARCHITECTURE},
    {"apt::build-essential::", 's', "build-essential"},
    {"apt::install-recommends", 'b', "true"},
    {"apt::install-suggests", 'b', "false"},
    {"apt::acquire-translation", 's', "environment"},
    /* File paths */
    {"dir", 'd', "/"},
    {"dir::cache", 'd', "var/cache/apt/"},
    {"dir::cache::archives", 'd', "archives/"},
    {"dir::cache::backup", 'd', "backup/"},
    {"dir::cache::cache", 'f', "cache.bin"},
    {"dir::etc", 'd', "etc/apt/"},
    {"dir::etc::main", 'f', "apt.conf"},
    {"dir::etc::parts", 'd', "apt.conf.d"},
    {"dir::etc::preferences", 'f', "preferences"},
    {"dir::etc::preferencesparts", 'd', "preferences.d"},
    {"dir::etc::sourcelist", 'f', "sources.list"},
    {"dir::etc::sourceparts", 'd', "sources.list.d"},
    {"dir::log", 'd', "var/log/apt"},
    {"dir::log::history", 'f', "history.log"},
    {"dir::log::terminal", 'f', "term.log"},
    {"dir::state", 'd', "var/lib/apt/"},
    {"dir::state::cdroms", 'f', "cdroms.list"},
    {"dir::state::extended_states", 'f', "extended_states"},
    {"dir::state::lists", 'd', "lists/"},
    {"dir::state::status", 'f', NULL},
    {"dir::bin::dpkg", 'f', NULL},
    {"dir::bin::methods", 'd', NULL},
    {"dir::cache::pkgcache", 'f', NULL},
    {"dir::cache::srccache", 'f', NULL},
    /* dpkg options */
    {"dpkg::pre-install-pkgs::", 's', NULL},
    {"dpkg::pre-invoke::", 's', NULL},
    {"dpkg::post-invoke::", 's', NULL},
    {"dpkg::options::", 's', NULL},
    {"^dpkg::tools::options::", 'S', NULL},
    /* matches all valid option names not in dir or dpkg */
    {"^(?!(dir::|dpkg::|apt2::))([^:]+(::){0,1})+$", 'S', NULL},
};

/**
 * APT_CONFIGURATION_ERROR:
 *
 * The error domain for configuration parsing.
 */
GQuark apt_configuration_error_quark()
{
    return g_quark_from_static_string("apt_configuration_error-quark");
}

/**
 * AptConfiguration:
 *
 * #AptConfiguration is an opaque data type which may only be accessed using
 * the following functions.
 **/
struct _AptConfiguration {
    GHashTable *options;
    GHashTable *lists;
    GScanner *scanner;
    gint ref_count;
};

/*
 * apt_configuration_init_scanner:
 * @scanner: Pointer to a variable that can hold a #GScanner
 *
 * Creates and configures a new #GScanner and assigns it to
 * the pointer @scanner points to; that is *@scanner.
 */
static void apt_configuration_init_scanner(GScanner **scanner)
{
    GScannerConfig config;
    memset(&config, 0, sizeof(GScannerConfig));
    config.cpair_comment_single = (gchar *) ("#\n");
    config.skip_comment_multi = TRUE;
    config.skip_comment_single = TRUE;
    config.cset_skip_characters = (gchar *) " \r\t";
    config.cset_identifier_nth =
        (gchar *) (G_CSET_a_2_z "/-:._+" G_CSET_A_2_Z "" G_CSET_DIGITS);
    config.cset_identifier_first = config.cset_identifier_nth;
    config.scan_identifier = TRUE;
    config.scan_identifier_1char = TRUE;

    config.scan_string_dq = TRUE;
    *scanner = g_scanner_new(&config);
    (*scanner)->msg_handler = (GScannerMsgFunc) apt_config_scanner_message;
}

/**
 * apt_configuration_new:
 *
 * Creates a new #AptConfiguration.
 *
 * Return value: A newly allocated #AptConfiguration with reference count 1.
 **/
AptConfiguration *apt_configuration_new(void)
{
    AptConfiguration *configuration = g_slice_new(AptConfiguration);
    configuration->options = g_hash_table_new_full(apt_utils_str_case_hash,
                                                   apt_utils_str_case_equal,
                                                   g_free, g_free);
    configuration->lists = g_hash_table_new_full(apt_utils_str_case_hash,
                                                 apt_utils_str_case_equal,
                                                 g_free, (GDestroyNotify)
                                                 g_ptr_array_unref);
    configuration->ref_count = 1;

    apt_configuration_init_scanner(&configuration->scanner);
    return configuration;
}

static gboolean is_valid(const gchar *name, const gchar *value, GError **error)
{

    static const gchar valid_bools[][8] = {
        "no", "false", "off", "disable", "without", "0",
        "yes", "true", "on", "enable", "with", "1"
    };
    gsize i;
    for (i = 0; i < G_N_ELEMENTS(defaults); i++) {
        if (g_ascii_islower(defaults[i].type)
            && g_ascii_strcasecmp(defaults[i].name, name) == 0)
            break;
        if (g_ascii_isupper(defaults[i].type)
            && g_regex_match_simple(defaults[i].name, name,
                                    G_REGEX_ANCHORED | G_REGEX_CASELESS,
                                    G_REGEX_MATCH_ANCHORED))
            break;
    }
    if (i == G_N_ELEMENTS(defaults)) {
        g_set_error(error, 0, 0, _("Unknown option '%s'"), name);
        return FALSE;
    }

    switch (g_ascii_tolower(defaults[i].type)) {
    case 'r':
        {
            GRegex *regex = g_regex_new(value, 0, 0, error);
            if (regex == NULL)
                return FALSE;
            g_regex_unref(regex);
            return TRUE;
        }
    case 'd':
        return TRUE;
    case 'f':
        if (value[strlen(value) - 1] == '/') {
            g_set_error(error, 0, 0, _("Cannot store directory reference '%s' "
                                       "in option '%s' of type file."),
                        value, name);
            return FALSE;
        }
    case 's':
        return TRUE;
    case 'b':
        for (i = 0; i < G_N_ELEMENTS(valid_bools); i++) {
            if (strcmp(valid_bools[i], value) == 0)
                return TRUE;
        }
        g_set_error(error, 0, 0, _("Invalid value '%s' for boolean option "
                                   "'%s', required one of 'true', 'false'."),
                    value, name);
        return FALSE;
    case 'u':
        if (*value == '-') {
            g_set_error(error, 0, 0, _("Invalid value '%s' for "
                                       "positive integer option '%s' (%s)"),
                        value, name, _("Not positive"));
            return FALSE;
        }
    case 'i':
        if (g_ascii_isspace(*value)) {
            g_set_error(error, 0, 0, _("Invalid value '%s' for "
                                       "positive integer option '%s' (%s)"),
                        value, name, _("Space not allowed"));
            return FALSE;
        } else {
            gchar *end;
            gint64 res;

            errno = 0;
            res = g_ascii_strtoll(value, &end, 10);
            if (errno != 0 || res < G_MININT || res > G_MAXINT) {
                gint err_no = errno ? errno : ERANGE;
                g_set_error(error, 0, 0, _("Invalid value '%s' for "
                                           "integer option '%s' (%s)"), value,
                            name, g_strerror(err_no));
            } else if (*end != '\0') {
                g_set_error(error, 0, 0, _("Invalid value '%s' for "
                                           "integer option '%s' (%s)"), value,
                            name, _("Not a number"));
            } else {
                return TRUE;
            }
            return FALSE;
        }
    }
    g_assert_not_reached();
}

/**
 * apt_configuration_unref:
 * @configuration: An #AptConfiguration
 *
 * Decrements the reference count of @configuration. If the reference count
 * drops to 0, the memory pointed to by @configuration will be freed.
 **/
void apt_configuration_unref(AptConfiguration *configuration)
{
    g_return_if_fail(configuration != NULL);
    if (g_atomic_int_dec_and_test(&configuration->ref_count)) {
        g_hash_table_unref(configuration->options);
        g_hash_table_unref(configuration->lists);
        g_scanner_destroy(configuration->scanner);
        g_slice_free(AptConfiguration, configuration);
    }
}

/**
 * apt_configuration_ref:
 * @configuration: An #AptConfiguration
 *
 * Increments the reference count of @configuration.
 *
 * Return value: @configuration
 **/
AptConfiguration *apt_configuration_ref(AptConfiguration *configuration)
{
    g_return_val_if_fail(configuration != NULL, NULL);
    return (g_atomic_int_inc(&configuration->ref_count), configuration);
}

/**
 * apt_configuration_get:
 * @configuration: An #AptConfiguration.
 * @name: The name of the option that shall be retrieved.
 *
 * Gets the value of the option @name and returns it as a constant
 * string. Please note that the returned string may be freed at any
 * time a new value for this option is set. Thus, if you want to keep
 * the value around, call g_strdup() on it. If the requested option is
 * not set, %NULL is returned.
 *
 * Return value: A constant string containing the value or %NULL if the
 *               option is not set.
 **/
const gchar *apt_configuration_get(AptConfiguration *configuration,
                                   const gchar *name)
{
    const gchar *result;
    g_return_val_if_fail(configuration != NULL && name != NULL, NULL);
    result = g_hash_table_lookup(configuration->options, name);
    return result ? result : NULL;
}

/**
 * apt_configuration_set:
 * @configuration: An #AptConfiguration
 * @name: The name of the option that should be set.
 * @value: The value that the option should be set to.
 *
 * Sets the configuration option @name to @value. The parameter @name is
 * pushed to lower-case before insertion, and both parameters are duplicated
 * before they are inserted into the hashtable.
 **/
void
apt_configuration_set(AptConfiguration *configuration, const gchar *name,
                      const gchar *value)
{
    g_return_if_fail(configuration != NULL);
    g_return_if_fail(name != NULL);
    g_return_if_fail(value != NULL);

    if (g_str_has_suffix(name, "::")) {
        gchar *key = g_ascii_strdown(name, -1);
        GPtrArray *array = g_hash_table_lookup(configuration->lists, key);
        if (!array) {
            array = g_ptr_array_new_with_free_func(g_free);
            g_hash_table_insert(configuration->lists, key, array);
        } else {
            g_free(key);
        }
        g_ptr_array_add(array, g_strdup(value));
    } else {
        g_hash_table_replace(configuration->options, g_ascii_strdown(name, -1),
                             g_strdup(value));
    }
}

/**
 * apt_configuration_get_list:
 * @configuration: An #AptConfiguration
 * @name: The name of the list option, including the :: suffix.
 * @length: Location to store the length of the list.
 *
 * Gets the list stored at the given option.
 *
 * Return value: A list of strings of the values at the given option.
 **/
const gchar **apt_configuration_get_list(AptConfiguration *configuration,
                                         const gchar *name, gsize *length)
{
    GPtrArray *array;

    g_return_val_if_fail(configuration != NULL, NULL);
    g_return_val_if_fail(name != NULL, NULL);
    g_return_val_if_fail(length != NULL, NULL);

    array = g_hash_table_lookup(configuration->lists, name);
    if (array == NULL) {
        return NULL;
    } else {
        *length = array->len;
        return (const gchar **) array->pdata;
    }
}

/**
 * apt_configuration_get_int:
 * @configuration: An #AptConfiguration.
 * @name: The name of the option that shall be retrieved.
 *
 * Calls apt_configuration_get() and converts the result into an
 * integer.
 *
 * Return value: The integer stored at @name, or 0 if the option does not
 *               exist or is no integer.
 **/
gint apt_configuration_get_int(AptConfiguration *configuration,
                               const gchar *name)
{
    const gchar *value = apt_configuration_get(configuration, name);
    return value ? atoi(value) : 0;
}

/**
 * apt_configuration_get_boolean:
 * @configuration: An #AptConfiguration.
 * @name: The name of the option that shall be retrieved.
 *
 * Calls apt_configuration_get() and converts the result into a
 * #gboolean. Valid boolean values are "no", "false", "off", "disable",
 * "without", and "0" for %FALSE; and "yes", "true", "on", "enable",
 * "with", and 1 for %TRUE. If the value of the option @name is not
 * one of those strings or if the option @name is not set, %FALSE
 * is returned.
 *
 * Return value: The boolean value stored at @name or %FALSE if @name is
 *               not set or is not a valid boolean value.
 **/
gboolean
apt_configuration_get_boolean(AptConfiguration *configuration,
                              const gchar *name)
{
    const gchar *value = apt_configuration_get(configuration, name);

    if (value == NULL)
        return FALSE;
    if (strcmp(value, "no") == 0 || strcmp(value, "false") == 0 ||
        strcmp(value, "off") == 0 || strcmp(value, "disable") == 0 ||
        strcmp(value, "without") == 0 || strcmp(value, "0") == 0)
        return FALSE;
    if (strcmp(value, "yes") == 0 || strcmp(value, "true") == 0 ||
        strcmp(value, "on") == 0 || strcmp(value, "enable") == 0 ||
        strcmp(value, "with") == 0 || strcmp(value, "1") == 0)
        return TRUE;
    return FALSE;
}

/*
 * parent:
 * @current: The name of the current option.
 *
 * Gets the parent option of @current.
 *
 * Return value: Parent of @current.
 */
static gchar *parent(gchar *current)
{
    gsize i;
    for (i = strlen(current); i > 1; i--)
        if (current[i] == ':' && current[i - 1] == ':')
            break;

    current[i - 1] = '\0';

    return current;
}

/**
 * apt_configuration_get_file:
 * @configuration: An #AptConfiguration
 * @name: The name of the option to be looked up
 *
 * Lookup a file in the configuration space. This lookups the value
 * at @name and goes upwards in the configuration tree, prepending
 * every value found until the path is absolute or a relative path
 * starting with "./" or "../". If a special configuration option
 * named "rootdir" exists, its value is prepended to the string.
 *
 * Return value: A string which contains the path to the file and
 *               must be freed using g_free() when no longer required.
 **/
gchar *apt_configuration_get_file(AptConfiguration *configuration,
                                  const gchar *name)
{
    GString *builder;
    gchar *parentkey;
    const gchar *root = apt_configuration_get(configuration, "rootdir");
    const gchar *value = apt_configuration_get(configuration, name);
    if (value == NULL)
        return NULL;
    if (g_path_is_absolute(value))
        return root ? g_build_filename(root, value, NULL) : g_strdup(value);

    builder = g_string_new(value);

    parentkey = g_strdup(name);
    while (!g_str_equal((parentkey = parent(parentkey)), "")) {
        value = apt_configuration_get(configuration, parentkey);
        if (g_path_is_absolute(builder->str) ||
            g_str_has_prefix(builder->str, "." G_DIR_SEPARATOR_S) ||
            g_str_has_prefix(builder->str, ".." G_DIR_SEPARATOR_S) ||
            value == NULL)
            break;
        if (!g_str_has_suffix(value, G_DIR_SEPARATOR_S))
            g_string_prepend_c(builder, G_DIR_SEPARATOR);
        g_string_prepend(builder, value);
    }
    g_free(parentkey);

    if (root != NULL)
        g_string_prepend(builder, root);

    return g_string_free(builder, FALSE);
}

/**
 * apt_configuration_dumps:
 * @configuration: An #AptConfiguration.
 *
 * Gets a string describing the options held in @configuration that can
 * be parsed by apt_configuration_parse_text().
 **/
gchar *apt_configuration_dumps(AptConfiguration *configuration)
{
    GString *string = g_string_new(NULL);
    GList *key;
    GList *keys;

    g_return_val_if_fail(configuration != NULL, NULL);

    keys = g_list_concat(g_hash_table_get_keys(configuration->options),
                         g_hash_table_get_keys(configuration->lists));
    keys = g_list_sort(keys, (GCompareFunc) strcmp);

    for (key = keys; key != NULL; key = key->next) {
        const gchar *name = key->data;
        if (g_str_has_suffix(key->data, "::")) {
            gsize i;
            gsize len;
            const gchar **values = apt_configuration_get_list(configuration,
                                                              name, &len);

            for (i = 0; i < len; i++)
                g_string_append_printf(string, "%s \"%s\";\n", name, values[i]);

        } else {
            g_string_append_printf(string, "%s \"%s\";\n", name,
                                   apt_configuration_get(configuration, name));
        }
    }
    g_list_free(keys);
    return g_string_free(string, FALSE);
}

/**
 * apt_configuration_init_defaults:
 * @configuration: An #AptConfiguration
 * @error: Return location to store a #GError.
 *
 * Initializes @configuration using default values and by reading the
 * configuration file specified by the environment variable APT_CONFIG
 * and then the file specified by the option Dir::Etc::main and the files
 * in the directory specified by the option Dir::Etc::parts.
 *
 * Return value: %TRUE if everything succeded, %FALSE otherwise.
 */
gboolean apt_configuration_init_defaults(AptConfiguration *configuration,
                                         GError **error)
{
    gsize i;
    const gchar *apt_config = g_getenv("APT_CONFIG");
    gchar *parts;
    gchar *main_;

    g_return_val_if_fail(configuration != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    /* General APT things */
    for (i = 0; i < G_N_ELEMENTS(defaults); i++)
        if (defaults[i].default_value != NULL)
            apt_configuration_set(configuration, defaults[i].name,
                                  defaults[i].default_value);

    /* Read an alternative config file */
    if (apt_config &&
        !apt_configuration_parse_file(configuration, apt_config, error))
        return FALSE;

    /* Read the configuration parts dir */
    parts = apt_configuration_get_file(configuration, "Dir::Etc::parts");
    if (!apt_configuration_parse_dir(configuration, parts, error)) {
        g_free(parts);
        return FALSE;
    }
    g_free(parts);

    /* Read the main config file */
    main_ = apt_configuration_get_file(configuration, "Dir::Etc::main");
    if (!apt_configuration_parse_file(configuration, main_, error)) {
        g_free(main_);
        return FALSE;
    }

    g_free(main_);

    if (apt_configuration_get_boolean(configuration, "Debug::pkgInitConfig")) {
        gchar *dumps = apt_configuration_dumps(configuration);
        g_print("%s", dumps);
        g_free(dumps);
    }
    return TRUE;
}

/* < private >
 * apt_config_scanner_next:
 * @scanner: A GScanner
 *
 * Returns the apt_config_scanner_next token in the stream while ignoring comments.
 *
 * Return value: A #GTokenType describing the apt_config_scanner_next token
 */
static GTokenType apt_config_scanner_next(GScanner *scanner)
{
    GTokenType token = g_scanner_get_next_token(scanner);
    while (token == G_TOKEN_CHAR && scanner->value.v_char == '\n')
        token = g_scanner_get_next_token(scanner);
    while (token == G_TOKEN_IDENTIFIER
           && strncmp(scanner->value.v_identifier, "//", 2) == 0) {
        while (token != G_TOKEN_EOF
               && (token != G_TOKEN_CHAR || scanner->value.v_char != '\n'))
            token = g_scanner_get_next_token(scanner);
        while (token == G_TOKEN_CHAR && scanner->value.v_char == '\n')
            token = g_scanner_get_next_token(scanner);
    }
    return token;
}

/*
 * apt_config_scanner_message:
 * @scanner: A #GScanner
 * @message: The message that describes the error.
 * @error: Whether the message is a real error or not.
 *
 * Calls g_set_error() to set a GError** in @scanner->user_data. This allows
 * us to forward all parsing errors via GError to the callers.
 */
static void apt_config_scanner_message(GScanner *scanner, const gchar *message,
                                       gboolean error)
{
    /* We handle warnings as errors too, ignore the argument */
    (void) error;
    if (scanner->user_data != NULL && *(GError **) scanner->user_data == NULL) {
        g_set_error(scanner->user_data, APT_CONFIGURATION_ERROR,
                    APT_CONFIGURATION_ERROR_FAILED, "%s:%d:%d: %s",
                    scanner->input_name ? scanner->input_name : "<memory>",
                    scanner->line, scanner->position, message);
    }
}

/* < private >
 * apt_configuration_parse_common:
 * @configuration: An #AptConfiguration
 * @section: The name of the current section
 * @error: Return location for a #GError.
 *
 * Parses the data previously input into the #GScanner of @configuration.
 */
static gboolean apt_configuration_parse_common(AptConfiguration *configuration,
                                               const gchar *section,
                                               GError **error)
{
    GScanner *scanner = configuration->scanner;
    GTokenType token;
    scanner->user_data = error;
    while ((token = apt_config_scanner_next(scanner)) != G_TOKEN_EOF) {
        gchar *name = NULL;
        if (token == G_TOKEN_CHAR) {
            if (scanner->value.v_char == '}' && *section != '\0')
                return TRUE;
            else {
                g_scanner_unexp_token(scanner, G_TOKEN_IDENTIFIER, NULL, NULL,
                                      NULL, NULL, TRUE);
                return FALSE;
            }
        }
        if (token == G_TOKEN_IDENTIFIER) {
            name = g_strdup(scanner->value.v_identifier);
            token = apt_config_scanner_next(scanner);
            /* The apt_config_scanner_next char can only be a {. */
            if ((token == G_TOKEN_CHAR && scanner->value.v_char != '{') &&
                token != G_TOKEN_STRING) {
                g_scanner_unexp_token(scanner, G_TOKEN_STRING, NULL, NULL, NULL,
                                      "Unfinished option, missing value.",
                                      TRUE);
                g_free(name);
                return FALSE;
            }
        }
        if (token == G_TOKEN_STRING) {
            GError *tmp_error = NULL;
            gchar *option = g_strjoin("", section, name ? name : "", NULL);

            if (!is_valid(option, scanner->value.v_string, &tmp_error)) {
                g_warning("%s:%d:%d: %s",
                          (scanner->input_name ? scanner->input_name
                           : "<memory>"),
                          scanner->line, scanner->position, tmp_error->message);
                g_error_free(tmp_error);
            }
            apt_configuration_set(configuration, option,
                                  scanner->value.v_string);
            token = apt_config_scanner_next(scanner);
            g_free(option);
        }
        if (token == G_TOKEN_CHAR && scanner->value.v_char == '{') {
            gchar *option =
                g_strjoin("", section, name ? name : "", "::", NULL);

            if (!apt_configuration_parse_common(configuration, option, error))
                return FALSE;
            token = apt_config_scanner_next(scanner);
            g_assert(token == G_TOKEN_CHAR);
            g_free(option);
        }

        g_free(name);
        if (token != G_TOKEN_CHAR || scanner->value.v_char != ';') {
            g_scanner_unexp_token(scanner, G_TOKEN_CHAR, NULL, NULL, NULL,
                                  "Unfinished option, missing ';'.", TRUE);
            return FALSE;
        }

    }
    if (*section != '\0') {
        g_scanner_unexp_token(scanner, G_TOKEN_CHAR, NULL, NULL, NULL,
                              "Missing end of section", TRUE);
        return FALSE;
    }

    return TRUE;

}

/**
 * apt_configuration_parse_file:
 * @configuration: An #AptConfiguration.
 * @filename: The name of the file which shall be parsed.
 * @error: Location to store an error.
 *
 * Parses the configuration file located at @filename and inserts the
 * option it defines into @configuration.
 *
 * Return value: %TRUE if the file could be parsed, %FALSE otherwise.
 **/
gboolean
apt_configuration_parse_file(AptConfiguration *configuration,
                             const gchar *filename, GError **error)
{
    gboolean result;
    int fd;

    g_return_val_if_fail(configuration != NULL, FALSE);
    g_return_val_if_fail(filename != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    configuration->scanner->input_name = filename;
    fd = open(filename, O_RDONLY | O_NOCTTY, 0);

    if (fd == -1) {
        const gchar *message = g_strerror(errno);
        GFileError errcode = g_file_error_from_errno(errno);
        g_set_error(error, G_FILE_ERROR, errcode,
                    _("Error while reading %s: %s"), filename, message);
        return FALSE;
    }

    g_scanner_input_file(configuration->scanner, fd);
    result = apt_configuration_parse_common(configuration, "", error);
    close(fd);
    return result;
}

/**
 * apt_configuration_parse_dir:
 * @configuration: An #AptConfiguration.
 * @dirname: The name of the directory containing the files to be parsed.
 * @error: Location to store an error.
 *
 * Parses the configuration files located in the directory @dirname and
 * insert the options they define into @configuration.
 *
 * Return value: %TRUE if all files could be parsed, %FALSE otherwise.
 **/
gboolean
apt_configuration_parse_dir(AptConfiguration *configuration,
                            const gchar *dirname, GError **error)
{
    g_return_val_if_fail(configuration != NULL, FALSE);
    g_return_val_if_fail(dirname != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);
    return apt_utils_foreach_in_dir(dirname, ".conf", (AptUtilsForeachInDirFunc)
                                    apt_configuration_parse_file,
                                    configuration, error);
}

/**
 * apt_configuration_parse_text:
 * @configuration: An #AptConfiguration.
 * @text: The text that shall be parsed.
 * @error: Location to store an error.
 *
 * Parses @text which must be a string in the format of APT configuration
 * files and inserts the options it defines into @configuration.
 *
 * Return value: %TRUE if the text could be parsed, %FALSE otherwise.
 **/
gboolean
apt_configuration_parse_text(AptConfiguration *configuration,
                             const gchar *text, GError **error)
{
    g_return_val_if_fail(configuration != NULL, FALSE);
    g_return_val_if_fail(text != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    configuration->scanner->input_name = NULL;
    g_scanner_input_text(configuration->scanner, text, strlen(text));
    return apt_configuration_parse_common(configuration, "", error);
}

/*
 * apt_configuration_opt_parse:
 * @option_name: The name of the command-line option (-c or --config-file).
 * @value: The value passed, should be a filename.
 * @data: The #AptConfiguration this function acts on.
 * @error: Return location for an error.
 *
 * Parses the file located at the path given by @value. This
 * is a call-back function for the #GOptionGroup returned by
 * apt_configuration_get_option_group().
 *
 * Return value: %TRUE if parsing was successful, %FALSE otherwise.
 */
static gboolean apt_configuration_opt_parse(const gchar *option_name,
                                            const gchar *value, gpointer data,
                                            GError **error)
{
    GError *sub_error = NULL;

    (void) option_name;

    if (!apt_configuration_parse_file(data, value, &sub_error)) {
        g_set_error_literal(error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                            sub_error->message);
        g_clear_error(&sub_error);
        return FALSE;
    }
    return TRUE;
}

/*
 * apt_configuration_opt_set:
 * @option_name: The name of the command-line option (-o or --option).
 * @value: The value passed, should be a key=value combination.
 * @data: The #AptConfiguration this function acts on.
 * @error: Return location for an error.
 *
 * Parses @value into a name and a value and sets the option
 * in the #AptConfiguration object @data. This is a call-back
 * function for the #GOptionGroup returned by
 * apt_configuration_get_option_group().
 *
 * Return value: %TRUE if the value was a key=value combination,
 *               %FALSE otherwise.
 */
static gboolean apt_configuration_opt_set(const gchar *option_name,
                                          const gchar *value, gpointer data,
                                          GError **error)
{
    gboolean result = TRUE;
    gchar **option = g_strsplit(value, "=", 2);

    (void) option_name;

    if (option[0] == NULL || option[1] == NULL || option[2] != NULL) {
        g_set_error(error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                    _("Expected key=value pair, received '%s'"), value);
        result = FALSE;
    } else if (!is_valid(option[0], option[1], error)) {
        g_prefix_error(error, "Error in command-line option: ");
        result = FALSE;
    } else {
        apt_configuration_set(data, option[0], option[1]);
    }
    g_strfreev(option);

    return result;
}

/*
 * apt_configuration_opt_entries:
 *
 * Option entries for use by apt_configuration_get_option_group. They are
 * marked using G_GNUC_EXTENSION since function pointers are assigned to
 * void* fields and this is not allowed in ISO C (and causes a warning
 * in -pedantic).
 */
G_GNUC_EXTENSION static const GOptionEntry apt_configuration_opt_entries[] = {
    {"config-file", 'c', G_OPTION_FLAG_FILENAME, G_OPTION_ARG_CALLBACK,
     &apt_configuration_opt_parse, N_("Read the given config file"),
     N_("FILE")},
    {"option", 'o', 0, G_OPTION_ARG_CALLBACK, &apt_configuration_opt_set,
     N_("Set the given option to value"), N_("option=value")},
    {NULL, 0, 0, 0, NULL, NULL, NULL}
};

/**
 * apt_configuration_get_option_group:
 * @configuration: An #AptConfiguration
 *
 * Gets a new #GOptionGroup for @configuration containing the options
 * 'config-file' (-c) and 'option' (-o).
 *
 * Return value: A newly allocated #GOptionGroup for @configuration. It holds
 *               its own reference to @configuration.
 **/
GOptionGroup *apt_configuration_get_option_group(AptConfiguration
                                                 *configuration)
{
    GOptionGroup *group;

    g_return_val_if_fail(configuration != NULL, NULL);

    group = g_option_group_new("apt", "APT Configuration options",
                               "APT Configuration options",
                               apt_configuration_ref(configuration),
                               (GDestroyNotify) apt_configuration_unref);

    g_option_group_add_entries(group, apt_configuration_opt_entries);
    g_option_group_set_translation_domain(group, GETTEXT_PACKAGE);

    return group;
}
