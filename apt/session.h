/* session.h - High-level application session API.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_SESSION_H__
#define __APT_LIB_SESSION_H__

#include <apt/cache.h>
#include <apt/configuration.h>
#include <apt/system.h>
#include <apt/policy.h>
#include <apt/sourceslist.h>

#include <glib.h>

G_BEGIN_DECLS

typedef struct _AptSession AptSession;

AptSession *apt_session_new(GError **error);
AptSession *apt_session_ref(AptSession *session);
void apt_session_unref(AptSession *session);
AptConfiguration *apt_session_get_configuration(AptSession *session);
AptCache *apt_session_get_cache(AptSession *session);
AptSystem *apt_session_get_system(AptSession *session);
AptPolicy *apt_session_get_policy(AptSession *session);

G_END_DECLS
#endif                          /* __APT_LIB_SESSION_H__ */
