/* utils.c - Various utility functions for APT2.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include <apt/utils.h>

#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <glib.h>
#include <glib/gi18n-lib.h>

/**
 * SECTION:utils
 * @short_description: Various utility functions that do not fit elsewhere.
 * @title: Utility Functions
 * @include: apt.h
 * @stability: Unstable
 *
 * This section documents various utility functions that are needed for APT2
 * and may also be useful to applications using APT2.
 **/

/* < private >
 * _apt_is_run_parts:
 * @name: A file name.
 *
 * Check that name is a valid run parts name; that is, it contains only
 * alphanumeric, hyphen-minus (-), underscore (_) and period characters.
 */
static inline gboolean _apt_is_run_parts(const gchar *name)
{
    for (; *name != '\0'; name++)
        if (*name != '-' && *name != '_' && *name != '.' && !isalnum(*name))
            return FALSE;
    return TRUE;
}

/**
 * apt_utils_foreach_in_dir:
 * @dirname: The path to the directory that shall be read.
 * @suffix: The suffix the files should have (including .)
 * @callback: The #AptUtilsForeachInDirFunc to be called for each file
 *            in the directory.
 * @user_data: The pointer that will be passed to @callback as its first
 *             argument.
 * @error: The location to store an error.
 *
 * Calls the function @callback for each file in the directory (given
 * the complete path to the file) that ends with @suffix or consists
 * only of alphanumeric, hyphen-minus (-), underscore (_) and period
 * characters.
 *
 * If @callback encounters an error, it should return %FALSE and set
 * its error pointer. In such a case, this function exits immediately
 * with the value %FALSE and propagates the error of @callback to the
 * caller.
 *
 * Return value: %TRUE if everything is OK, %FALSE if this function or
 *               @callback encountered an error.
 **/
gboolean apt_utils_foreach_in_dir(const gchar *dirname, const gchar *suffix,
                                  AptUtilsForeachInDirFunc callback,
                                  gpointer user_data, GError **error)
{
    GError *dir_open_error = NULL;
    GDir *dir;
    GList *list = NULL;
    GList *current = NULL;
    gboolean res = TRUE;
    const gchar *name;

    g_return_val_if_fail(dirname != NULL, FALSE);
    g_return_val_if_fail(suffix != NULL, FALSE);
    g_return_val_if_fail(callback != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    dir = g_dir_open(dirname, 0, &dir_open_error);
    if (dir == NULL) {
        g_propagate_error(error, dir_open_error);
        return FALSE;
    }

    while ((name = g_dir_read_name(dir))) {
        if (_apt_is_run_parts(name) || g_str_has_suffix(name, suffix))
            list = g_list_prepend(list, g_build_filename(dirname, name, NULL));
    }
    list = g_list_sort(list, (GCompareFunc) strcmp);
    for (current = list; current != NULL; current = current->next) {
        GError *callback_error = NULL;
        if (!callback(user_data, current->data, &callback_error)) {
            g_propagate_error(error, callback_error);
            res = FALSE;
            break;
        }
    }
    g_list_foreach(list, (GFunc) g_free, NULL);
    g_list_free(list);
    g_dir_close(dir);
    return res;
}

/**
 * apt_utils_match:
 * @pattern: A regular expression if it starts with a forward slash and ends
 *           with one, otherwise a glob() like pattern.
 * @string: The string @pattern should be matched again.
 *
 * Checks whether @pattern matches @string.
 *
 * Return value: %TRUE if @pattern matches @string, %FALSE otherwise. If
 *               @string is %NULL, the result is always %FALSE.
 **/
gboolean apt_utils_match(const gchar *pattern, const gchar *string)
{
    g_return_val_if_fail(pattern != NULL, FALSE);
    if (string == NULL)
        return FALSE;
    if (pattern[0] == '/') {
        gsize length = strlen(pattern);
        if (pattern[length - 1] == '/') {
            char *regex = g_strndup(pattern + 1, length - 2);
            gboolean res = g_regex_match_simple(regex, string, 0, 0);
            g_free(regex);
            return res;
        }
    }
    return g_pattern_match_simple(pattern, string);
}

/* Needed for apt_utils_str_case_hash, to avoid function calls there */
#define ISUPPER(c) ((c) >= 'A' && (c) <= 'Z')
#define TOLOWER(c) (ISUPPER(c) ? (c) - 'A' + 'a' : (c))

/**
 * apt_utils_str_case_hash:
 * @v: a string key
 *
 * Converts a string to a hash value, ignoring the
 * case of ASCII characters.
 *
 * Return value: a hash value corresponding to the key
 */
guint apt_utils_str_case_hash(gconstpointer v)
{
    /* 31 bit hash function */
    const signed char *p = v;
    guint32 h = TOLOWER(*p);

    if (h)
        for (p += 1; *p != '\0'; p++)
            h = (h << 5) - h + TOLOWER(*p);

    return h;
}

/**
 * apt_utils_str_case_equal:
 * @s1: The first string
 * @s2: The second string
 *
 * Checks whether the two strings @s1 and @s2 are equal,
 * ignoring differing cases of ASCII characters.
 *
 * Return value: %TRUE if @s1 matches @s2, %FALSE otherwise.
 **/
gboolean apt_utils_str_case_equal(gconstpointer s1, gconstpointer s2)
{
    return g_ascii_strcasecmp(s1, s2) == 0;
}

/**
 * apt_utils_lock_file:
 * @file: The path to the file that shall be locked.
 * @error: Location to store a #GFileError.
 *
 * Acquires an advisory POSIX write lock for the file
 * @file. If any other process already holds a lock
 * on @file or if the file can not be locked due to
 * other reasons, the return value is -1 and @error
 * is set to an error describing the reason why the
 * lock could not be acquired.
 *
 * Since this uses POSIX locking, calling close() on
 * any file descriptor for the file @file will release
 * all locks on this file.
 *
 * Return value: The file descriptor of the lock file, or -1 on failure.
 **/
gint apt_utils_lock_file(const gchar *file, GError **error)
{
    struct flock flockfile;
    gint fd;

    g_return_val_if_fail(file != NULL, -1);
    g_return_val_if_fail(error == NULL || *error == NULL, -1);

    fd = open(file, O_WRONLY | O_CREAT | O_NOCTTY, 0644);
    flockfile.l_type = F_WRLCK;
    flockfile.l_whence = SEEK_SET;
    flockfile.l_start = 0;
    flockfile.l_len = 0;
    if (fd == -1 || fcntl(fd, F_SETFD, FD_CLOEXEC) == -1 ||
        fcntl(fd, F_SETLK, &flockfile) == -1) {
        g_set_error(error, G_FILE_ERROR, g_file_error_from_errno(errno),
                    _("Error while locking %s: %s"), file, g_strerror(errno));
        if (fd != -1)
            close(fd);
        fd = -1;
    }
    return fd;
}

/**
 * apt_utils_uri_to_filename:
 * @uri: A valid Uniform Resource Identifier (URI) as specified in RFC 3986
 *
 * Converts @uri, excluding scheme and userinfo, into a string that can
 * be used as a local filename. For this, forward slashes are replaced
 * by underscores; the other remaining reserved characters, excluding
 * the colon but including the tilde character, are replaced using
 * their percent-encoded form.
 *
 * Return value: A newly allocated, escaped version of @uri. The returned
 *               string should be freed when no longer needed.
 */
gchar *apt_utils_uri_to_filename(const gchar *uri)
{
    GString *string;
    const gchar *userinfo_end;
    gsize i;

    g_return_val_if_fail(uri != NULL, NULL);

    uri = strstr(uri, ":/");

    g_return_val_if_fail(uri != NULL, NULL);

    /* Skip :// or :/ after the scheme */
    uri += 2;
    if (*uri == '/')
        uri++;

    /* Skip user information */
    userinfo_end = strchr(uri, '@');
    if (userinfo_end != NULL && userinfo_end < strchr(uri, '/'))
        uri = userinfo_end + 1;

    string = g_string_sized_new(strlen(uri) + 10);

    g_string_append_uri_escaped(string, uri, "/:", FALSE);

    for (i = 0; i < string->len; i++) {
        switch (string->str[i]) {
        case '/':
            string->str[i] = '_';
            break;
        case '~':
            string->str[i] = '%';
            g_string_insert_len(string, i + 1, "7e", 2);
            i += 2;
        }
    }

    return g_string_free(string, FALSE);
}

const gchar *apt_get_version()
{
    return PACKAGE_VERSION;
}
