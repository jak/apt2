/* cache-builder.h - Internal Cache Layout of APT2
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_CACHE_BUILDER_H
#define __APT_CACHE_BUILDER_H

#include <apt/cache.h>
#include <apt/cache-internal.h>

#include <glib.h>

G_BEGIN_DECLS

typedef struct _AptCacheBuilder AptCacheBuilder;

coffset apt_cache_builder_new_string(AptCacheBuilder *builder, const gchar *s,
                                     gssize len, gboolean dedup);
AptCachePackage *apt_cache_builder_new_package(AptCacheBuilder *builder);
AptCacheDependency *apt_cache_builder_new_dependency(AptCacheBuilder *builder,
                                                     cindex *index);
AptCacheProvides *apt_cache_builder_new_provides(AptCacheBuilder *builder,
                                                 cindex *index);
AptCachePackageFile *apt_cache_builder_new_file(AptCacheBuilder *builder);
AptCacheDescription *apt_cache_builder_new_description(AptCacheBuilder *builder,
                                                       cindex *index);
AptCacheGroupMember *apt_cache_builder_new_group_member(AptCacheBuilder
                                                        *builder,
                                                        AptCacheGroup *group,
                                                        AptCacheGroupMemberType
                                                        type,
                                                        cindex *index);
AptCacheGroup *apt_cache_builder_new_group(AptCacheBuilder *builder,
                                           const gchar *name,
                                           gssize length);

AptCacheBuilder *apt_cache_builder_new(void);
AptCache *apt_cache_builder_end(AptCacheBuilder *builder);

G_END_DECLS
#endif                          /* __APT_CACHE_BUILDER_H */
