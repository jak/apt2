/* policy.c - APT Policy handling.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include <apt/controlfile.h>
#include <apt/policy.h>
#include <apt/utils.h>
#include "policy-internal.h"

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n-lib.h>

/**
 * SECTION:policy
 * @short_description: Package pinning.
 * @title: AptPolicy
 * @include: apt.h
 *
 * This section documents the package pinning functionality in APT2. In APT,
 * priorities are assigned to package files (represented by #AptPackageFile)
 * and packages (represented by #AptPackage). A priority can be any 16-bit
 * integer except for 0 (which is used for unset priorities).
 *
 * The pin priority of a package is determined by looking at the specific
 * pin first. If the priority of this pin is not 0, the priority is
 * found. If it is 0, the pin priority of the package equals the maximum
 * pin priority of all package files this package belongs to.
 *
 * The pin priorities are used when packages should be installed. Given a
 * set of packages which can satisfy a request, the one with the highest
 * priority is the 'candidate' that shall be installed.
 **/

/**
 * APT_POLICY_ERROR:
 *
 * The error domain of the policy handling.
 **/
GQuark apt_policy_error_quark(void)
{
    return g_quark_from_static_string("apt-policy-error-quark");
}

/**
 * apt_policy_new:
 * @cache: The #AptCache the policy belongs to.
 * @compare_versions: A function to compare version strings.
 *
 * Creates a new #AptPolicy. The created #AptPolicy holds a reference to
 * @cache.
 *
 * Return value: A newly allocated #AptPolicy.
 */
AptPolicy *apt_policy_new(AptCache *cache, GCompareFunc compare_versions)
{
    gsize index;
    AptPolicy *policy;

    g_return_val_if_fail(cache != NULL, NULL);
    g_return_val_if_fail(compare_versions != NULL, NULL);

    policy = g_slice_new(AptPolicy);
    policy->cache = apt_cache_ref(cache);
    policy->n_packagefiles = apt_cache_n_packagefiles(cache);
    policy->package_pins = g_new0(gint16, apt_cache_n_packages(cache));
    policy->packagefile_pins = g_new0(gint16, policy->n_packagefiles);
    policy->packagefiles = g_new(AptPackageFile *, policy->n_packagefiles);
    policy->default_packagefile_pins = g_new(gint16, policy->n_packagefiles);
    policy->pin_regex = PIN_REGEX;
    policy->package_regex = PACKAGE_REGEX;
    policy->compare_versions = compare_versions;
    policy->ref_count = 1;

    g_assert(policy->pin_regex != NULL);
    g_assert(policy->package_regex != NULL);

    /* Load the defaults into the policy. */
    for (index = 0; index < policy->n_packagefiles; index++) {
        AptPackageFile *file = apt_cache_get_package_file(cache, index);
        AptPackageFileFlags flags = apt_package_file_get_flags(file);
        if (flags & APT_PACKAGE_FILE_NOT_AUTOMATIC)
            policy->default_packagefile_pins[index] = 1;
        else if (flags & APT_PACKAGE_FILE_NOT_SOURCE)
            policy->default_packagefile_pins[index] = 100;
        else
            policy->default_packagefile_pins[index] = 500;

        policy->packagefiles[index] = file;
    }
    return policy;
}

/**
 * apt_policy_unref:
 * @policy: An #AptPolicy
 *
 * Decrements the reference count of @policy. If the reference count drops to
 * 0, frees the memory allocated by @policy.
 **/
void apt_policy_unref(AptPolicy *policy)
{
    g_return_if_fail(policy != NULL);
    if (g_atomic_int_dec_and_test(&policy->ref_count)) {
        gsize i = 0;
        for (i = 0; i < policy->n_packagefiles; i++)
            apt_package_file_unref(policy->packagefiles[i]);

        apt_cache_unref(policy->cache);
        g_regex_unref(policy->pin_regex);
        g_regex_unref(policy->package_regex);
        g_free(policy->package_pins);
        g_free(policy->packagefile_pins);
        g_free(policy->packagefiles);
        g_free(policy->default_packagefile_pins);
        g_slice_free(AptPolicy, policy);
    }
}

/**
 * apt_policy_ref:
 * @policy: An #AptPolicy
 *
 * Increments the reference count of @policy.
 *
 * Return value: @policy.
 **/
AptPolicy *apt_policy_ref(AptPolicy *policy)
{
    g_return_val_if_fail(policy != NULL, NULL);
    g_atomic_int_inc(&policy->ref_count);
    return policy;
}

static void apt_policy_insert_pin(AptPolicy *policy, const AptPin *pin)
{
    gint16 *matched_packagefiles;

    g_assert(policy != NULL);
    g_assert(pin != NULL);

    matched_packagefiles = g_newa(gint16, policy->n_packagefiles);

    /* Check matching packagefiles for release and origin pins */
    if (pin->type == APT_PIN_RELEASE || pin->type == APT_PIN_ORIGIN) {
        gsize i = 0;
        for (i = 0; i < policy->n_packagefiles; i++) {
            if (apt_pin_matches_file(pin, policy->packagefiles[i]))
                matched_packagefiles[i] = pin->priority;
            else
                matched_packagefiles[i] = 0;
        }
    }

    if (pin->packages == NULL) {
        gsize i;
        g_assert(pin->type == APT_PIN_RELEASE || pin->type == APT_PIN_ORIGIN);
        for (i = 0; i < policy->n_packagefiles; i++) {
            if (matched_packagefiles[i] && policy->packagefile_pins[i] == 0)
                policy->packagefile_pins[i] = matched_packagefiles[i];
        }
    } else {
        int i = 0;
        AptCacheFindFlags find_type = (pin->is_source_pin ?
                                       APT_CACHE_FIND_BY_SOURCE :
                                       APT_CACHE_FIND_BY_BINARY);

        for (i = 0; pin->packages[i] != NULL; i++) {
            const gchar *pname = pin->packages[i];
            gboolean must_match = FALSE;
            AptPackage *package;
            AptPackageIter *packages;
            if (strchr(pname, '*') != NULL || pname[0] == '/') {
                /* TODO: pinning by expression should use its own table */
                must_match = TRUE;
                packages = apt_cache_iterator(policy->cache);
            } else {
                packages = apt_cache_find(policy->cache, pname, find_type);
            }

            if (packages == NULL)
                continue;
            while ((package = apt_package_iter_next_value(packages))) {
                const gchar *this_name = apt_package_get_name(package);
                const gchar *this_sname = apt_package_get_source_name(package);
                gsize id = apt_package_get_id(package);
                if (policy->package_pins[id] != 0)
                    continue;
                if (must_match) {
                    if ((find_type & APT_CACHE_FIND_BY_BINARY)) {
                        if (!apt_utils_match(pname, this_name))
                            continue;
                    } else if (!apt_utils_match(pname, this_sname)) {
                        continue;
                    }
                }
                if (pin->type == APT_PIN_VERSION) {
                    const gchar *version = apt_package_get_version(package);
                    if (apt_utils_match(pin->pin.version, version))
                        policy->package_pins[id] = pin->priority;
                } else {
                    /* pin->type is ORIGIN or RELEASE now */
                    AptDescription *desc;
                    AptDescriptionIter *descs;
                    descs = apt_package_get_descriptions(package);
                    if (descs == NULL)
                        continue;
                    while ((desc = apt_description_iter_next_value(descs))) {
                        guint32 fi = apt_description_get_package_file_id(desc);
                        gint16 fprio = matched_packagefiles[fi];
                        if (fprio > policy->package_pins[id])
                            policy->package_pins[id] = fprio;
                        apt_description_unref(desc);
                    }
                    apt_description_iter_free(descs);

                }
                apt_package_unref(package);
            }
            apt_package_iter_free(packages);
        }
    }
}

/**
 * apt_policy_parse_file:
 * @policy: An #AptPolicy
 * @filename: The name of the file that shall be parsed.
 * @error: Location to store an error.
 *
 * Parses the file located at @filename and adds the new pins the file
 * defines into @policy.
 *
 * Return value: %TRUE if the file could be parsed, %FALSE if not.
 **/
gboolean apt_policy_parse_file(AptPolicy *policy, const gchar *filename,
                               GError **error)
{
    AptControlSection *section;
    GError *control_error = NULL;
    AptControlFile *file;

    g_return_val_if_fail(policy != NULL, FALSE);
    g_return_val_if_fail(filename != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    file = apt_control_file_new(filename, &control_error);
    if (file == NULL) {
        g_propagate_error(error, control_error);
        return FALSE;
    }

    while ((section = apt_control_file_step(file))) {
        AptPin *pin;
        pin = apt_pin_new(policy, section, &control_error);
        if (pin == NULL) {
            g_propagate_error(error, control_error);
            apt_control_section_unref(section);
            apt_control_file_free(file);
            return FALSE;
        }

        apt_policy_insert_pin(policy, pin);
        apt_pin_free(pin);
        apt_control_section_unref(section);
    }
    apt_control_file_free(file);
    return TRUE;
}

/**
 * apt_policy_parse_dir:
 * @policy: An #AptPolicy
 * @dirname: The path to the directory.
 * @error: Location to store an error.
 *
 * Parses all the files in the directory at @dirname by calling
 * apt_policy_parse_file() for every file. The function stops
 * immediately if it encounters an invalid file.
 *
 * Return value: %TRUE if all files could be parsed, %FALSE otherwise.
 **/
gboolean apt_policy_parse_dir(AptPolicy *policy, const gchar *dirname,
                              GError **error)
{
    g_return_val_if_fail(policy != NULL, FALSE);
    g_return_val_if_fail(dirname != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);

    return apt_utils_foreach_in_dir(dirname, ".pref", (AptUtilsForeachInDirFunc)
                                    apt_policy_parse_file, policy, error);
}

/**
 * apt_policy_get_priority:
 * @policy: An #AptPolicy
 * @package: The #AptPackage for which the policy shall be retrieved.
 *
 * Gets the priority of the given package.
 **/
gint apt_policy_get_priority(AptPolicy *policy, AptPackage *package)
{
    gint16 pin;

    g_return_val_if_fail(policy != NULL, 0);
    g_return_val_if_fail(package != NULL, 0);

    pin = policy->package_pins[apt_package_get_id(package)];
    if (G_LIKELY(pin == 0)) {
        AptDescription *desc;
        AptDescriptionIter *descs;
        pin = G_MININT16;
        descs = apt_package_get_descriptions(package);
        while ((desc = apt_description_iter_next_value(descs))) {
            guint32 pfid = apt_description_get_package_file_id(desc);
            gint16 npin = apt_policy_get_package_file_priority(policy, pfid);
            if (npin > pin)
                pin = npin;
            apt_description_unref(desc);
        }
        apt_description_iter_free(descs);
    }
    return pin;
}

/**
 * apt_policy_get_package_file_priority:
 * @policy: An #AptPolicy
 * @package_file: The id of the package file.
 *
 * Gets the priority assigned to the package file.
 *
 * Return value: The priority.
 **/
gint apt_policy_get_package_file_priority(AptPolicy *policy,
                                          guint8 package_file)
{
    g_return_val_if_fail(policy != NULL, 0);
    g_return_val_if_fail(package_file < policy->n_packagefiles, 0);

    if (policy->packagefile_pins[package_file] != 0)
        return policy->packagefile_pins[package_file];
    return policy->default_packagefile_pins[package_file];
}

/**
 * apt_policy_get_candidate:
 * @policy: An #AptPolicy
 * @name: The name of the package to retrieve.
 * @architecture: The architecture of the package to retrieve.
 *
 * Gets the #AptPackage with the name @name and the architecture @architecture
 * that has the highest priority. Only packages with a priority >= 0 will be
 * considered a valid candidate. If there is no package with a priority >= 0,
 * %NULL is returned. If there are two packages with equal priority, the one
 * with the higher version number is chosen.
 *
 * If a matching package is installed, it will be preferred to packages
 * with lower versions, unless those packages have a priority greater
 * than 1000.
 *
 * Return value: The best #AptPackage or %NULL.
 **/
AptPackage *apt_policy_get_candidate(AptPolicy *policy, const gchar *name,
                                     const gchar *architecture)
{
    gint16 priority = -1;
    gboolean installed = FALSE;
    const gchar *version = "";
    AptPackage *result = NULL;
    AptPackage *package;
    AptPackageIter *packages;

    g_return_val_if_fail(policy != NULL, NULL);
    g_return_val_if_fail(name != NULL, NULL);
    g_return_val_if_fail(architecture == NULL, NULL);   /* not supported yet */

    packages = apt_cache_find(policy->cache, name, APT_CACHE_FIND_BY_BINARY);
    if (packages == NULL)
        return NULL;
    /* TODO: Handle multi-arch (using NULL and "any") */
    (void) architecture;
    while ((package = apt_package_iter_next_value(packages))) {
        gint16 this_priority = apt_policy_get_priority(policy, package);
        const gchar *this_version = apt_package_get_version(package);
        gboolean this_installed = (apt_package_get_current_state(package)
                                   == APT_CURRENT_STATE_INSTALLED);

        if (this_priority < priority)
            goto next;
        if (this_priority < 1000
            && (this_priority == priority || this_installed < installed)
            && policy->compare_versions(this_version, version) < 0)
            goto next;
        {
            priority = this_priority;
            version = this_version;
            installed = this_installed;
            if (result != NULL)
                apt_package_unref(result);
            result = apt_package_ref(package);
        }
      next:
        apt_package_unref(package);
    }
    apt_package_iter_free(packages);
    return result;
}

/**
 * apt_policy_set_default_release:
 * @policy: An #AptPolicy
 * @release: The codename or archive of the release
 *
 * Sets the priority of all packages belonging to the release to
 * 990. For this to be effective as documented in apt_preferences(5),
 * you must call this function before any of the parser functions,
 * as already existing priorities are not changed. If you use
 * #AptSession, this function will be called for the value defined
 * in the configuration variable 'APT::Default-Release'.
 **/
void apt_policy_set_default_release(AptPolicy *policy, const gchar *release)
{
    AptPin pin = { NULL, 990, FALSE, APT_PIN_RELEASE,
        {{NULL, NULL, NULL, NULL, NULL, NULL}}
    };

    g_return_if_fail(policy != NULL);
    g_return_if_fail(release != NULL);

    pin.pin.release.archive = (gchar *) release;
    apt_policy_insert_pin(policy, &pin);

    pin.pin.release.archive = NULL;
    pin.pin.release.codename = (gchar *) release;
    apt_policy_insert_pin(policy, &pin);
}

/* < private >
 * apt_pin_free:
 * @pin: An #AptPin
 *
 * Frees @pin.
 */
void apt_pin_free(AptPin *pin)
{
    g_assert(pin != NULL);

    g_strfreev(pin->packages);
    switch (pin->type) {
    case APT_PIN_RELEASE:
        g_free(pin->pin.release.archive);
        g_free(pin->pin.release.codename);
        g_free(pin->pin.release.component);
        g_free(pin->pin.release.label);
        g_free(pin->pin.release.origin);
        g_free(pin->pin.release.version);
        break;
    case APT_PIN_ORIGIN:
        g_free(pin->pin.origin);
        break;
    case APT_PIN_VERSION:
        g_free(pin->pin.version);
        break;
    }
    g_slice_free(AptPin, pin);
}

/* < private >
 * apt_pin_new:
 * @policy: An #AptPolicy
 * @section: The #AptControlSection that should be parsed.
 * @error: Location to store a #GError or %NULL.
 *
 * Parses the given section and returns an #AptPin or %NULL if the
 * parsing failed.
 *
 * Return value: A new #AptPin or %NULL.
 */
AptPin *apt_pin_new(AptPolicy *policy, AptControlSection *section,
                    GError **error)
{
    GError *regex_error = NULL;
    AptPin *pin;
    gsize pin_value_len, pin_package_len, pin_source_len;
    gint64 priority;
    gchar *priority_end;
    const gchar *pin_value;
    const gchar *pin_package;
    const gchar *pin_source;

    g_assert(policy != NULL);
    g_assert(section != NULL);
    g_assert(error != NULL);

    pin_value = apt_control_section_get_raw(section, "Pin", FALSE,
                                            &pin_value_len);
    pin_package = apt_control_section_get_raw(section, "Package", FALSE,
                                              &pin_package_len);
    pin_source = apt_control_section_get_raw(section, "Source", FALSE,
                                             &pin_source_len);

    if (!((pin_package == NULL) ^ (pin_source == NULL))) {
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_MISSING_FIELD,
                    _("%s:%d: Must specify either a 'Package' or a 'Source' "
                      "field (and not both)"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, NULL));

        return NULL;
    }
    if (pin_value == NULL) {
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_MISSING_FIELD,
                    _("%s:%d: The required field 'Pin' is not specified"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, NULL));
        return NULL;
    }

    if (!apt_control_section_has_key(section, "Pin-Priority")) {
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_MISSING_FIELD,
                    _("%s:%d: The required field 'Pin-Priority' is not "
                      "specified"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, NULL));
        return NULL;
    }

    pin = g_slice_new0(AptPin);
    pin->is_source_pin = (pin_source != NULL);
    if (strncmp((pin_package ? pin_package : pin_source), "*",
                (pin_package ? pin_package_len : pin_source_len)) != 0)
        pin->packages = g_regex_split_full(policy->package_regex,
                                           pin_package ? pin_package :
                                           pin_source,
                                           pin_package ? pin_package_len :
                                           pin_source_len, 0, 0, -1, NULL);
    else
        pin->packages = NULL;

    if (strncmp(pin_value, "version ", 8) == 0 && pin_value_len > 8) {
        pin->type = APT_PIN_VERSION;
        pin->pin.version = g_strndup(pin_value + 8, pin_value_len - 8);
        g_assert(pin->pin.version != NULL);
    } else if (strncmp(pin_value, "origin ", 7) == 0 && pin_value_len > 7) {
        pin->type = APT_PIN_ORIGIN;
        pin->pin.origin = g_strndup(pin_value + 7, pin_value_len - 7);
        g_assert(pin->pin.origin != NULL);
    } else if (strncmp(pin_value, "release ", 8) == 0 && pin_value_len > 8) {
        GMatchInfo *match;
        gint start, end;
        pin->type = APT_PIN_RELEASE;
        if (!g_regex_match_full(policy->pin_regex, pin_value,
                                pin_value_len, 8, 0, &match, NULL)) {
            g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN,
                        _("%s:%d: Invalid Pin release value: %s"),
                        apt_control_section_get_filename(section),
                        apt_control_section_get_lineno(section, "Pin"),
                        pin_value + 8);
            goto error;
        }
        do {
            gint type_start, type_end;
            gchar *value = g_match_info_fetch(match, 2);

            /* Fetch the type */
            g_match_info_fetch_pos(match, 1, &type_start, &type_end);
            g_assert_cmpint(type_end - type_start, ==, 1);
            /* Fetch the end (used for error checking when end != start). */
            g_match_info_fetch_pos(match, 3, &start, &end);
            switch (pin_value[type_start]) {
            case 'a':
                pin->pin.release.archive = value;
                break;
            case 'n':
                pin->pin.release.codename = value;
                break;
            case 'c':
                pin->pin.release.component = value;
                break;
            case 'l':
                pin->pin.release.label = value;
                break;
            case 'o':
                pin->pin.release.origin = value;
                break;
            case 'v':
                pin->pin.release.version = value;
                break;
            default:
                g_set_error(error, APT_POLICY_ERROR,
                            APT_POLICY_ERROR_INVALID_PIN,
                            _("%s:%d: Invalid Pin release attribute %c"),
                            apt_control_section_get_filename(section),
                            apt_control_section_get_lineno(section, "Pin"),
                            pin_value[type_start]);
                g_free(value);
                g_match_info_free(match);
                goto error;
            }
        } while (g_match_info_next(match, &regex_error));

        g_match_info_free(match);

        /* The end of the line occupies no space (end == start), so if
         * end != start, the value is incomplete. */
        if (end != start) {
            g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN,
                        _("%s:%d: Incomplete pin: %s"),
                        apt_control_section_get_filename(section),
                        apt_control_section_get_lineno(section, "Pin"),
                        pin_value);
            goto error;
        }

        if (regex_error != NULL) {
            g_propagate_error(error, regex_error);
            goto error;
        }
    } else {
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN,
                    _("%s:%d: Unknown or incomplete pin: '%s'"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, "Pin"), pin_value);
        goto error;
    }
    if (pin->type == APT_PIN_VERSION && pin->packages == NULL) {
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN,
                    _("%s:%d: Using Pin: version is not supported for "
                      "Package: * entries"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, "Pin"));
        goto error;
    }

    errno = 0;
    priority = g_ascii_strtoll(apt_control_section_get_raw(section,
                                                           "Pin-Priority",
                                                           FALSE, NULL),
                               &priority_end, 10);
    if (priority < G_MININT16 || priority > G_MAXINT16) {
        gint err_no = errno ? errno : ERANGE;
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN,
                    _("%s:%d: Invalid Pin-Priority: \"%s\" (%s)"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, "Pin-Priority"),
                    apt_control_section_get(section, "Pin-Priority"),
                    g_strerror(err_no));
        goto error;
    } else if (*priority_end != '\n') {
        g_set_error(error, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN,
                    _("%s:%d: Invalid Pin-Priority: \"%s\" (not a number)"),
                    apt_control_section_get_filename(section),
                    apt_control_section_get_lineno(section, "Pin-Priority"),
                    apt_control_section_get(section, "Pin-Priority"));
        goto error;
    } else {
        pin->priority = priority;
    }
    return pin;
  error:
    apt_pin_free(pin);
    return NULL;
}

/* < private >
 * MATCH:
 * @release: An #AptReleaseInfo
 * @pf: An #AptPackageFile
 * @what: The name of an attribute.
 *
 * Checks that release.what matches apt_package_file_get_##what().
 */
#define MATCH(release, pf, what) \
    (release.what != NULL ? apt_utils_match(release.what, \
                                      apt_package_file_get_##what(pf)) : \
                                       TRUE)

/* <private>
 * apt_pin_matches_file:
 * @pin: An #AptPin
 * @packagefile: The #AptPackageFile to check again.
 *
 * Checks that @pin matches @packagefile.
 *
 * Return value: %TRUE if @pin matches @packagefile, %FALSE otherwise.
 */
gboolean apt_pin_matches_file(const AptPin *pin, AptPackageFile *packagefile)
{
    g_assert(pin != NULL);
    g_assert(packagefile != NULL);

    if (pin->type == APT_PIN_ORIGIN)
        return apt_utils_match(pin->pin.origin,
                               apt_package_file_get_site(packagefile));
    return (MATCH(pin->pin.release, packagefile, archive) &&
            MATCH(pin->pin.release, packagefile, codename) &&
            MATCH(pin->pin.release, packagefile, codename) &&
            MATCH(pin->pin.release, packagefile, component) &&
            MATCH(pin->pin.release, packagefile, label) &&
            MATCH(pin->pin.release, packagefile, origin) &&
            MATCH(pin->pin.release, packagefile, version));
}

#undef MATCH
