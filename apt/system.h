/* system.h - Dynamically loaded modules for system-specific parts.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_SYSTEM_H__
#define __APT_LIB_SYSTEM_H__

#include <glib.h>

#include <apt/configuration.h>

G_BEGIN_DECLS

/* typedef for AptSystem */
typedef struct _AptSystem AptSystem;

/**
 * AptSystemError:
 * @APT_SYSTEM_ERROR_LOCKED: Indicates that the system is locked. Please note
 *                           that in apt_system_lock(), other error values may
 *                           be used to indicate a locked system, this depends
 *                           on how locking is implemented in the underlying
 *                           package manager.
 * @APT_SYSTEM_ERROR_FAILED: A generic error value that indicates any kind of
 *                           error.
 *
 * Possible errors of #AptSystem.
 */
typedef enum {
    APT_SYSTEM_ERROR_LOCKED,
    APT_SYSTEM_ERROR_FAILED
} AptSystemError;

extern GQuark apt_system_error_quark(void);
#define APT_SYSTEM_ERROR apt_system_error_quark()

AptSystem *apt_system_new(const gchar *name, AptConfiguration *configuration,
                          GError **error);
AptSystem *apt_system_ref(AptSystem *system);
void apt_system_unref(AptSystem *system);

gint apt_system_compare_versions(AptSystem *system, const gchar *a,
                                 const gchar *b);
gboolean apt_system_lock(AptSystem *system, GError **error);
gboolean apt_system_unlock(AptSystem *system, GError **error);

const gchar *apt_system_get_author(AptSystem *system);
const gchar *apt_system_get_description(AptSystem *system);
GCompareFunc apt_system_get_version_compare_func(AptSystem *system);

G_END_DECLS
#endif                          /* __APT_LIB_SYSTEM_H__ */
