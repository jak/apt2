/* system.c - Dynamically loaded modules for system-specific parts.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include <apt/system.h>
#include <apt/system-internal.h>

#include <glib.h>
#include <gmodule.h>
#include <glib/gi18n-lib.h>

/**
 * SECTION:system
 * @short_description: Dynamically loaded modules for system-specific parts.
 * @title: AptSystem
 * @include: apt.h
 *
 * APT2 uses dynamically loaded modules for performing the system-specific
 * parts like parsing index files or communicating with package managers
 * such as dpkg or rpm. By dynamically loading those modules at run-time,
 * distributions can provide support for APT2 with multiple different
 * package management systems which can be useful when creating chroots
 * or maintaining repositories for multiple distributions, or simply for
 * testing purposes. Most users of this API should load the module defined
 * in the configuration variable Apt::System.
 **/

#ifdef APT_LOCAL
gboolean apt_system_force_local = TRUE;
#else
gboolean apt_system_force_local = FALSE;
#endif

/**
 * APT_SYSTEM_ERROR:
 *
 * The error domain of #AptSystem.
 **/
GQuark apt_system_error_quark(void)
{
    return g_quark_from_static_string("apt-system-error-quark");
}

/**
 * apt_system_new:
 * @name: The name of the system module.
 * @configuration: The #AptConfiguration on which the module shall
 *                 get/set options.
 * @error: Return location to store a #GError.
 *
 * Creates a new #AptSystem by opening the module @name in the
 * module directory. This function should not be considered thread-safe
 * with regards to @configuration, as the system may change options.
 *
 * If the module could not be opened, or the initialization of the
 * module failed, %NULL is returned and the location pointed to by
 * @error is set to a #GError describing the error.
 *
 * Return value: A new #AptSystem with a reference count of 1 or %NULL if
 *               no #AptSystem could be created.
 */
AptSystem *apt_system_new(const gchar *name, AptConfiguration *configuration,
                          GError **error)
{
    AptSystem *system;
    gchar *path;

    g_return_val_if_fail(name != NULL, NULL);

    if (apt_system_force_local) {
        gchar *dir;
        dir = g_build_filename(PACKAGE_BUILD_DIR, "systems", name, ".libs",
                               NULL);
        path = g_module_build_path(dir, name);
        g_free(dir);
    } else {
        path = g_module_build_path(APT_SYSTEM_MODULE_PATH, name);
    }
    system = apt_system_new_for_path(path, configuration, error);

    g_free(path);
    return system;
}

/* < internal >
 * apt_system_new_for_path:
 * @path: The path to the module, will be passed to g_module_open() as it
 * @configuration: The #AptConfiguration the system shall use
 * @error: Return location for a #GError
 *
 * Creates a new #AptSystem for the module at @path.
 *
 * Return value: A new #AptSystem or %NULL on failure.
 */
AptSystem *apt_system_new_for_path(const gchar *path,
                                   AptConfiguration *configuration,
                                   GError **error)
{
    AptSystem *system = NULL;
    const AptSystemDesc *desc;
    GModule *module;

    g_return_val_if_fail(path != NULL, NULL);
    g_return_val_if_fail(configuration != NULL, NULL);
    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    if ((module = g_module_open(path, 0)) == NULL ||
        !g_module_symbol(module, "apt_system_desc", (gpointer) &desc)) {
        g_set_error(error, APT_SYSTEM_ERROR, APT_SYSTEM_ERROR_FAILED,
                    _("Could not load module %s: %s"), path, g_module_error());
        goto error;
    }

    system = g_slice_alloc0(desc->size);
    system->desc = desc;
    system->configuration = apt_configuration_ref(configuration);
    system->module = module;
    system->ref_count = 1;
    if (APT_SYSTEM_DESC(system)->initialize != NULL)
        if (!APT_SYSTEM_DESC(system)->initialize(system, error))
            goto error;

    return system;

  error:
    if (system != NULL)
        apt_system_unref(system);
    return NULL;
}

/**
 * apt_system_ref:
 * @system: An #AptSystem
 *
 * Increments the reference count of @system.
 *
 * Return value: @system
 */
AptSystem *apt_system_ref(AptSystem *system)
{
    g_return_val_if_fail(system != NULL, NULL);

    return (g_atomic_int_inc(&system->ref_count), system);
}

/**
 * apt_system_unref:
 * @system: An #AptSystem
 *
 * Decrements the reference count of @system. If the reference count drops to
 * 0, the memory allocated for @system is freed and the module is closed.
 */
void apt_system_unref(AptSystem *system)
{
    g_return_if_fail(system != NULL);
    g_return_if_fail(APT_SYSTEM_DESC(system) != NULL);

    if (g_atomic_int_dec_and_test(&system->ref_count)) {
        GModule *module = system->module;
        if (APT_SYSTEM_DESC(system)->destroy != NULL)
            APT_SYSTEM_DESC(system)->destroy(system);
        apt_configuration_unref(system->configuration);
        g_slice_free1(APT_SYSTEM_DESC(system)->size, system);
        g_module_close(module);
    }
}

/**
 * apt_system_compare_versions:
 * @system: An #AptSystem.
 * @a: The first version.
 * @b: The second version.
 *
 * Compares the two version strings @a and @b and return -1 if @a is less
 * than @b, 0 if they are equal, and 1 if @a is greater than @b.
 **/
gint apt_system_compare_versions(AptSystem *system, const gchar *a,
                                 const gchar *b)
{
    g_return_val_if_fail(system != NULL, 0);
    g_return_val_if_fail(APT_SYSTEM_DESC(system) != NULL, 0);
    g_assert(APT_SYSTEM_DESC(system)->compare_versions != NULL);

    return APT_SYSTEM_DESC(system)->compare_versions(a, b);
}

/**
 * apt_system_lock:
 * @system: An #AptSystem.
 * @error: Location to store an error.
 *
 * Locks @system globally; that is, no other process
 * may install/remove packages and no other thread in
 * the same process may lock the object. Note that this
 * may not prevent another #AptSystem in the same
 * process to be locked.
 *
 * If another process already holds a lock on the system
 * or the system could not be locked due to other reasons
 * such as file permission issues; %FALSE is returned
 * and the #GError pointed to by @error is set to something
 * indicating the lower error. If this specific #AptSystem
 * is already locked inside the same program, %FALSE is
 * returned as well and @error will be an #AptSystemError
 * with the error code APT_SYSTEM_ERROR_LOCKED.
 *
 * Return value: %TRUE if locked, %FALSE otherwise.
 */
gboolean apt_system_lock(AptSystem *system, GError **error)
{
    g_return_val_if_fail(system != NULL, FALSE);
    g_return_val_if_fail(APT_SYSTEM_DESC(system) != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);
    g_assert(APT_SYSTEM_DESC(system)->lock != NULL);

    if (!g_bit_trylock(&system->state, 0)) {
        g_set_error(error, APT_SYSTEM_ERROR, APT_SYSTEM_ERROR_LOCKED,
                    _("System is already locked in the program."));
        return FALSE;
    }
    if (!APT_SYSTEM_DESC(system)->lock(system, error))
        g_bit_unlock(&system->state, 0);

    return TRUE;
}

/**
 * apt_system_unlock:
 * @system: An #AptSystem.
 * @error: Location to store an error.
 *
 * Unlocks @system, allowing other threads or
 * processes to lock it again. If an error
 * occurs, %FALSE is returned and @error is
 * set to another program
 *
 * Return value: %TRUE if unlocked, %FALSE otherwise.
 */
gboolean apt_system_unlock(AptSystem *system, GError **error)
{
    gboolean result;

    g_return_val_if_fail(system != NULL, FALSE);
    g_return_val_if_fail(APT_SYSTEM_DESC(system) != NULL, FALSE);
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);
    g_assert(APT_SYSTEM_DESC(system)->unlock != NULL);

    result = APT_SYSTEM_DESC(system)->unlock(system, error);
    g_bit_unlock(&system->state, 0);

    return result;
}

/**
 * apt_system_get_version_compare_func:
 * @system: An #AptSystem
 *
 * Gets the #GCompareFunc that implements apt_system_compare_versions(), as
 * needed for apt_policy_new().
 *
 * Return value: A #GCompareFunc for comparing two version strings.
 **/
GCompareFunc apt_system_get_version_compare_func(AptSystem *system)
{
    g_return_val_if_fail(system != NULL, 0);
    g_return_val_if_fail(APT_SYSTEM_DESC(system) != NULL, 0);
    g_assert(APT_SYSTEM_DESC(system)->compare_versions != NULL);

    return (GCompareFunc) APT_SYSTEM_DESC(system)->compare_versions;
}

/**
 * apt_system_get_author:
 * @system: An #AptSystem
 *
 * Gets the author of the system module.
 *
 * Return value: Constant string naming the author of the module.
 **/
const gchar *apt_system_get_author(AptSystem *system)
{
    g_return_val_if_fail(system != NULL, 0);
    g_return_val_if_fail(APT_SYSTEM_DESC(system) != NULL, 0);

    return APT_SYSTEM_DESC(system)->author;
}

/**
 * apt_system_get_description:
 * @system: An #AptSystem
 *
 * Gets the description of the system module.
 *
 * Return value: Constant string describing the module in one line.
 **/
const gchar *apt_system_get_description(AptSystem *system)
{
    g_return_val_if_fail(system != NULL, 0);
    g_return_val_if_fail(APT_SYSTEM_DESC(system) != NULL, 0);

    return APT_SYSTEM_DESC(system)->description;
}
