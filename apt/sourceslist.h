/* sourceslist.h - Parser for /etc/apt/sources.list
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_SOURCESLIST_H__
#define __APT_LIB_SOURCESLIST_H__

#include <apt/system.h>

#include <glib.h>

G_BEGIN_DECLS

typedef struct _AptSource AptSource;
typedef struct _AptSourcesList AptSourcesList;

AptSourcesList *apt_sources_list_new(AptSystem *system);
AptSourcesList *apt_sources_list_ref(AptSourcesList * list);
void apt_sources_list_unref(AptSourcesList * list);
gboolean apt_sources_list_parse_file(AptSourcesList * list, const gchar *file,
                                     GError **error);
gboolean apt_sources_list_parse_dir(AptSourcesList * list, const gchar *dir,
                                    GError **error);
gboolean apt_sources_list_parse_line(AptSourcesList * list, const gchar *line,
                                     GError **error);
GList *apt_sources_list_get_list(AptSourcesList * list);

G_END_DECLS
#endif                          /* __APT_LIB_SOURCESLIST_H__ */
