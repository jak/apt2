/* policy.h - APT Policy handling.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_POLICY_H__
#define __APT_LIB_POLICY_H__

#include <apt/cache.h>
#include <glib.h>

G_BEGIN_DECLS

typedef struct _AptPolicy AptPolicy;

/**
 * AptPolicyError:
 * @APT_POLICY_ERROR_MISSING_FIELD: One of the fields 'Pin', 'Pin-Priority'
 *                                  is missing; 'Package' and 'Source' are
 *                                  both given; neither 'Package' nor 'Source'
 *                                  is given.
 * @APT_POLICY_ERROR_INVALID_PIN: The pin is invalid. This can have various
 *                                reasons such as unknown pin types, invalid
 *                                attributes or combinining a 'Pin: version'
 *                                with 'Package *'.
 *
 * Possible errors of the apt preferences file parser.
 */
typedef enum {
    APT_POLICY_ERROR_MISSING_FIELD,
    APT_POLICY_ERROR_INVALID_PIN
} AptPolicyError;

extern GQuark apt_policy_error_quark(void);
#define APT_POLICY_ERROR apt_policy_error_quark()

AptPolicy *apt_policy_new(AptCache *cache, GCompareFunc compare_versions);
AptPolicy *apt_policy_ref(AptPolicy *policy);
void apt_policy_unref(AptPolicy *policy);
AptPackage *apt_policy_get_candidate(AptPolicy *policy, const gchar *name,
                                     const gchar *architecture);
gint apt_policy_get_priority(AptPolicy *policy, AptPackage *package);
gint apt_policy_get_package_file_priority(AptPolicy *policy,
                                          guint8 package_file);
gboolean apt_policy_parse_file(AptPolicy *policy, const gchar *filename,
                               GError **error);
gboolean apt_policy_parse_dir(AptPolicy *policy, const gchar *dirname,
                              GError **error);
void apt_policy_set_default_release(AptPolicy *policy, const gchar *release);

G_END_DECLS
#endif                          /* __APT_LIB_POLICY_H__ */
