/* sourceslist.c - Parser for /etc/apt/sources.list
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <apt/sourceslist.h>
#include <apt/utils.h>

#include <glib.h>

/**
 * SECTION:sourceslist
 * @short_description: Parse sources.list files and create #AptSource
 *                     objects.
 * @title: AptSourcesList
 * @include: apt.h
 *
 * #AptSourcesList parses sources.list files and creates the corresponding
 * #AptSource instance for each entry, if such entry is supported by the
 * underlying #AptSystem.
 **/

/**
 * AptSource:
 * @type: The type of the source; for example, "deb".
 * @uri: The location of the source, as an URI.
 * @distribution:The distribution that shall be fetched.
 * @components: The components.
 * @options: Various key=value options given in the options field.
 *
 * #AptSource is a data structure that describes a single entry in a
 * sources.list file.
 */
struct _AptSource {
    gchar *type;
    gchar *uri;
    gchar *distribution;
    gchar **components;
    GHashTable *options;
    /* < private > */
    gint ref_count;
};

/**
 * AptSourcesList:
 *
 * A list of #AptSource objects.
 */
struct _AptSourcesList {
    AptSystem *system;
    GRegex *option_parser;
    GRegex *line_parser;
    GList *sources;
    gint ref_count;
};

/**
 * apt_sources_list_new:
 * @system: The #AptSystem used for the sources.
 *
 * Creates a new #AptSourcesList.
 *
 * Return value: A newly allocated #AptSourcesList.
 **/
AptSourcesList *apt_sources_list_new(AptSystem *system)
{
    AptSourcesList *list;

    g_return_val_if_fail(system != NULL, NULL);

    list = g_slice_new0(AptSourcesList);
    list->system = apt_system_ref(system);
    list->option_parser = g_regex_new("([^=,\\]]+)=([^=,\\]]+)[,\\]]", 0, 0,
                                      NULL);
    list->line_parser = g_regex_new("^([^ ]+) +(?:\\[([^\\]]+)\\]|) *([^ ]+) +"
                                    "([^ ]+) *([^#]*).*$", 0, 0, NULL);
    list->ref_count = 1;
    return list;
}

/**
 * apt_sources_list_ref:
 * @list: An #AptSourcesList
 *
 * Increments the reference count of @list by one.
 *
 * Return value: @list
 **/
AptSourcesList *apt_sources_list_ref(AptSourcesList *list)
{
    return (g_atomic_int_inc(&list->ref_count), list);
}

/**
 * apt_sources_list_unref:
 * @list: An #AptSourcesList
 *
 * Decrements the reference count of @list by one. If the reference
 * count drops to 0, frees all memory used by @list.
 */
void apt_sources_list_unref(AptSourcesList *list)
{
    if (g_atomic_int_dec_and_test(&list->ref_count)) {
        /* g_list_foreach(list->sources, (GFunc)apt_source_unref, NULL); */
        g_list_free(list->sources);
        g_regex_unref(list->line_parser);
        g_regex_unref(list->option_parser);
        apt_system_unref(list->system);
        g_slice_free(AptSourcesList, list);
    }
}

/**
 * apt_sources_list_get_list:
 * @list: An #AptSourcesList
 *
 * Gets a list of unowned references to #AptSource objects. The list is
 * ordered according to the time the sources were entered. This means that
 * the first source is the source added first and the last source has been
 * added last.
 *
 * Return value: A #GList of unowned #AptSource references. You must call
 *               g_list_free() on the list once you do not need it anymore.
 **/
GList *apt_sources_list_get_list(AptSourcesList *list)
{
    return g_list_reverse(g_list_copy(list->sources));
}

/**
 * apt_sources_list_parse_line:
 * @list: An #AptSourcesList
 * @line: A line in the sources.list(5) format that shall be parsed
 * @error: Return location for an error
 *
 * Parses @line which shall describe a single #AptSource in the format
 * described in sources.list(5). If an error occured, %FALSE is returned
 * and @error is set (if it is not NULL).
 *
 * Return value: %TRUE if the line could be parsed. Otherwise, %FALSE.
 **/
gboolean apt_sources_list_parse_line(AptSourcesList *list, const gchar *line,
                                     GError **error)
{
    GMatchInfo *match;
    gchar *scomponents;
    AptSource source;

    while (g_ascii_isspace(*line))
        line++;

    /* Skip comment lines and empty lines */
    if (*line == '#' || *line == '\0')
        return TRUE;

    if (!g_regex_match(list->line_parser, line, 0, &match)) {
        g_set_error(error, 0, 0, "Could not parse line '%s'.", line);
        return FALSE;
    }

    source.type = g_match_info_fetch(match, 1);
    source.uri = g_match_info_fetch(match, 3);
    source.distribution = g_match_info_fetch(match, 4);
    scomponents = g_match_info_fetch(match, 5);
    source.components = g_regex_split_simple("\\s+", scomponents, 0, 0);

    list->sources = g_list_prepend(list->sources,
                                   g_slice_dup(AptSource, &source));

    g_free(scomponents);
    g_match_info_free(match);

    return TRUE;
}

/**
 * apt_sources_list_parse_file:
 * @list: An #AptSourcesList
 * @file: Path to the file that should be parsed
 * @error: Return location for an error
 *
 * Parses the file at @file. If an error occured, %FALSE is returned and
 * @error is set (if it is not NULL).
 *
 * Return value: %TRUE if the file could be parsed. Otherwise, %FALSE.
 **/
gboolean apt_sources_list_parse_file(AptSourcesList *list, const gchar *file,
                                     GError **error)
{
    gchar *line;
    gint lineno = 0;
    gsize linelen;
    GIOStatus status;
    GIOChannel *channel = g_io_channel_new_file(file, "r", error);
    if (channel == NULL)
        return FALSE;

    while ((status = g_io_channel_read_line(channel, &line, &linelen, NULL,
                                            error)) == G_IO_STATUS_NORMAL) {
        GError *line_error = NULL;
        lineno++;
        if (line[linelen - 1] == '\n')
            line[linelen - 1] = '\0';
        if (!apt_sources_list_parse_line(list, line, &line_error)) {
            g_propagate_prefixed_error(error, line_error, "%s:%d: ",
                                       file, lineno);
            g_free(line);
            break;
        }
        g_free(line);
    }

    g_io_channel_unref(channel);
    return (status == G_IO_STATUS_EOF);
}

/**
 * apt_sources_list_parse_dir:
 * @list: An #AptSourcesList
 * @dir: Path to the directory from which the files should be parsed
 * @error: Return location for an error
 *
 * Parses the files in @dir until an error occurs. In such a case, %FALSE
 * is returned and @error is set (if it is not NULL).
 *
 * Return value: %TRUE if all files could be parsed. Otherwise, %FALSE.
 **/
gboolean apt_sources_list_parse_dir(AptSourcesList *list, const gchar *dir,
                                    GError **error)
{
    return apt_utils_foreach_in_dir(dir, ".list", (AptUtilsForeachInDirFunc)
                                    apt_sources_list_parse_file, list, error);
}
