/* controlfile.c - Parser for Debian control files.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <apt/controlfile.h>
#include <apt/utils.h>

#include <string.h>

#include <glib.h>

/**
 * SECTION:controlfile
 * @short_description: Parsing files consisting of multiple RFC-822 sections.
 * @title: AptControlFile
 * @include: apt.h
 * @stability: Unstable
 *
 * This section explains the control file parser of APT2. A control file is
 * a file consisting of multiple RFC 822-style header sections. Those files
 * are widely used in the Debian project for packaging purposes and other
 * stuff as well.
 **/

/**
 * AptControlSection:
 *
 * #AptControlSection is an opaque data type and may only be accessed using
 * the following functions.
 **/
struct _AptControlSection {
    GHashTable *table;
    const gchar *start;
    const gchar *end;
    gint state;
    gint ref_count;

    gint lineno;
    gint linecount;
    gchar *filename;
};

/*
 * AptControlField:
 * @name_begin: Pointer to the beginning of the field name
 * @name_end:   Pointer to the end of the field name (the colon)
 * @value_begin: Pointer to the beginning of the field value
 * @value_end:   Pointer to the end of the field value
 * @name: Nul-terminated string from @name_begin to @name_end
 * @value: Nul-terminated string from @value_begin to @value_end
 *
 * Internal representation of a single control field.
 */
typedef struct AptControlField {
    const gchar *name_begin;
    const gchar *name_end;
    const gchar *value_begin;
    const gchar *value_end;
    /* Those are used if nul-terminated strings are requested. */
    gchar *name;
    gchar *value;
    /* Meta information */
    gint lineno;
} AptControlField;

static const gchar *apt_control_field_get_name(AptControlField *field)
{
    if (field->name == NULL)
        field->name = g_strndup(field->name_begin, field->name_end -
                                field->name_begin);
    return field->name;
}

static const gchar *apt_control_field_get_value(AptControlField *field)
{
    if (field->value == NULL)
        field->value = g_strndup(field->value_begin, field->value_end -
                                 field->value_begin);
    return field->value;
}

static void apt_control_field_free(AptControlField *field)
{
    g_free(field->name);
    g_free(field->value);
    g_slice_free(AptControlField, field);
}

/*
 * apt_rfc822_field_name_hash:
 * @v: a string key, terminated at a colon.
 *
 * Hashes the string @v. The processing of @v stops as
 * soon as a nul-byte or a colon is reached. This is
 * useful for RFC-822 header field names, as they always
 * end with a colon in a header.
 *
 * Return value: a hash value corresponding to the key
 */
static guint apt_rfc822_field_name_hash(gconstpointer v)
{
    /* 31 bit hash function */
    const signed char *p = v;
    guint32 h = g_ascii_tolower(*p);

    if (h)
        for (p += 1; *p != '\0' && *p != ':'; p++)
            h = (h << 5) - h + g_ascii_tolower(*p);

    return h;
}

/*
 * apt_rfc822_field_name_equal:
 * @a: a string key, terminated at colon or nul-byte.
 * @b: a string key, terminated at colon or nul-byte.
 *
 * Compares the two strings @a and @b which can end at colon
 * or nul-byte; behaving as if ':' == '\0'.
 *
 * Return value: %TRUE if @a and @b are equal, %FALSE otherwise.
 */
static gboolean apt_rfc822_field_name_equal(gconstpointer a, gconstpointer b)
{
#define order(c) (c == ':' ? '\0' : g_ascii_tolower(c))
    const gchar *pa = a;
    const gchar *pb = b;
    while (order(*pa) && order(*pa) == order(*pb))
        pa++, pb++;

    return order(*pa) == order(*pb);
#undef order
}

/* < private >
 * next_line:
 * @start: A string.
 * @end: The last member in the string.
 *
 * Gets the beginning of the next line in @start.
 *
 * Return value: The next line or @end if there is no next line.
 */
static const gchar *next_line(const gchar *start, const gchar *end)
{
    start = memchr(start, '\n', end - start);
    return (start) ? start + 1 : end;
}

/**
 * apt_control_section_new:
 * @section: The section that shall be parsed.
 * @length: The length of @section. It may be longer than the to be parsed
 *          section, it only exists to not read outside of the string.
 * @next:   If not %NULL, it will be set to the beginning of the body
 *          of the RFC-822 message; that is, (for our use) the next section.
 *
 * Parses a RFC 822 header section. In case multiple fields with the same
 * name exist, only the last one will be available. Any newlines in front
 * of a section are ignored.
 *
 * Return value: An #AptControlSection containing the section.
 **/
AptControlSection *apt_control_section_new(const gchar *section, gssize length,
                                           const gchar **const next)
{
    AptControlSection *object;
    const gchar *end = section + length;
    gint lineno = 0;

    g_return_val_if_fail(section != NULL, NULL);
    g_return_val_if_fail(length > 0, NULL);

    object = g_slice_new(AptControlSection);
    object->state = 0;
    object->filename = NULL;
    object->lineno = 1;
    object->ref_count = 1;
    object->table = g_hash_table_new_full(apt_rfc822_field_name_hash,
                                          apt_rfc822_field_name_equal,
                                          NULL, (GDestroyNotify)
                                          apt_control_field_free);

    /* Skip newlines and comments prior to section */
    while (section < end && (*section == '#' || *section == '\n')) {
        section = next_line(section, end);
        lineno++;
        continue;
    }

    object->start = section;

    while (section < end) {
        AptControlField field;
        /* Read a field */
        field.name_begin = section;
        field.name_end = memchr(section, ':', end - section);
        field.value_begin = field.name_end + 2;
        field.name = NULL;
        field.value = NULL;
        field.lineno = lineno++;

        while ((section = next_line(section, end)) < end && *section == ' ')
            lineno++;

        field.value_end = (section == end) ? section : section - 1;
        g_hash_table_replace(object->table, (gpointer) field.name_begin,
                             g_slice_dup(AptControlField, &field));

        if (*section == '\n')
            break;
    }

    object->end = section - 1;
    object->linecount = lineno;

    if (next != NULL) {
        while (section < end && *section == '\n')
            section++, object->linecount++;
        *next = section;
    }

    return object;
}

/**
 * apt_control_section_ref:
 * @section: An #AptControlSection
 *
 * Increments the reference count of @section by one.
 *
 * Return value: @section.
 **/
AptControlSection *apt_control_section_ref(AptControlSection *section)
{
    g_return_val_if_fail(section != NULL, NULL);
    g_atomic_int_inc(&section->ref_count);
    return section;
}

/**
 * apt_control_section_unref:
 * @section: An #AptControlSection
 *
 * Decrements the reference count of @section by one. If the reference count
 * drops to 0, the memory used by @section is freed.
 **/
void apt_control_section_unref(AptControlSection *section)
{
    g_return_if_fail(section != NULL);
    if (g_atomic_int_dec_and_test(&section->ref_count)) {
        g_hash_table_unref(section->table);
        g_slice_free(AptControlSection, section);
    }
}

/**
 * apt_control_section_get:
 * @section: An #AptControlSection
 * @name: The name of the field that shall be looked up.
 *
 * Looks up the name in the hash table and returns the value of the field,
 * excluding whitespace at the beginning and the end.
 *
 * Return value: The stripped value as a string or %NULL if the key is not
 *               set.
 **/
const gchar *apt_control_section_get(AptControlSection *section,
                                     const gchar *name)
{
    AptControlField *field;
    const gchar *value = NULL;

    g_return_val_if_fail(section != NULL, NULL);
    g_return_val_if_fail(name != NULL, NULL);

    field = g_hash_table_lookup(section->table, name);
    if (field != NULL) {
        g_bit_lock(&section->state, 0);
        value = apt_control_field_get_value(field);
        g_bit_unlock(&section->state, 0);
    }
    return value;
}

/**
 * apt_control_section_get_raw:
 * @section: An #AptControlSection
 * @name: The name of the field that shall be looked up.
 * @complete: Return the complete field, including the field name.
 * @length: Return location for the length of the return value; or %NULL
 *          if the length is not needed (e.g. for passing to atoi()).
 *
 * Looks up the name in the hash table and returns the value of the field,
 * or (if @complete is %TRUE) the complete field. This function is preferred
 * over apt_control_section_get(), because it does not require string
 * duplication.
 *
 * Return value: The field as a string or %NULL if the key is not
 *               set. The returned string is not nul-terminated.
 **/
const gchar *apt_control_section_get_raw(AptControlSection *section,
                                         const gchar *name, gboolean complete,
                                         gsize *length)
{
    AptControlField *field;
    const gchar *value = NULL;

    g_return_val_if_fail(section != NULL, NULL);
    g_return_val_if_fail(name != NULL, NULL);

    field = g_hash_table_lookup(section->table, name);
    if (field != NULL) {
        value = (complete ? field->name_begin : field->value_begin);
        if (length != NULL)
            *length = field->value_end - value;
    }
    return value;
}

/**
 * apt_control_section_get_keys:
 * @section: An #AptControlSection
 *
 * Gets a list of all keys contained in this section.
 *
 * Return value: A #GList containing the keys of this section. The data in this
 *               list is owned by the section and must not be modified. If the
 *               list is no longer needed, call g_list_free() on it.
 **/
GList *apt_control_section_get_keys(AptControlSection *section)
{
    GList *values;
    GList *current;

    g_return_val_if_fail(section != NULL, NULL);

    values = g_hash_table_get_values(section->table);

    g_bit_lock(&section->state, 0);
    for (current = values; current != NULL; current = current->next)
        current->data = (gpointer) apt_control_field_get_name(current->data);
    g_bit_unlock(&section->state, 0);
    return values;
}

/**
 * apt_control_section_get_values:
 * @section: An #AptControlSection
 *
 * Gets a list of all values contained in this section.
 *
 * Return value: A #GList containing the values of this section. The data in
 *               this list is owned by the section and must not be modified.
 *               If the list is no longer needed, call g_list_free() on it.
 **/
GList *apt_control_section_get_values(AptControlSection *section)
{
    GList *values;
    GList *current;

    g_return_val_if_fail(section != NULL, NULL);

    values = g_hash_table_get_values(section->table);

    g_bit_lock(&section->state, 0);
    for (current = values; current != NULL; current = current->next)
        current->data = (gpointer) apt_control_field_get_value(current->data);
    g_bit_unlock(&section->state, 0);
    return values;
}

/**
 * apt_control_section_get_size:
 * @section: An #AptControlSection
 *
 * Gets the number of fields in this section.
 *
 * Return value: The number of fields in this section.
 **/
gsize apt_control_section_get_size(AptControlSection *section)
{
    g_return_val_if_fail(section != NULL, 0);
    return g_hash_table_size(section->table);
}

/**
 * apt_control_section_has_key:
 * @section: An #AptControlSection
 * @name: The name of the option
 *
 * Checks whether @section contains the field @name.
 *
 * Return value: %TRUE if @section contains @name, %FALSE otherwise.
 **/
gboolean apt_control_section_has_key(AptControlSection *section,
                                     const gchar *name)
{
    g_return_val_if_fail(section != NULL, FALSE);
    g_return_val_if_fail(name != NULL, FALSE);
    return g_hash_table_lookup(section->table, name) != NULL;
}

/**
 * apt_control_section_raw:
 * @section: An #AptControlSection
 * @size: A pointer to the #gsize holding the size of the section.
 *
 * Gets the raw section data for the current section. This is useful if a
 * section should be displayed. In most cases, the returned data is not
 * null-terminated.
 *
 * Return value: The raw section data, not null-terminated.
 **/
const gchar *apt_control_section_raw(AptControlSection *section, gsize *size)
{
    g_return_val_if_fail(section != NULL, NULL);
    g_return_val_if_fail(size != NULL, NULL);
    g_return_val_if_fail(section->end > section->start, NULL);
    *size = (gsize) (section->end - section->start);
    return section->start;
}

/* < internal >
 * apt_control_section_get_filename:
 * @section: An #AptControlSection
 *
 * Gets the file name associated with @section. If @section
 * is not associated with an #AptControlFile, the result is
 * %NULL.
 *
 * Return value: A file name or %NULL if none is stored.
 */
const gchar *apt_control_section_get_filename(AptControlSection *section)
{
    g_return_val_if_fail(section != NULL, NULL);
    return section->filename;
}

/* < internal >
 * apt_control_section_get_lineno:
 * @section: An #AptControlSection
 * @name: The name of a field or %NULL if the line number of the
 *        beginning of the header is requested.
 *
 * Gets the line number at which the field @name of @section
 * is located, relative to the beginning of the file; or, if
 * @name is %NULL, the line number at which the section begins
 * is returned.
 *
 * Return value: A line number > 0; or 0 if the field does not
 *               exist.
 */
gint apt_control_section_get_lineno(AptControlSection *section,
                                    const gchar *name)
{
    AptControlField *field;

    g_return_val_if_fail(section != NULL, 0);

    if (name == NULL)
        return section->lineno;

    field = g_hash_table_lookup(section->table, name);
    return field ? section->lineno + field->lineno : 0;
}

/**
 * AptControlFile:
 *
 * An opaque data structure that may only be accessed using the following
 * functions.
 *
 * Thread-safe: No
 **/
struct _AptControlFile {
    GMappedFile *file;
    const gchar *start;
    const gchar *current;
    const gchar *next;
    const gchar *end;
    gchar *filename;
    gint lineno;
};

/**
 * apt_control_file_new:
 * @file: The name of the file that should be parsed.
 * @error: Location to store an error.
 *
 * Creates a new #AptControlFile for parsing @file.
 *
 * Return value: A newly allocated #AptControlFile or %NULL if an error
 *               occurs.
 **/
AptControlFile *apt_control_file_new(const gchar *filename, GError **error)
{
    AptControlFile *file;
    GError *map_error = NULL;

    g_return_val_if_fail(filename != NULL, NULL);
    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    file = g_slice_new(AptControlFile);
    file->file = g_mapped_file_new(filename, FALSE, &map_error);
    if (file->file == NULL) {
        g_propagate_error(error, map_error);
        g_slice_free(AptControlFile, file);
        return NULL;
    }
    file->filename = g_strdup(filename);
    file->start = g_mapped_file_get_contents(file->file);
    file->current = file->start;
    file->lineno = 1;
    file->next = file->start;
    file->end = file->start + g_mapped_file_get_length(file->file);
    return file;
}

/**
 * apt_control_file_new_for_data:
 * @data: The data that shall be parsed. Must be kept around for the life-time
 *        of the object and will not be copied.
 * @length: The length of @data or 0 if @data is null terminated.
 *
 * Creates a new #AptControlFile for parsing @data.
 *
 * Return value: A newly allocated #AptControlFile.
 **/
AptControlFile *apt_control_file_new_for_data(const gchar *data, gsize length)
{
    AptControlFile *file;

    g_return_val_if_fail(data != NULL, NULL);

    file = g_slice_new(AptControlFile);
    file->file = NULL;
    file->lineno = 1;
    file->start = data;
    file->current = file->start;
    file->filename = g_strdup("<input>");
    file->end = file->start + ((length != 0) ? length : strlen(data));
    return file;
}

/**
 * apt_control_file_free:
 * @file: An #AptControlFile
 *
 * Free the memory used by @file.
 **/
void apt_control_file_free(AptControlFile *file)
{
    g_return_if_fail(file != NULL);
    if (file->file)
        g_mapped_file_unref(file->file);
    g_free(file->filename);
    g_slice_free(AptControlFile, file);
}

/**
 * apt_control_file_step:
 * @file: An #AptControlFile
 *
 * Advances to the next section in the file and returns that section as a
 * #AptControlSection as returned by apt_control_section_new().
 *
 * Return value: A newly allocated #AptControlSection.
 **/
AptControlSection *apt_control_file_step(AptControlFile *file)
{
    AptControlSection *section;
    g_return_val_if_fail(file != NULL, NULL);
    while ((file->end > file->next)) {
        file->current = file->next;
        section = apt_control_section_new(file->current, file->end -
                                          file->current, &file->next);
        section->lineno = file->lineno;
        section->filename = file->filename;
        file->lineno += section->linecount;
        if (apt_control_section_get_size(section) > 0)
            return section;
        apt_control_section_unref(section);
    }
    return NULL;
}

/**
 * apt_control_file_jump:
 * @file: An #AptControlFile
 * @offset: The offset at which the section begins.
 *
 * Parse the header section at @offset. This moves the internal file
 * pointer to the given offset, it should thus not be mixed with calls
 * to apt_control_file_step().
 *
 * Return value: A newly allocated #GHashTable or %NULL if there are no
 *               more sections.
 **/
AptControlSection *apt_control_file_jump(AptControlFile *file, goffset offset)
{
    g_return_val_if_fail(file != NULL, NULL);
    g_return_val_if_fail(file->start + offset < file->end, NULL);
    file->next = file->start + offset;
    file->lineno = G_MININT;
    return apt_control_file_step(file);
}

/**
 * apt_control_file_get_offset:
 * @file: An #AptControlFile
 *
 * Gets the absolute offset at which the current section is
 * located.
 *
 * Return value: The offset of the current section.
 **/
goffset apt_control_file_get_offset(AptControlFile *file)
{
    g_return_val_if_fail(file != NULL, 0);
    return (goffset) (file->current - file->start);
}
