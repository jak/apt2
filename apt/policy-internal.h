/* policy-internal.h - Internal AptPolicy stuff (e.g. for testing).
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_POLICY_INTERNAL_H__
#define __APT_LIB_POLICY_INTERNAL_H__

#include <apt/cache.h>
#include <apt/policy.h>
#include <apt/controlfile.h>

/* < private >
 * AptPinType:
 * @APT_PIN_VERSION: The pin is a version pin.
 * @APT_PIN_RELEASE: The pin is a release pin.
 * @APT_PIN_ORIGIN: The pin is an origin pin (pin by site name).
 *
 * Enum values that describe the type of a dependency.
 */
typedef enum {
    APT_PIN_VERSION,
    APT_PIN_RELEASE,
    APT_PIN_ORIGIN
} AptPinType;

/* <private>
 * AptReleaseInfo:
 * @archive: The archive of the release.
 * @codename: The codename of the release.
 * @component: The component of the release.
 * @label: The label of the release.
 * @origin: The origin of the release.
 * @version: The version of the release.
 *
 * #AptReleaseInfo holds information about a release.
 */
typedef struct _AptReleaseInfo {
    gchar *archive;
    gchar *codename;
    gchar *component;
    gchar *label;
    gchar *origin;
    gchar *version;
} AptReleaseInfo;

/* < private >
 * AptPin:
 * @packages: A null-terminated array of package names or NULL if the
 *            pin is against a package file.
 * @priority: The priority of the pin.
 * @is_source_pin: Whether @packages refers to source package names.
 * @type: The type of the pin.
 * @pin: The value of the pin.
 *
 * #AptPin is a structure holding information about a single section in an
 * APT preferences file.
 */
typedef struct _AptPin {
    gchar **packages;
    gint priority;
    gboolean is_source_pin;
    AptPinType type;
    union {
        AptReleaseInfo release;
        gchar *origin;
        gchar *version;
    } pin;
} AptPin;

/**
 * AptPolicy:
 *
 * #AptPolicy is an opaque data structure that may only be accessed using
 * the following functions.
 **/
struct _AptPolicy {
    AptCache *cache;
    GRegex *pin_regex;
    GRegex *package_regex;
    gint16 *package_pins;
    gint16 *packagefile_pins;
    gint16 *default_packagefile_pins;
    AptPackageFile **packagefiles;
    gsize n_packagefiles;
    gint ref_count;
    GCompareFunc compare_versions;
};

AptPin *apt_pin_new(AptPolicy *policy, AptControlSection *section,
                    GError **error);
void apt_pin_free(AptPin *pin);
gboolean apt_pin_matches_file(const AptPin *pin, AptPackageFile *packagefile);

#define PACKAGE_REGEX g_regex_new("\\s+", G_REGEX_OPTIMIZE, 0, NULL)
#define PIN_REGEX     g_regex_new("\\G([a-z])=([^,]+)(,\\s*|$)", \
                                  G_REGEX_OPTIMIZE, 0, NULL)

#endif                          /* __APT_LIB_POLICY_INTERNAL_H__ */
