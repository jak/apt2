/* cache.h - APT2 cache.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_CACHE_H__
#define __APT_LIB_CACHE_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * AptCache:
 *
 * #AptCache is an opaque data structure and can only be accessed
 * using the following functions.
 **/
typedef struct _AptCache AptCache;

/**
 * AptPackage:
 *
 * An opaque data structure representing a package in the cache.
 **/
typedef struct _AptPackage AptPackage;

/**
 * AptPackageIter:
 *
 * An opaque data structure representing an iterator over #AptPackage
 * instances.
 **/
typedef struct _AptPackageIter AptPackageIter;

/**
 * AptDependency:
 *
 * An opaque data structure representing a dependency in the cache.
 **/
typedef struct _AptDependency AptDependency;

/**
 * AptProvides:
 *
 * An opaque data structure representing a virtual package.
 **/
typedef struct _AptProvides AptProvides;

/**
 * AptPackageFile:
 *
 * An opaque data structure representing package files.
 **/
typedef struct _AptPackageFile AptPackageFile;

/**
 * AptDescription:
 *
 * An opaque data structure which may only be accessed using the following
 * functions. Currently there is no way to create objects of this type, as
 * this is still under heavy development.
 */
typedef struct _AptDescription AptDescription;

/**
 * AptDescriptionIter:
 *
 * An iterator over #AptDescription objects. This is an opaque type.
 */
typedef struct _AptDescriptionIter AptDescriptionIter;

/**
 * AptDependencyType:
 * @APT_DEPENDENCY_INVALID:    The dependency is invalid. If you see this
 *                             somewhere, it is a bug.
 * @APT_DEPENDENCY_PREDEPENDS: The dependency must be completely installed
 *                             before the package can be installed
 * @APT_DEPENDENCY_DEPENDS:    The dependency must be unpacked.
 * @APT_DEPENDENCY_RECOMMENDS: Like the previous one, but only installed
 *                             if available (it should be installed).
 * @APT_DEPENDENCY_SUGGESTS:   The dependency may be useful for certain
 *                             cases, but is normally not needed.
 * @APT_DEPENDENCY_ENHANCES:   The dependency is enhanced by this package,
 *                             think of it as a reverse suggestion.
 * @APT_DEPENDENCY_CONFLICTS:  The both packages may not be installed at
 *                             the same time.
 * @APT_DEPENDENCY_BREAKS:     This package breaks the dependency; for
 *                             example, by removing a file the dependency
 *                             needs.
 * @APT_DEPENDENCY_REPLACES:   The package may replace files installed by
 *                             the dependency.
 *
 * Enumeration which defines all types of dependencies (positive and negative)
 * understood by APT2. The names come from Debian package management, but the
 * underlying concept is also understood by RPM systems.
 **/
typedef enum AptDependencyType {
    APT_DEPENDENCY_INVALID,
    APT_DEPENDENCY_PREDEPENDS,
    APT_DEPENDENCY_DEPENDS,
    APT_DEPENDENCY_RECOMMENDS,
    APT_DEPENDENCY_SUGGESTS,
    APT_DEPENDENCY_ENHANCES,
    APT_DEPENDENCY_CONFLICTS,
    APT_DEPENDENCY_BREAKS,
    APT_DEPENDENCY_REPLACES
} AptDependencyType;

/**
 * AptComparisonType:
 * @APT_COMPARISON_NONE: There is no comparison at all.
 * @APT_COMPARISON_LT: Less (<).
 * @APT_COMPARISON_LE: Less or equal (<=).
 * @APT_COMPARISON_EQ: Equal (= or ==).
 * @APT_COMPARISON_NE: Not equal (!=).
 * @APT_COMPARISON_GE: Greater or equal (>=)
 * @APT_COMPARISON_GT: Greater (>)
 *
 * Enum values to specify comparison operators. The names are derived from the
 * comparison operators in Perl and are abbreviations.
 **/
typedef enum AptComparisonType {
    APT_COMPARISON_NONE,
    APT_COMPARISON_LT,
    APT_COMPARISON_LE,
    APT_COMPARISON_EQ,
    APT_COMPARISON_NE,
    APT_COMPARISON_GE,
    APT_COMPARISON_GT
} AptComparisonType;

/**
 * AptSelectionState:
 * @APT_SELECTION_STATE_UNKNOWN: The state is not set.
 * @APT_SELECTION_STATE_INSTALL: The package shall be installed.
 * @APT_SELECTION_STATE_HOLD: The package shall not be modified.
 * @APT_SELECTION_STATE_DEINSTALL: The package shall be deinstalled,
 *                                         excluding its configuration files.
 * @APT_SELECTION_STATE_PURGE: The package shall be deleted, including
 *                                     its configuration files.
 *
 * Enum values that describe the state the user wants the package to be
 * in. They are taken directly from the manual page of dpkg. If the lower
 * package manager has no notion of config files, it shall treat
 * "purge" and "deinstall" identically.
 */
typedef enum {
    APT_SELECTION_STATE_UNKNOWN,
    APT_SELECTION_STATE_INSTALL,
    APT_SELECTION_STATE_HOLD,
    APT_SELECTION_STATE_DEINSTALL,
    APT_SELECTION_STATE_PURGE
} AptSelectionState;

/**
 * AptCurrentState:
 * @APT_CURRENT_STATE_NOT_INSTALLED: The package is not installed.
 * @APT_CURRENT_STATE_CONFIG_FILES: Only configuration files exist.
 * @APT_CURRENT_STATE_HALF_INSTALLED: Installation has started.
 * @APT_CURRENT_STATE_UNPACKED: The files of the package are unpacked.
 * @APT_CURRENT_STATE_HALF_CONFIGURED: Not totally configured.
 * @APT_CURRENT_STATE_TRIGGERS_AWAITED: The package awaits triggers.
 * @APT_CURRENT_STATE_TRIGGERS_PENDING: The package has been triggered.
 * @APT_CURRENT_STATE_INSTALLED: The package is completely installed.
 *
 * Enum values that describe the current state of the package. The values
 * have been derived from the manual page of dpkg.
 */
typedef enum {
    APT_CURRENT_STATE_NOT_INSTALLED,
    APT_CURRENT_STATE_CONFIG_FILES,
    APT_CURRENT_STATE_HALF_INSTALLED,
    APT_CURRENT_STATE_UNPACKED,
    APT_CURRENT_STATE_HALF_CONFIGURED,
    APT_CURRENT_STATE_TRIGGERS_AWAITED,
    APT_CURRENT_STATE_TRIGGERS_PENDING,
    APT_CURRENT_STATE_INSTALLED
} AptCurrentState;

/**
 * AptPackageFlags:
 * @APT_PACKAGE_REINST_REQUIRED: The package is broken and must
 *                                           be reinstalled.
 * @APT_PACKAGE_AUTO: The package is automatically installed.
 *
 * Some flags which may be attached to a package.
 **/
typedef enum {
    APT_PACKAGE_REINST_REQUIRED = 1 << 0,
    APT_PACKAGE_AUTO = 1 << 1
} AptPackageFlags;

/**
 * AptPackageFileFlags:
 * @APT_PACKAGE_FILE_NOT_SOURCE: Packages from this package file can not be
 *                               retrieved. This mainly applies to package
 *                               manager databases, such as dpkg's
 *                               <filename>/var/lib/dpkg/status</filename>.
 * @APT_PACKAGE_FILE_NOT_AUTOMATIC: Packages from this file shall not
 *                                  be upgraded automatically.
 * @APT_PACKAGE_FILE_IS_TRUSTED: The package file is considered to be
 *                               trusted. For Debian, this means that the
 *                               Release file of the distribution this
 *                               package file belongs to is signed.
 *
 * Various flags that can be set on an #AptPackageFile.
 *
 **/
typedef enum {
    APT_PACKAGE_FILE_NOT_SOURCE = 1 << 0,
    APT_PACKAGE_FILE_NOT_AUTOMATIC = 1 << 1,
    APT_PACKAGE_FILE_IS_TRUSTED = 1 << 2
} AptPackageFileFlags;

/**
 * AptMultiArch:
 * @APT_MULTI_ARCH_NONE: The package may only be installed for one architecture
 *                       and may only satisfy dependencies of packages of the
 *                       same architecture.
 * @APT_MULTI_ARCH_SAME: Similar to @APT_MULTI_ARCH_NONE, but it allows the
 *                       package to be installed for multiple architectures
 *                       at the same time.
 * @APT_MULTI_ARCH_FOREIGN: The package is not co-installable with itself,
 *                          but can satisfy the dependencies of packages from
 *                          a different architecture.
 * @APT_MULTI_ARCH_ALLOWED: Similar to @APT_MULTI_ARCH_SAME, but it satisfies
 *                          dependencies of packages from any architecture, as
 *                          long as the dependency allows this (by setting the
 *                          architecture to "any").
 *
 * Enum values that determine how a package may engage in multi-architecture
 * dependency situations.
 **/
typedef enum {
    APT_MULTI_ARCH_NONE,
    APT_MULTI_ARCH_SAME,
    APT_MULTI_ARCH_FOREIGN,
    APT_MULTI_ARCH_ALLOWED
} AptMultiArch;

/**
 * AptPriority:
 * @APT_PRIORITY_UNKNOWN: The priority of the package is unknown, such a
 *                        value is normally invalid.
 * @APT_PRIORITY_REQUIRED: The package is required for a working base system.
 * @APT_PRIORITY_IMPORTANT: The package should be installed on the system.
 * @APT_PRIORITY_STANDARD:  The package has standard priority.
 * @APT_PRIORITY_OPTIONAL:  The package is optional.
 * @APT_PRIORITY_EXTRA:     The package is extra; that is, users only want to
 *                          install it in special circumstances.
 *
 * Enum values that describe the priority of the package.
 **/
typedef enum {
    APT_PRIORITY_UNKNOWN,
    APT_PRIORITY_REQUIRED,
    APT_PRIORITY_IMPORTANT,
    APT_PRIORITY_STANDARD,
    APT_PRIORITY_OPTIONAL,
    APT_PRIORITY_EXTRA
} AptPriority;

/**
 * AptCacheFindFlags:
 * @APT_CACHE_FIND_BY_BINARY: Find packages based on their name.
 * @APT_CACHE_FIND_BY_SOURCE: Find packages based on the name of their source.
 * @APT_CACHE_FIND_BY_PROVIDES: Find packages based on what they provide.
 * @APT_CACHE_FIND_BY_DEPENDS: Find packages based on what they depend on.
 * @APT_CACHE_FIND_INSTALLED_ONLY: Only find installed packages.
 *
 * Enum values that can be passed to apt_cache_find() as its third
 * argument. You may freely combine those values using OR.
 *
 **/
typedef enum {
    APT_CACHE_FIND_BY_BINARY = 1 << 0,
    APT_CACHE_FIND_BY_SOURCE = 1 << 1,
    APT_CACHE_FIND_BY_PROVIDES = 1 << 2,
    APT_CACHE_FIND_BY_DEPENDS = 1 << 3,
    APT_CACHE_FIND_INSTALLED_ONLY = 1 << 4
} AptCacheFindFlags;

/**
 * AptCacheError:
 * @APT_CACHE_ERROR_CORRUPT: The cache file is corrupt.
 * @APT_CACHE_ERROR_UNSUPPORTED: The cache file is in an unsupported format and
 *                               needs to be recreated from scratch.
 *
 * Possible errors.
 */
typedef enum {
    APT_CACHE_ERROR_CORRUPT,
    APT_CACHE_ERROR_UNSUPPORTED
} AptCacheError;

#define APT_CACHE_ERROR apt_cache_error_quark ()
extern GQuark apt_cache_error_quark(void);

AptCache *apt_cache_new(const gchar *filename, GError **error);
gint32 apt_cache_get_format_version(AptCache *cache);
const gchar *apt_cache_get_system_name(AptCache *cache);
AptCache *apt_cache_ref(AptCache *cache);
void apt_cache_unref(AptCache *cache);
gsize apt_cache_n_packages(AptCache *cache);
gsize apt_cache_n_packagefiles(AptCache *cache);
AptPackage *apt_cache_get_package(AptCache *cache, gsize id);
AptPackageIter *apt_cache_find(AptCache *cache, const gchar *name,
                               AptCacheFindFlags flags);
AptPackageIter *apt_cache_iterator(AptCache *cache);
AptPackageFile *apt_cache_get_package_file(AptCache *cache, guint8 id);
gboolean apt_cache_is_up_to_date(AptCache *cache);

void apt_package_unref(AptPackage *package);
AptPackage *apt_package_ref(AptPackage *package);
guint32 apt_package_get_id(AptPackage *package);
const gchar *apt_package_get_name(AptPackage *package);
const gchar *apt_package_get_version(AptPackage *package);
const gchar *apt_package_get_architecture(AptPackage *package);
AptMultiArch apt_package_get_multi_arch(AptPackage *package);
goffset apt_package_get_size(AptPackage *package);
goffset apt_package_get_installed_size(AptPackage *package);
const gchar *apt_package_get_checksum(AptPackage *package);
AptPriority apt_package_get_priority(AptPackage *package);
const gchar *apt_package_get_section(AptPackage *package);
AptProvides *apt_package_get_provides(AptPackage *package, gsize index);
gsize        apt_package_n_provides(AptPackage *package);
const gchar *apt_package_get_source_name(AptPackage *package);
const gchar *apt_package_get_source_version(AptPackage *package);
AptDependency *apt_package_get_dependency(AptPackage *package, gsize index);
gsize apt_package_n_dependencies(AptPackage *package);

AptCurrentState apt_package_get_current_state(AptPackage *package);
AptSelectionState apt_package_get_selection_state(AptPackage *package);
AptPackageFlags apt_package_get_flags(AptPackage *package);

AptDescriptionIter *apt_package_get_descriptions(AptPackage *package);

AptPackage *apt_package_iter_next_value(AptPackageIter *iter);
void apt_package_iter_free(AptPackageIter *iter);

void apt_dependency_unref(AptDependency *dependency);
AptDependency *apt_dependency_ref(AptDependency *dependency);
const gchar *apt_dependency_get_name(AptDependency *dependency);
const gchar *apt_dependency_get_version(AptDependency *dependency);
const gchar *apt_dependency_get_architecture(AptDependency *dependency);
AptComparisonType apt_dependency_get_comparison(AptDependency *dependency);
AptDependencyType apt_dependency_get_type(AptDependency *dependency);
gboolean apt_dependency_get_next_is_or(AptDependency *dependency);

void apt_provides_unref(AptProvides *provides);
AptProvides *apt_provides_ref(AptProvides *provides);
const gchar *apt_provides_get_name(AptProvides *provides);
const gchar *apt_provides_get_version(AptProvides *provides);

const gchar *apt_package_file_get_filename(AptPackageFile *packagefile);
const gchar *apt_package_file_get_archive(AptPackageFile *packagefile);
const gchar *apt_package_file_get_codename(AptPackageFile *packagefile);
const gchar *apt_package_file_get_component(AptPackageFile *packagefile);
const gchar *apt_package_file_get_version(AptPackageFile *packagefile);
const gchar *apt_package_file_get_origin(AptPackageFile *packagefile);
const gchar *apt_package_file_get_label(AptPackageFile *packagefile);
const gchar *apt_package_file_get_site(AptPackageFile *packagefile);
const gchar *apt_package_file_get_type(AptPackageFile *packagefile);
const gchar *apt_package_file_get_base_uri(AptPackageFile *packagefile);
time_t apt_package_file_get_mtime(AptPackageFile *packagefile);
AptPackageFileFlags apt_package_file_get_flags(AptPackageFile *packagefile);
gboolean apt_package_file_is_up_to_date(AptPackageFile *packagefile);
AptPackageFile *apt_package_file_ref(AptPackageFile *packagefile);
void apt_package_file_unref(AptPackageFile *packagefile);

AptDescription *apt_description_ref(AptDescription *description);
void apt_description_unref(AptDescription *description);
guint8 apt_description_get_package_file_id(AptDescription *description);
const gchar *apt_description_get_filename(AptDescription *description);
const gchar *apt_description_get_md5(AptDescription *description);
const gchar *apt_description_get_language(AptDescription *description);
goffset apt_description_get_offset(AptDescription *description);

AptDescription *apt_description_iter_next_value(AptDescriptionIter *iter);
void apt_description_iter_free(AptDescriptionIter *iter);

const gchar *apt_multi_arch_to_string(AptMultiArch multi_arch) G_GNUC_CONST;
const gchar *apt_priority_to_string(AptPriority priority) G_GNUC_CONST;
const gchar *apt_dependency_type_to_string(AptDependencyType type) G_GNUC_CONST;
gint apt_dependency_type_rate(AptDependencyType type) G_GNUC_CONST;
const gchar *apt_comparison_type_to_string(AptComparisonType type) G_GNUC_CONST;

G_END_DECLS
#endif                          /* __APT_LIB_CACHE_H__ */
