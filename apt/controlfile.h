/* controlfile.h - Parser for Debian control files.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __APT_LIB_CONTROLFILE_H__
#define __APT_LIB_CONTROLFILE_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _AptControlSection AptControlSection;
AptControlSection *apt_control_section_new(const gchar *section,
                                           gssize length, const gchar **next);
AptControlSection *apt_control_section_ref(AptControlSection *section);
void apt_control_section_unref(AptControlSection *section);
const gchar *apt_control_section_get(AptControlSection *section,
                                     const gchar *name);
const gchar *apt_control_section_get_raw(AptControlSection *section,
                                         const gchar *name,
                                         gboolean complete, gsize *length);
GList *apt_control_section_get_keys(AptControlSection *section);
GList *apt_control_section_get_values(AptControlSection *section);
gsize apt_control_section_get_size(AptControlSection *section);
gboolean apt_control_section_has_key(AptControlSection *section,
                                     const gchar *name);

G_GNUC_INTERNAL const gchar *apt_control_section_get_filename(AptControlSection
                                                              *section);
G_GNUC_INTERNAL gint apt_control_section_get_lineno(AptControlSection *section,
                                                    const gchar *name);

typedef struct _AptControlFile AptControlFile;
AptControlFile *apt_control_file_new(const gchar *file, GError **error);
AptControlFile *apt_control_file_new_for_data(const gchar *data, gsize length);
void apt_control_file_free(AptControlFile *file);
AptControlSection *apt_control_file_step(AptControlFile *file);
AptControlSection *apt_control_file_jump(AptControlFile *file, goffset offset);
goffset apt_control_file_get_offset(AptControlFile *file);
const gchar *apt_control_section_raw(AptControlSection *section, gsize *size);

G_END_DECLS
#endif                          /* __APT_LIB_CONTROLFILE_H__ */
