/* utils.h - Various utility functions for APT2.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_LIB_UTILS_H__
#define __APT_LIB_UTILS_H__

#include <glib.h>

G_BEGIN_DECLS

/**
 * AptUtilsForeachInDirFunc:
 * @user_data: The user data passed to apt_utils_foreach_in_dir().
 * @file: The path to the file.
 * @error: The location to store an error.
 *
 * The type of the callback function passed to apt_utils_foreach_in_dir().
 *
 * Return value: %TRUE if no error occured, %FALSE otherwise. Returning
 *               %FALSE will cancel apt_utils_foreach_in_dir().
 **/
typedef gboolean (*AptUtilsForeachInDirFunc) (gpointer user_data,
                                              const gchar *file,
                                              GError **error);

gboolean apt_utils_foreach_in_dir(const gchar *dirname,
                                  const gchar *suffix,
                                  AptUtilsForeachInDirFunc callback,
                                  gpointer user_data, GError **error);

gint apt_utils_lock_file(const gchar *file, GError **error);
gboolean apt_utils_match(const gchar *pattern, const gchar *string);
guint apt_utils_str_case_hash(gconstpointer v);
gboolean apt_utils_str_case_equal(gconstpointer s1, gconstpointer s2);
gchar *apt_utils_uri_to_filename(const gchar *uri);

const gchar *apt_get_version(void);

G_END_DECLS
#endif                          /* __APT_LIB_UTILS_H__ */
