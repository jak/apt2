/* cache-builder.c - Build a new APT2 cache.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "cache-builder.h"

#include <string.h>

#include <glib.h>

/**
 * SECTION:cache-builder
 * @short_description: Building caches
 * @title: Cache Builder
 * @include: apt/cache-builder.h
 *
 * The cache builder is a simple object that enables the
 * creation of caches.
 **/

/* Check that the sizes of the objects are as predicted */
G_STATIC_ASSERT(sizeof(AptCacheHeader) % 8 == 0);
G_STATIC_ASSERT(sizeof(AptCacheHeader) == 72 +
                (APT_CACHE_N_BUCKETS * sizeof(cindex)));
G_STATIC_ASSERT(sizeof(AptCachePackage) == 80);
G_STATIC_ASSERT(sizeof(AptCacheDependency) == 16);
G_STATIC_ASSERT(sizeof(AptCacheProvides) == 8);
G_STATIC_ASSERT(sizeof(AptCacheGroupMember) == 12);
G_STATIC_ASSERT(sizeof(AptCacheGroup) == 16);
G_STATIC_ASSERT(sizeof(AptCachePackageFile) == 56);
G_STATIC_ASSERT(sizeof(AptCacheDescription) == 24);

/* Helper macros:
 * cache_at_offset: Gets a gpointer to the given offset
 * size: Calculate the size of an array in bytes
 * end: Calculate the end of an array
 */
#define cache_at_offset(c, o) ((gpointer)((char*) (c)->header + (o)))
#define size(name) (builder->name->len * \
                    g_array_get_element_size(builder->name))
#define end(name) (header.begin_##name + size(name))

static inline void array_assert_element_size(GArray *array, guint element_size)
{
    g_assert_cmpuint(g_array_get_element_size(array), ==, element_size);
}

/* <private>
 * array_new:
 * @array: A #GArray
 * @type: The type of the items stored in @array
 *
 * Allocates the space for an item of @type in @array
 * and returns a pointer to it.
 */
#define array_new(array, type) (\
     array_assert_element_size(array, sizeof(type)), \
     g_array_set_size(array, array->len + 1), \
     &g_array_index(array, type, array->len - 1))

/* <private>
 * array_reserve:
 * @array: A #GArray
 * @size: The size that shall be reserved
 *
 * Reserves space for @size items in @array.
 */
#define array_reserve(array, size) ( \
    g_array_set_size(array, array->len + size), \
    g_array_set_size(array, array->len - size))

/**
 * quark:
 * @next: Pointer to next quark
 * @length: Length of the stored string
 * @data: The position of the stored string
 *
 * Deduplication of strings in the cache, using a linked list. The list
 * is ordered according to the (length, data) tuple, thus limiting the
 * number of data comparisons required for lookup.
 */
typedef struct quark {
    struct quark *next;
    guint32 length;
    coffset data;
} quark;

/* < private >
 * str_n_hash:
 * @v: The string to hash
 * @n: The length of the string
 *
 * Hash the string @v of length @n. If @n is larger than the
 * string length, the behavior is undefined.
 *
 * Return value: The hash value
 */
static guint32 str_n_hash(gconstpointer v, gsize n)
{
    const signed char *p = v;
    const signed char *e = p + n;

    guint32 h = 5381;

    for (p = v; p != e; p++)
        h = (h << 5) + h + *p;

    return h;
}

/**
 * AptCacheBuilder:
 *
 * #AptCacheBuilder is an opaque data type that may only be accessed
 * using the following functions. It is responsible for creating APT2
 * caches.
 */
struct _AptCacheBuilder {
    /* < private > */
    AptCacheHeader header;
    GByteArray *strings;
    GArray *packages;
    GArray *dependencies;
    GArray *provides;
    GArray *files;
    GArray *descriptions;
    GArray *group_members;
    GArray *groups;
    GArray *hashtable;
    quark *quarks;
};

static coffset apt_cache_builder_dedup(AptCacheBuilder *builder, const gchar *s,
                                       gsize len)
{
    quark **q0 = &builder->quarks;
    quark *q;
    for (q = builder->quarks; q != NULL; q0 = &q->next, q = q->next) {
        const gchar *qs = (gchar *) builder->strings->data + q->data;
        gssize result = (gssize) q->length - len;
        if (result == 0 && (result = memcmp(qs, s, len)) == 0)
            return q->data;
        if (result > 0)
            break;
    }

    q = g_slice_new(quark);
    q->data = builder->strings->len;
    q->length = len;
    q->next = *q0;
    *q0 = q;

    return 0;
}

/**
 * apt_cache_builder_new_string:
 * @builder: An #AptCacheBuilder
 * @s: The string that shall be inserted
 * @len: The length of @s, or -1 if @s is nul-terminated
 * @dedup: Whether to de-duplicate the string
 *
 * Inserts a string into the cache. Duplicate strings may be inserted only once,
 * to improve space utilization and lookups. Empty strings and NULL pointers
 * are not inserted, for them, 0 is returned (and the 0-th string is an empty
 * string).
 *
 * Return value: The offset of the string in the cache's string array.
 */
coffset apt_cache_builder_new_string(AptCacheBuilder *builder, const gchar *s,
                                     gssize len, gboolean dedup)
{
    coffset offset;

    g_return_val_if_fail(builder != NULL, 0);
    g_return_val_if_fail(s != NULL || len <= 0, 0);

    if (s == NULL)
        return 0;
    if (len == -1)
        len = strlen(s);
    if (len == 0)
        return 0;

    if (dedup && (offset = apt_cache_builder_dedup(builder, s, len)) != 0)
        return offset;

    offset = builder->strings->len;
    g_byte_array_append(builder->strings, (const guint8 *) s, len);
    g_byte_array_append(builder->strings, (const guint8 *) "\0", 1);

    return offset;
}

/**
 * apt_cache_builder_new_package:
 * @builder: An #AptCacheBuilder
 *
 * Creates a new package in the cache and returns a pointer to it which
 * can be used to set the data.
 *
 * Return value: A pointer to an #AptCachePackage that stays valid until the
 * next call to apt_cache_builder_new_package() or apt_cache_builder_end().
 */
AptCachePackage *apt_cache_builder_new_package(AptCacheBuilder *builder)
{
    return array_new(builder->packages, AptCachePackage);
}

/**
 * apt_cache_builder_new_dependency:
 * @builder: An #AptCacheBuilder
 * @index: Return location for the dependencies index
 *
 * Creates a new dependency in the cache and returns a pointer to it
 * which can be used to set the data. The index stored in *@index can
 * be used; for example, to set #AptCachePackage.begin_dependencies.
 *
 * Return value: A pointer to an #AptCacheDependency that stays valid
 * until the next call to apt_cache_builder_new_dependency() or
 * apt_cache_builder_end().
 */
AptCacheDependency *apt_cache_builder_new_dependency(AptCacheBuilder *builder,
                                                     cindex *index)
{
    *index = builder->dependencies->len;
    return array_new(builder->dependencies, AptCacheDependency);
}

/**
 * apt_cache_builder_new_provides:
 * @builder: An #AptCacheBuilder
 * @index: Return location for the provides index
 *
 * Creates a new provides in the cache and returns a pointer to it
 * which can be used to set the data. The index stored in *@index can
 * be used; for example, to set #AptCachePackage.begin_provides.
 *
 * Return value: A pointer to an #AptCacheProvides that stays valid
 * until the next call to apt_cache_builder_new_provides() or
 * apt_cache_builder_end().
 */
AptCacheProvides *apt_cache_builder_new_provides(AptCacheBuilder *builder,
                                                 cindex *index)
{
    *index = builder->provides->len;
    return array_new(builder->provides, AptCacheProvides);
}

/**
 * apt_cache_builder_new_file:
 * @builder: An #AptCacheBuilder
 *
 * Creates a new file in the cache and returns a pointer to it
 * which can be used to set the data.
 *
 * Return value: A pointer to an #AptCachePackageFile that stays
 * valid until the next call to apt_cache_builder_new_file() or
 * apt_cache_builder_end().
 */
AptCachePackageFile *apt_cache_builder_new_file(AptCacheBuilder *builder)
{
    return array_new(builder->files, AptCachePackageFile);
}

/**
 * apt_cache_builder_new_description:
 * @builder: An #AptCacheBuilder
 * @index: Return location for the description index
 *
 * Creates a new description in the cache and returns a pointer to it
 * which can be used to set the data. The index stored in *@index can
 * be used; for example, to set #AptCachePackage.begin_descriptions.
 *
 * Return value: A pointer to an #AptCacheDescription that stays valid
 * until the next call to apt_cache_builder_new_description() or
 * apt_cache_builder_end().
 */
AptCacheDescription *apt_cache_builder_new_description(AptCacheBuilder *builder,
                                                       cindex *index)
{
    *index = builder->descriptions->len;
    return array_new(builder->descriptions, AptCacheDescription);
}

/**
 * apt_cache_builder_new_group_member:
 * @builder: An #AptCacheBuilder
 * @index: Return location for the group member index, or %NULL if not needed
 *
 * Creates a new group member in the cache and returns a pointer to it
 * which can be used to set the data. The index stored in *@index can
 * be used; for example, to set #AptCacheGroup.begin_members.
 *
 * Return value: A pointer to an #AptCacheGroupMember that stays valid
 * until the next call to apt_cache_builder_new_group_member() or
 * apt_cache_builder_end().
 */
AptCacheGroupMember *apt_cache_builder_new_group_member(AptCacheBuilder
                                                        *builder,
                                                        AptCacheGroup *group,
                                                        AptCacheGroupMemberType
                                                        type, cindex *index)
{
    AptCacheGroupMember *member;
    const cindex idx = builder->group_members->len;
    cindex cur = group->begin_members;
    cindex *prev = &group->begin_members;

    if (index != NULL)
        *index = builder->group_members->len;

    member = array_new(builder->group_members, AptCacheGroupMember);
    member->type = type;

    while (cur != CINDEX_NONE) {
        AptCacheGroupMember *m = &g_array_index(builder->group_members,
                                                AptCacheGroupMember, cur);
        if (m->type >= type)
            break;

        prev = &m->next;
        cur = m->next;
    }

    member->next = *prev;
    *prev = idx;

    return member;
}

/**
 * apt_cache_builder_new_group:
 * @builder: An #AptCacheBuilder
 *
 * Creates a new group in the cache and returns a pointer
 * to it which can be used to set the data. You normally
 * do not need to call this function. If this function is
 * not called at all, but group members have been added,
 * the builder will automatically create the groups for
 * the members.
 *
 * Return value: A pointer to an #AptCacheGroup that stays valid
 * until the next call to apt_cache_builder_new_group() or
 * apt_cache_builder_end().
 */
AptCacheGroup *apt_cache_builder_new_group(AptCacheBuilder *builder,
                                           const gchar *name, gssize length)
{
    AptCacheGroup *group;
    cindex *bucket;
    guint32 hash;
    guint32 h;
    const char *strings;

    if (length == -1)
        length = strlen(name);

    hash = str_n_hash(name, length);
    h = builder->header.h_buckets[hash % APT_CACHE_N_BUCKETS];
    strings = (char *) builder->strings->data;

    while (h != CINDEX_NONE) {
        AptCacheGroup *group = (AptCacheGroup *) builder->groups->data + h;
        if (group->hash == hash
            && strncmp(strings + group->name, name, length) == 0
            && strings[group->name + length] == '\0')
            return group;
        h = group->h_next;
    }

    group = array_new(builder->groups, AptCacheGroup);
    group->hash = hash;
    group->name = apt_cache_builder_new_string(builder, name, length, FALSE);
    group->begin_members = CINDEX_NONE;
    group->h_next = CINDEX_NONE;
    bucket = &builder->header.h_buckets[group->hash % APT_CACHE_N_BUCKETS];
    group->h_next = *bucket;
    *bucket = group - (AptCacheGroup *) builder->groups->data;

    return group;
}

/**
 * apt_cache_builder_new:
 *
 * Creates a new #AptCacheBuilder.
 *
 * Return value: A pointer to a newly-allocated #AptCacheBuilder. Must be
 * freed with apt_cache_builder_end().
 */
AptCacheBuilder *apt_cache_builder_new(void)
{
    AptCacheBuilder *builder;

    builder = g_slice_new(AptCacheBuilder);
    builder->strings = g_byte_array_sized_new(8 * 1024 * 1024);
    builder->packages = g_array_new(FALSE, TRUE, sizeof(AptCachePackage));
    builder->dependencies = g_array_new(FALSE, TRUE,
                                        sizeof(AptCacheDependency));
    builder->provides = g_array_new(FALSE, TRUE, sizeof(AptCacheProvides));
    builder->files = g_array_new(FALSE, TRUE, sizeof(AptCachePackageFile));
    builder->descriptions = g_array_new(FALSE, TRUE,
                                        sizeof(AptCacheDescription));
    builder->group_members = g_array_new(FALSE, TRUE,
                                         sizeof(AptCacheGroupMember));
    builder->groups = g_array_new(FALSE, TRUE, sizeof(AptCacheGroup));

    array_reserve(builder->packages, 64 * 1024);
    array_reserve(builder->dependencies, 256 * 1024);
    array_reserve(builder->provides, 8 * 1024);
    array_reserve(builder->files, 256);
    array_reserve(builder->descriptions, 128 * 1024);

    /* Reserve the 0-th string for an empty string */
    g_byte_array_append(builder->strings, (const guint8 *) "", 1);

    memset(builder->header.h_buckets, CINDEX_NONE,
           sizeof(builder->header.h_buckets));

    builder->quarks = NULL;

    return builder;
}

/**
 * apt_cache_builder_end:
 * @builder: An #AptCacheBuilder
 *
 * Finalizes @builder and returns the generated cache. After this
 * function returned, the object pointed to by @builder does not
 * exist anymore.
 *
 * Return value: A pointer to a newly-allocated #AptCache. Must
 * be freed by calling apt_cache_unref().
 */
AptCache *apt_cache_builder_end(AptCacheBuilder *builder)
{
    AptCacheHeader header;
    AptCache *cache;
    header = builder->header;

    while ((builder->strings->len % 8) != 0)
        g_byte_array_append(builder->strings, (const guint8 *) "", 1);
    while ((builder->group_members->len * sizeof(AptCacheGroupMember)) % 8)
        (void) array_new(builder->group_members, AptCacheGroupMember);

    memcpy(&header.magic_bytes, "APT", 4);
    header.major_version = APT_CACHE_MAJOR_VERSION;
    header.minor_version = APT_CACHE_MINOR_VERSION;
    header.begin_strings = sizeof(AptCacheHeader);
    header.begin_packages = header.begin_strings + builder->strings->len;
    header.n_packages = builder->packages->len;
    header.begin_dependencies = end(packages);
    header.n_dependencies = builder->dependencies->len;
    header.begin_provides = end(dependencies);
    header.n_provides = builder->provides->len;
    header.begin_files = end(provides);
    header.n_files = builder->files->len;
    header.begin_descriptions = end(files);
    header.n_descriptions = builder->descriptions->len;
    header.begin_group_members = end(descriptions);
    header.n_group_members = builder->group_members->len;
    header.begin_groups = end(group_members);
    header.n_groups = builder->groups->len;
    header.end_of_file = end(groups);

    cache = g_slice_new0(AptCache);
    cache->data = g_malloc(header.end_of_file);
    cache->header = cache->data;
    cache->strings = cache_at_offset(cache, header.begin_strings);
    cache->packages = cache_at_offset(cache, header.begin_packages);
    cache->dependencies = cache_at_offset(cache, header.begin_dependencies);
    cache->provides = cache_at_offset(cache, header.begin_provides);
    cache->files = cache_at_offset(cache, header.begin_files);
    cache->descriptions = cache_at_offset(cache, header.begin_descriptions);
    cache->group_members = cache_at_offset(cache, header.begin_group_members);
    cache->groups = cache_at_offset(cache, header.begin_groups);
    cache->ref_count = 1;

    memcpy(cache->header, &header, sizeof(header));
    memcpy(cache->strings, builder->strings->data, builder->strings->len);
    memcpy(cache->packages, builder->packages->data, size(packages));
    memcpy(cache->dependencies, builder->dependencies->data,
           size(dependencies));
    memcpy(cache->provides, builder->provides->data, size(provides));
    memcpy(cache->files, builder->files->data, size(files));
    memcpy(cache->descriptions, builder->descriptions->data,
           size(descriptions));
    memcpy(cache->group_members, builder->group_members->data,
           size(group_members));
    memcpy(cache->groups, builder->groups->data, size(groups));

    g_byte_array_free(builder->strings, TRUE);
    g_array_free(builder->packages, TRUE);
    g_array_free(builder->dependencies, TRUE);
    g_array_free(builder->provides, TRUE);
    g_array_free(builder->files, TRUE);
    g_array_free(builder->descriptions, TRUE);
    g_array_free(builder->group_members, TRUE);
    g_array_free(builder->groups, TRUE);
    g_slice_free_chain(quark, builder->quarks, next);
    g_slice_free(AptCacheBuilder, builder);

    return cache;
}
