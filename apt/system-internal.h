/* system-internal.h - Implementation-specific parts
 *
 * Copyright (C) 2010-2011 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __APT_SYSTEM_INTERNAL_H__
#define __APT_SYSTEM_INTERNAL_H__

#include <glib.h>
#include <gmodule.h>

#include <apt/configuration.h>
#include <apt/system.h>

G_BEGIN_DECLS

typedef struct _AptSystemDesc AptSystemDesc;

#define APT_SYSTEM_DESC(s) (s->desc)

/**
 * AptSystem:
 *
 * This data structure has to be implemented by the system modules. There is
 * no guarantee about the stability of this struct's API and/or ABI, please
 * do not access any of its fields directly.
 */
struct _AptSystem {
    /* < private > */
    const AptSystemDesc *desc;
    AptConfiguration *configuration;
    GModule *module;
    gint ref_count;
    /* First bit is lock-bit, others are unused (for now). */
    gint state;
};

struct _AptSystemDesc {
    const gchar *description;
    const gchar *author;
    gsize size;
    gboolean (*initialize) (AptSystem *system, GError **error);
    void (*destroy) (AptSystem *system);
    gint (*compare_versions) (const gchar *a, const gchar *b);
    gboolean (*lock) (AptSystem *system, GError **error);
    gboolean (*unlock) (AptSystem *system, GError **error);
};

/* < internal > */
AptSystem *apt_system_new_for_path(const gchar *path,
                                   AptConfiguration *configuration,
                                   GError **error);


G_END_DECLS
#endif                          /* __APT_SYSTEM_INTERNAL_H__ */
