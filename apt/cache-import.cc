/* import-variant.cc - Import APT cache into a GVariant-based cache->
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <apt-pkg/fileutl.h>
#include <apt-pkg/error.h>
#include <apt-pkg/init.h>
#include <apt-pkg/cachefile.h>
#include <apt-pkg/pkgcache.h>

#include <glib.h>
#include <glib/gstdio.h>

#include <apt/apt.h>
#include <apt/cache-internal.h>
#include <apt/cache-builder.h>
#include <apt/session.h>

#include <iostream>
#include <cstring>

extern "C"
G_GNUC_INTERNAL AptCache *apt_cache_import(AptConfiguration *configuration);

/*
 * get_deptype:
 * @deptype: An APT 0.X dependency type.
 *
 * Converts @deptype to an #AptDependencyType.
 */
static inline AptDependencyType get_deptype(unsigned char deptype)
{
    switch (deptype) {
    case pkgCache::Dep::Depends:
        return APT_DEPENDENCY_DEPENDS;
    case pkgCache::Dep::PreDepends:
        return APT_DEPENDENCY_PREDEPENDS;
    case pkgCache::Dep::Suggests:
        return APT_DEPENDENCY_SUGGESTS;
    case pkgCache::Dep::Recommends:
        return APT_DEPENDENCY_RECOMMENDS;
    case pkgCache::Dep::Conflicts:
        return APT_DEPENDENCY_CONFLICTS;
    case pkgCache::Dep::Replaces:
        return APT_DEPENDENCY_REPLACES;
    case pkgCache::Dep::DpkgBreaks:
        return APT_DEPENDENCY_BREAKS;
    case pkgCache::Dep::Enhances:
        return APT_DEPENDENCY_ENHANCES;
    default:
        g_assert_not_reached();
    }
    return APT_DEPENDENCY_INVALID;
}

/*
 * get_priority:
 * @priority: An APT 0.X priority.
 *
 * Converts @priority to an #AptPriority
 */
static inline AptPriority get_priority(unsigned char priority)
{
    switch (priority) {
    case pkgCache::State::Required:
        return APT_PRIORITY_REQUIRED;
    case pkgCache::State::Important:
        return APT_PRIORITY_IMPORTANT;
    case pkgCache::State::Standard:
        return APT_PRIORITY_STANDARD;
    case pkgCache::State::Optional:
        return APT_PRIORITY_OPTIONAL;
    case pkgCache::State::Extra:
        return APT_PRIORITY_EXTRA;
    default:
        return APT_PRIORITY_UNKNOWN;
    }
}

/*
 * get_selstate:
 * @state: An APT 0.X state.
 *
 * Converts @state to an #AptSelectionState
 */
static inline AptSelectionState get_selstate(unsigned char state)
{
    switch (state) {
    case pkgCache::State::Unknown:
        return APT_SELECTION_STATE_UNKNOWN;
    case pkgCache::State::Install:
        return APT_SELECTION_STATE_INSTALL;
    case pkgCache::State::Hold:
        return APT_SELECTION_STATE_HOLD;
    case pkgCache::State::DeInstall:
        return APT_SELECTION_STATE_DEINSTALL;
    case pkgCache::State::Purge:
        return APT_SELECTION_STATE_PURGE;

    }
    g_assert_not_reached();
}

/*
 * get_state_flags:
 * @pkg: An APT 0.X package object.
 *
 * Extracts the #AptPackageFlags from @pkg.
 */
static inline guchar get_state_flags(pkgCache::Package * pkg)
{
    guchar f = 0;
    if (pkg->Flags & pkgCache::Flag::Auto)
        f |= APT_PACKAGE_AUTO;
    switch (pkg->InstState) {
    case pkgCache::State::Ok:
        return f;
    case pkgCache::State::ReInstReq:
        return f | APT_PACKAGE_REINST_REQUIRED;
    case pkgCache::State::HoldInst:
        return f;
    case pkgCache::State::HoldReInstReq:
        return f | APT_PACKAGE_REINST_REQUIRED;
    }
    g_assert_not_reached();
}

/*
 * get_curstate:
 * @state: An APT 0.X state.
 *
 * Converts @state to an #AptCurrentState
 */
static inline AptCurrentState get_curstate(unsigned char state)
{
    switch (state) {
    case pkgCache::State::NotInstalled:
        return APT_CURRENT_STATE_NOT_INSTALLED;
    case pkgCache::State::ConfigFiles:
        return APT_CURRENT_STATE_CONFIG_FILES;
    case pkgCache::State::HalfInstalled:
        return APT_CURRENT_STATE_HALF_INSTALLED;
    case pkgCache::State::UnPacked:
        return APT_CURRENT_STATE_UNPACKED;
    case pkgCache::State::HalfConfigured:
        return APT_CURRENT_STATE_HALF_CONFIGURED;
    case pkgCache::State::TriggersAwaited:
        return APT_CURRENT_STATE_TRIGGERS_AWAITED;
    case pkgCache::State::TriggersPending:
        return APT_CURRENT_STATE_TRIGGERS_PENDING;
    case pkgCache::State::Installed:
        return APT_CURRENT_STATE_INSTALLED;
    }
    g_assert_not_reached();
}

/*
 * get_comptype:
 * @comp: An APT 0.X comparison operator.
 *
 * Converts @comp into the value stored in an APT 2 cache; that is,
 * an #AptComparisonType value.
 */
static inline guint8 get_comptype(unsigned char comp)
{
    switch (comp & ~pkgCache::Dep::Or) {
    case pkgCache::Dep::NoOp:
        return APT_COMPARISON_NONE;
    case pkgCache::Dep::LessEq:
        return APT_COMPARISON_LE;
    case pkgCache::Dep::GreaterEq:
        return APT_COMPARISON_GE;
    case pkgCache::Dep::Less:
        return APT_COMPARISON_LT;
    case pkgCache::Dep::Greater:
        return APT_COMPARISON_GT;
    case pkgCache::Dep::Equals:
        return APT_COMPARISON_EQ;
    case pkgCache::Dep::NotEquals:
        return APT_COMPARISON_NE;
    default:
        g_assert_not_reached();
    }
}

struct CacheBuilder {
    AptCacheBuilder *builder;
    coffset new_string(const gchar *s, gssize len = -1, bool dedup = false) {
        return apt_cache_builder_new_string(builder, s, len, dedup);
    }

    AptCachePackage *new_package() {
        return apt_cache_builder_new_package(builder);
    }

    AptCacheDependency *new_dependency(cindex *index) {
        return apt_cache_builder_new_dependency(builder, index);
    }

    AptCacheProvides *new_provides(cindex *index) {
        return apt_cache_builder_new_provides(builder, index);
    }

    AptCachePackageFile *new_file() {
        return apt_cache_builder_new_file(builder);
    }

    AptCacheDescription *new_description(cindex *index) {
        return apt_cache_builder_new_description(builder, index);
    }

    AptCacheGroupMember *new_group_member(AptCacheGroup *group,
                                          AptCacheGroupMemberType type,
                                          cindex *index=NULL) {
        return apt_cache_builder_new_group_member(builder, group, type, index);
    }

    AptCacheGroup *new_group(const gchar *name, gssize length = -1) {
        return apt_cache_builder_new_group(builder, name, length);
    }

    CacheBuilder() {
        builder = apt_cache_builder_new();
    }

    AptCache *end() {
        return apt_cache_builder_end(builder);
    }
};

/*
 * Desc:
 *
 * In APT 2, three objects from APT 0.X have been combined into a single
 * one (#AptDescription). This has been done because the old objects were
 * largely equal and combining them saves space.
 */
struct Desc {
    pkgCache::VerFileIterator ver;
    pkgCache::DescIterator desc;
    pkgCache::DescFileIterator descfile;
};

struct Importer {
    CacheBuilder builder;
    pkgCacheFile cachefile;
    pkgCache *cache;
    pkgSourceList sources;
    pkgRecords *records;
    GTimer *timer;
    guint32 *vermap;
    AptConfiguration *configuration;

    Importer(AptConfiguration *configuration) {
        pkgInitConfig(*_config);
        pkgInitSystem(*_config, _system);
        timer = g_timer_new();
        if (!cachefile.Open(NULL, false)) {
            _error->DumpErrors();
        }
        cache = cachefile;
        records = new pkgRecords(*cache);
        sources.ReadMainList();
        vermap = new guint32[cache->HeaderP->VersionCount];
        this->configuration = configuration;
    }
    std::ostream &log() {
        std::clog.precision(3);
        return std::clog << "[" << std::fixed
                         << g_timer_elapsed (timer, NULL) << "]\t";
    }

    ~Importer() {
        g_timer_destroy(timer);
        delete records;
        delete[] vermap;
    }

    AptCache *import();
    void add_descriptions(AptCachePackage *package, pkgCache::VerIterator &ver)
    {
        package->begin_descriptions = CINDEX_NONE;
        Desc descs[cache->HeaderP->PackageFileCount];
        for (pkgCache::VerFileIterator verfile = ver.FileList();
             !verfile.end(); verfile++)
            descs[verfile.File()->ID].ver = verfile;

        for (pkgCache::DescIterator desc = ver.DescriptionList();
             !desc.end(); desc++)
            for (pkgCache::DescFileIterator descfile = desc.FileList();
                 !descfile.end(); descfile++) {

                descs[descfile.File()->ID].descfile = descfile;
                descs[descfile.File()->ID].desc = desc;
            }

        for (guint8 i = 0; i < cache->HeaderP->PackageFileCount; i++) {
            std::string filename;
            const gchar *md5 = NULL, *language = NULL;
            guint32 offset = 0;
            cindex desc_id;
            if (descs[i].ver.end() && descs[i].desc.end())
                continue;
            if (!descs[i].ver.end()) {
                offset = descs[i].ver->Offset;
                pkgRecords::Parser & parser = records->Lookup(descs[i].ver);
                filename = parser.FileName();
            }
            if (!descs[i].desc.end()) {
                offset = descs[i].descfile->Offset;
                md5 = descs[i].desc.md5();
                language = descs[i].desc.LanguageCode();
            }

            AptCacheDescription *description;
            description = builder.new_description(&desc_id);
            description->file_id = i;
            description->filename = builder.new_string(filename.c_str(), filename.length(), false);
            description->desc_hash = builder.new_string(md5, -1, false);
            description->language = builder.new_string(language, -1, true);
            description->desc_offset = offset;
            description->next = CINDEX_NONE;

            if (package->begin_descriptions == CINDEX_NONE)
                package->begin_descriptions = desc_id;
            else
                (description-1)->next = desc_id;
        }
    }
    void add_provides(AptCachePackage *package, pkgCache::VerIterator &ver)
    {
        package->begin_provides = package->end_provides = CINDEX_NONE;
        for (pkgCache::PrvIterator prov = ver.ProvidesList(); !prov.end(); prov++) {
            AptCacheProvides *provides;
            AptCacheGroup *group;
            cindex i;

            group = builder.new_group(prov.Name(), -1);

            provides = builder.new_provides(&package->end_provides);
            provides->name = group->name;
            provides->version = builder.new_string(prov.ProvideVersion());
            if (package->begin_provides == CINDEX_NONE)
                package->begin_provides = package->end_provides;

            AptCacheGroupMember *member = builder.new_group_member(group, APT_CACHE_GROUP_MEMBER_PROVIDES, &i);
            member->package_id = vermap[ver->ID];
            member->child = package->end_provides - package->begin_provides;
        }
        if (package->end_provides != CINDEX_NONE)
            package->end_provides++;
    }

    void add_dependencies(AptCachePackage *package, pkgCache::VerIterator &ver)
    {
        package->begin_dependencies = package->end_dependencies = CINDEX_NONE;
        // Insert the dependencies.
        for (pkgCache::DepIterator dep = ver.DependsList(); !dep.end(); dep++) {
            AptCacheDependency *dependency;
            AptCacheGroup *group;
            cindex i;

            group = builder.new_group(dep.TargetPkg().Name(), -1);
            dependency = builder.new_dependency(&package->end_dependencies);
            dependency->target_name = group->name;
            dependency->target_version = builder.new_string(dep.TargetVer());
            dependency->target_architecture = 0;
            dependency->comparison = get_comptype(dep->CompareOp);
            dependency->next_is_or = bool(dep->CompareOp & pkgCache::Dep::Or);
            dependency->type = get_deptype(dep->Type);
            if (package->begin_dependencies == CINDEX_NONE)
                package->begin_dependencies = package->end_dependencies;

            AptCacheGroupMember *member = builder.new_group_member(group, APT_CACHE_GROUP_MEMBER_DEPENDS, &i);
            member->package_id = vermap[ver->ID];
            member->child = package->end_dependencies - package->begin_dependencies;
        }
        if (package->end_dependencies != CINDEX_NONE)
            package->end_dependencies++;
    }

    void add_package(pkgCache::PkgIterator &, pkgCache::VerIterator &);

    void add_file(pkgCache::PkgFileIterator &file);
};

void Importer::add_file(pkgCache::PkgFileIterator &file)
{
    pkgIndexFile *found;
    guchar flags = file->Flags;
    string uri = "/var/lib/dpkg/status";
    if (sources.FindIndex(file, found)) {
        uri = found->ArchiveURI("");
        if (found->IsTrusted())
            flags |= APT_PACKAGE_FILE_IS_TRUSTED;
    }

    AptCachePackageFile *cfile = builder.new_file();
    cfile->filename = builder.new_string(file.FileName(), -1, true);
    cfile->archive = builder.new_string(file.Archive(), -1, true);
    cfile->codename = builder.new_string(file.Codename(), -1, true);
    cfile->component = builder.new_string(file.Component(), -1, true);
    cfile->version = builder.new_string(file.Version(), -1, true);
    cfile->origin = builder.new_string(file.Origin(), -1, true);
    cfile->label = builder.new_string(file.Label(), -1, true);
    cfile->site = builder.new_string(file.Site(), -1, true);
    cfile->type = builder.new_string(file.IndexType(), -1, true);
    cfile->base_uri = builder.new_string(uri.c_str(), uri.length(), true);
    cfile->mtime = file->mtime;
    cfile->flags = flags;
}

void Importer::add_package(pkgCache::PkgIterator &pkg, pkgCache::VerIterator &ver)
{
    string spkg, sver, checksum;
    for (pkgCache::VerFileIterator verfile = ver.FileList();
         !verfile.end(); verfile++) {
        pkgRecords::Parser &parser = records->Lookup(verfile);
        if (spkg.empty())
            spkg = parser.SourcePkg();
        if (sver.empty())
            sver = parser.SourceVer();

        if (checksum.size() < 64)
            checksum = parser.SHA256Hash();
        if (checksum.size() < 30)
            checksum = parser.SHA1Hash();
        if (checksum.empty())
            checksum = parser.MD5Hash();
        if ((!spkg.empty() || !sver.empty()) && checksum.size() == 64)
            break;
    }
    AptCachePackage *package = builder.new_package();
    AptCacheGroup *group;
    cindex i;

    add_dependencies(package, ver);
    add_provides(package, ver);

    g_assert(ver->Size < G_MAXOFFSET);
    g_assert(ver->InstalledSize < G_MAXOFFSET);

    group = builder.new_group(pkg.Name(), -1);

    package->name = group->name;
    package->version = builder.new_string(ver.VerStr());
    package->architecture = builder.new_string(ver.Arch(), -1, true);
    package->checksum = builder.new_string(checksum.c_str(), checksum.length(), false);
    package->size = ver->Size;
    package->installed_size = ver->InstalledSize;
    package->multi_arch = APT_MULTI_ARCH_NONE;
    package->priority = get_priority(ver->Priority);
    package->section = builder.new_string(ver.Section(), -1, true);
    package->source_version = builder.new_string(sver.c_str(), sver.length());

    if (ver == pkg.CurrentVer()) {
        package->state_current = get_curstate(pkg->CurrentState);
        package->state_selection = get_selstate(pkg->SelectedState);
        package->flags = get_state_flags(pkg);
    } else {
        package->state_current = 0;
        package->state_selection = 0;
        package->flags = 0;
    }

    AptCacheGroupMember *member = builder.new_group_member(group, APT_CACHE_GROUP_MEMBER_BINARY, &i);
    member->package_id = vermap[ver->ID];

    if (!spkg.empty()) {
        group = builder.new_group(spkg.c_str(), spkg.length());
        package->source_name = group->name;
    }
    member = builder.new_group_member(group, APT_CACHE_GROUP_MEMBER_SOURCE, &i);
    member->package_id = vermap[ver->ID];

    add_descriptions(package, ver);
}

AptCache *Importer::import() {
    guint32 pkgid = 0;

    log() << "Processing packages...\r";
    for (pkgCache::PkgIterator pkg = cache->PkgBegin(); pkg != cache->PkgEnd();
         pkg++) {
        for (pkgCache::VerIterator ver = pkg.VersionList(); !ver.end(); ver++) {
            if (++pkgid % 10000 == 0 || pkgid == cache->HeaderP->VersionCount)
                std::clog << "\tProcessing package " << pkgid << " of " <<
                    cache->HeaderP->VersionCount << "\r";

            vermap[ver->ID] = pkgid - 1;
            add_package(pkg, ver);
        }
    }
    std::clog << "\n";

    log() << "Processing package files...\n";
    for (guint i = 0; i < cache->HeaderP->PackageFileCount; i++) {
        for (pkgCache::PkgFileIterator f = cache->FileBegin(); !f.end(); f++) {
            if (f->ID == i)
                add_file(f);
        }
    }

    log() << "Building the final cache...\n";
    AptCache *r = builder.end();
    gchar *write_file = apt_configuration_get_file(configuration,
                                                   "dir::cache::cache");
    gchar *write_dir = g_path_get_dirname(write_file);
    if (g_access(write_dir, W_OK) == 0) {
        GError *e = NULL;
        log() << "Storing the cache on the disk...\n";
        if (!g_file_set_contents(write_file, (const gchar *) r->data,
                                 r->header->end_of_file, &e))
            std::cerr << "Could not write database file: " << e->message << "\n";
    }
    log() << "Completed\n";

    g_free(write_file);
    g_free(write_dir);

    return r;
}

/*
 * apt_cache_import:
 * @configuration: The #AptConfiguration configuring the location of the cache->
 *
 * Imports the APT 0.X cache into the APT 2 format and if possible, stores
 * the cache at the path set in the configuration variable Dir::cache::cache->
 *
 * The performance is not optimal, that's caused by many file seeks when using
 * pkgRecords and by far too many string duplications.
 *
 * Return value: A new #Aptcache->
 */
AptCache *apt_cache_import(AptConfiguration *configuration) {
    Importer i(configuration);
    return i.import();
}
