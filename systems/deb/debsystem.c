/* debsystem.c - The Debian system for APT2.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>

#include <unistd.h>
#include <errno.h>

#include <apt/apt.h>
#include <apt/system-internal.h>
#include "debsystem.h"

typedef struct DebSystem {
    AptSystem base;
    gint lock_count;
    gint lock_fd;
} DebSystem;

static gboolean deb_lock(AptSystem *system_, GError **error)
{
    DebSystem *system = (DebSystem *) system_;
    gboolean res = FALSE;
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);
    if (system->lock_count++ == 0) {
        gchar *status = apt_configuration_get_file(system->base.configuration,
                                                   "dir::state::status");
        gchar *dirname = g_path_get_dirname(status);
        gchar *lockfile = g_build_filename(dirname, "lock", NULL);

        system->lock_fd = apt_utils_lock_file(lockfile, error);
        res = (system->lock_fd != -1);

        g_free(status);
        g_free(dirname);
        g_free(lockfile);
    }
    return res;
}

static gboolean deb_unlock(AptSystem *system_, GError **error)
{
    DebSystem *system = (DebSystem *) system_;
    gboolean res = TRUE;
    g_return_val_if_fail(error == NULL || *error == NULL, FALSE);
    g_warn_if_fail(system->lock_fd != -1 && system->lock_count > 0);
    if (--system->lock_count == 0 && close(system->lock_fd) == -1) {
        g_set_error(error, G_FILE_ERROR, g_file_error_from_errno(errno),
                    _("Error while unlocking system: %s"), g_strerror(errno));
        res = FALSE;
    }
    return res;
}

static gboolean deb_initialize(AptSystem *system, GError **error)
{
    (void) error;

    if (apt_configuration_get(system->configuration, "Dir::State::status")
        == NULL)
        apt_configuration_set(system->configuration, "Dir::State::status",
                              "/var/lib/dpkg/status");
    if (apt_configuration_get(system->configuration, "Dir::Bin::dpkg")
        == NULL)
        apt_configuration_set(system->configuration, "Dir::Bin::dpkg",
                              "/usr/bin/dpkg");
    return TRUE;
}

G_MODULE_EXPORT const AptSystemDesc apt_system_desc = {
    "Debian System",
    "Julian Andres Klode <jak@debian.org>",
    sizeof(DebSystem),
    deb_initialize,
    NULL,
    deb_compare_versions,
    deb_lock,
    deb_unlock
};
