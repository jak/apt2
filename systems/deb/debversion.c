/* debversion.c - Handling of Debian versioning.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>

#include "debsystem.h"

typedef struct DebVersion {
    const gchar *epoch;
    const gchar *version;
    const gchar *version_end;
    const gchar *revision;
    const gchar *revision_end;
} DebVersion;

static void deb_parsever(struct DebVersion *version, const gchar *v)
{
    const gchar *epoch_end = strchr(v, ':');
    const gchar *version_end = strrchr(v, '-');
    const gchar *complete_end = v + strlen(v);

    version->epoch = (epoch_end == NULL) ? "" : v;
    version->version = (epoch_end == NULL) ? v : epoch_end + 1;
    version->version_end = (version_end == NULL) ? complete_end : version_end;
    version->revision = (version_end == NULL) ? "0" : version_end + 1;
    version->revision_end = ((version_end == NULL) ? version->revision + 1
                             : complete_end);
}

/*
 * cmp_number:
 * @a: The first number as a string.
 * @b: The second number as a string.
 * @pa: A pointer to a pointer which will be set to the first non-digit in @a.
 * @pb: A pointer to a pointer which will be set to the first non-digit in @b.
 *
 * Compares two numbers that are represented as strings
 * against each other. Compared to converting to an int
 * and comparing the integers, this has the advantage
 * that it does not cause overflow.
 */
static gint cmp_number(const gchar *a, const gchar *b, const gchar **pa,
                       const gchar **pb)
{
    /* const gchar *ra = a, *rb = b; */
    gint res = 0;
    if (*a == '\0' && *b == '\0')
        return 0;
    for (; *a == '0'; a++);
    for (; *b == '0'; b++);
    for (; g_ascii_isdigit(*a) && g_ascii_isdigit(*b); a++, b++) {
        if (res == 0 && *a != *b)
            res = *a < *b ? -1 : 1;
    }
    if (g_ascii_isdigit(*a)) {
        if (!g_ascii_isdigit(*b))
            res = 1;
    } else if (g_ascii_isdigit(*b)) {
        res = -1;
    }
    if (pa != NULL) {
        g_assert(pb != NULL);
        *pa = a;
        *pb = b;
    }
    return res;
}

static gint cmp_part(const gchar *a, const gchar *a_end,
                     const gchar *b, const gchar *b_end)
{
    while (a != a_end || b != b_end) {
        int cmpres;
        for (; !g_ascii_isdigit(*a) || !g_ascii_isdigit(*b); a++, b++) {
            if (a == a_end && b == b_end)
                return 0;
            else if (*a == *b && a != a_end && b != b_end)
                continue;
            /* Tilde always sorts first; i.e., the string with tilde loses */
            else if (*a == '~' || *b == '~')
                return (*a == '~') ? -1 : 1;
            /* One string is empty, other is a number -> go into number mode */
            else if ((a == a_end && *b == '0') || (b == b_end && *a == '0'))
                return cmp_number(a, b, NULL, NULL);
            /* One string is empty, other is not a number -> other wins */
            else if (a == a_end || b == b_end)
                return (a == a_end) ? -1 : 1;
            /* One non-digit part is shorter than the other one */
            else if (g_ascii_isdigit(*a) != g_ascii_isdigit(*b))
                return g_ascii_isdigit(*a) ? -1 : 1;
            /* Alpha looses against not alpha */
            else if (g_ascii_isalpha(*a) != g_ascii_isalpha(*b))
                return g_ascii_isalpha(*a) ? -1 : 1;
            /* Standard ASCII comparison */
            else
                return *a < *b ? -1 : 1;
        }

        /* Now compare numbers */
        cmpres = cmp_number(a, b, &a, &b);
        if (cmpres != 0 || (a == a_end && b == b_end))
            return cmpres;
    }

    return 0;
}

gint deb_compare_versions(const gchar *a, const gchar *b)
{
    gint res = 0;
    DebVersion vera, verb;

    g_return_val_if_fail(a != NULL && b != NULL, INT_MIN);

    if (a == b)
        return 0;
    /* Optimize the case of differing single digit epochs. */
    if (*a != *b && *a != '\0' && *b != '\0' && a[1] == ':' && b[1] == ':')
        return *a < *b ? -1 : 1;

    deb_parsever(&vera, a);
    deb_parsever(&verb, b);

    if (G_UNLIKELY(vera.epoch != verb.epoch)
        && (res = cmp_number(vera.epoch, verb.epoch, NULL, NULL)) != 0)
        goto out;
    else if ((res = cmp_part(vera.version, vera.version_end,
                             verb.version, verb.version_end)) != 0)
        goto out;
    /* Optimizes for native packages (where revision is a string literal) */
    else if (G_LIKELY(vera.revision != verb.revision) &&
             (res = cmp_part(vera.revision, vera.revision_end,
                             verb.revision, verb.revision_end)) != 0)
        goto out;
  out:
    return res;
}
