/* testcache.c - Tests for the APT2 cache.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <apt/apt.h>

#include <string.h>

#include <glib.h>

extern gboolean apt_system_force_local;

static void test_iterator(gconstpointer cache_)
{
    gsize i = 0;
    AptCache *cache = (AptCache *) cache_;
    AptPackageIter *iter;
    AptPackage *p1, *p2;

    iter = apt_cache_iterator(cache);
    while ((p1 = apt_package_iter_next_value(iter))) {
        p2 = apt_cache_get_package(cache, i);
        g_assert_cmpuint(apt_package_get_id(p1), ==, i);
        g_assert_cmpuint(apt_package_get_id(p2), ==, i);
        apt_package_unref(p1);
        apt_package_unref(p2);
        i++;
    }
    apt_package_iter_free(iter);
    g_assert_cmpuint(i, ==, apt_cache_n_packages(cache));
}

static void test_description(gconstpointer cache_)
{
    gsize i = 0;
    AptCache *cache = (AptCache *) cache_;

    for (i = 0; i < apt_cache_n_packages(cache); i++) {
        AptPackage *package = apt_cache_get_package(cache, i);
        AptDescription *desc;
        AptDescriptionIter *iter;
        gsize desc_count = 0;

        iter = apt_package_get_descriptions(package);

        while ((desc = apt_description_iter_next_value(iter))) {
            desc_count++;
            g_assert_cmpuint(apt_description_get_package_file_id(desc), <=,
                             apt_cache_n_packagefiles(cache));

            apt_description_unref(desc);
        }
        g_assert_cmpuint(desc_count, >, 0);
        apt_description_iter_free(iter);
        apt_package_unref(package);
    }
}

static void test_relations(gconstpointer cache_)
{
    gsize i = 0;
    AptCache *cache = (AptCache *) cache_;

    for (i = 0; i < apt_cache_n_packages(cache); i++) {
        AptPackage *package = apt_cache_get_package(cache, i);
        gsize i;
        for (i = 0; i < apt_package_n_dependencies(package); i++) {
            AptDependency *dependency;
            AptDependencyType type;

            dependency = apt_package_get_dependency(package, i);
            g_assert(dependency != NULL);

            type = apt_dependency_get_type(dependency);
            g_assert(type > 0);

            g_assert(apt_dependency_get_name(dependency) != NULL);
            g_assert(apt_dependency_get_version(dependency)
                     ? apt_dependency_get_comparison(dependency) != 0
                     : apt_dependency_get_comparison(dependency) == 0);
            g_assert(apt_dependency_type_rate(type) != -1 ||
                     !apt_dependency_get_next_is_or(dependency));

            apt_dependency_unref(dependency);
        }
        for (i = 0; i < apt_package_n_provides(package); i++) {
            AptProvides *provides = apt_package_get_provides(package, i);
            g_assert(provides != NULL);
            g_assert(apt_provides_get_name(provides) != NULL);
            apt_provides_get_version(provides);
            apt_provides_unref(provides);
        }
        apt_package_unref(package);
    }
}

int main(int argc, char *argv[])
{
    GError *error = NULL;
    AptSession *session;
    AptCache *cache;
    gint res;

    apt_system_force_local = TRUE;

    g_test_init(&argc, &argv, NULL);

    session = apt_session_new(&error);
    g_assert_no_error(error);
    cache = apt_session_get_cache(session);

    g_test_add_data_func("/testcache/iterator", cache, test_iterator);
    g_test_add_data_func("/testcache/descriptions", cache, test_description);
    g_test_add_data_func("/testcache/relations", cache, test_relations);

    res = g_test_run();

    apt_session_unref(session);
    return res;
}
