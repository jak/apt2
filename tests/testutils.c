/* testutils.c - Tests for apt/utils.h.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <apt/utils.h>
#include <string.h>

static void test_match(void)
{
    g_assert(apt_utils_match("/a/", "aa"));
    g_assert(apt_utils_match("/^a[a-z]$/", "aa"));
    g_assert(!apt_utils_match("/^a$/", "aa"));
    g_assert(apt_utils_match("a*", "aa"));
    g_assert(!apt_utils_match("b*", "aa"));
    g_assert(!apt_utils_match("a", "aa"));
    g_assert(apt_utils_match("a?a", "aba"));
    g_assert(!apt_utils_match("a?a", "abba"));
}

static void test_uri(void)
{
    gchar *res;
    res = apt_utils_uri_to_filename("scheme://user@host.domain/~dir/file.deb");
    g_assert_cmpstr(res, ==, "host.domain_%7edir_file.deb");
    g_free(res);

    /* The same thing without user should give the same result */
    res = apt_utils_uri_to_filename("scheme://host.domain/~dir/file.deb");
    g_assert_cmpstr(res, ==, "host.domain_%7edir_file.deb");
    g_free(res);

    /* Only one slash after colon; for example, local files */
    res = apt_utils_uri_to_filename("file:/dir/file.deb");
    g_assert_cmpstr(res, ==, "dir_file.deb");
    g_free(res);

    /* This has one slash to much, the file has to start with an underscore */
    res = apt_utils_uri_to_filename("scheme:///host.domain/~dir/file.deb");
    g_assert_cmpstr(res, ==, "_host.domain_%7edir_file.deb");
    g_free(res);

}

int main(int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);
    g_test_add_func("/testutils/match", test_match);
    g_test_add_func("/testutils/uri", test_uri);
    return g_test_run();
}
