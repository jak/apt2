/* testconfiguration.c - Tests for AptConfiguration.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib.h>
#include <apt/apt.h>

static void test_defaults(void)
{
    AptConfiguration *configuration = apt_configuration_new();

    g_assert(apt_configuration_get(configuration, "Dir") == NULL);
    g_assert(apt_configuration_get_file(configuration, "Dir") == NULL);
    g_assert(apt_configuration_get_boolean(configuration, "Dir") == FALSE);
    apt_configuration_unref(configuration);
}

static void test_files(void)
{
    gchar *f;
    AptConfiguration *configuration = apt_configuration_new();
    apt_configuration_set(configuration, "magic::int", "314159265");
    g_assert_cmpint(apt_configuration_get_int(configuration, "magic::int"),
                    ==, 314159265);
    apt_configuration_set(configuration, "Dir", "/mydir");
    apt_configuration_set(configuration, "dir::relative", "relative");
    apt_configuration_set(configuration, "dir::absolute", "/absolute");

    g_assert_cmpstr(f =
                    apt_configuration_get_file(configuration, "dir::relative"),
                    ==, "/mydir/relative");
    g_free(f);
    g_assert_cmpstr(f =
                    apt_configuration_get_file(configuration, "dir::absolute"),
                    ==, "/absolute");
    g_free(f);

    apt_configuration_set(configuration, "rootdir", "/ROOT");
    g_assert_cmpstr(f = apt_configuration_get_file(configuration,
                                                   "dir::relative"), ==,
                    "/ROOT/mydir/relative");

    g_free(f);
    g_assert_cmpstr(f = apt_configuration_get_file(configuration,
                                                   "dir::absolute"),
                    ==, "/ROOT/absolute");
    g_free(f);
    apt_configuration_unref(configuration);
}

static void test_lists(void)
{
    gsize length;
    const gchar **list;
    AptConfiguration *configuration = apt_configuration_new();
    apt_configuration_set(configuration, "A::", "a");
    apt_configuration_set(configuration, "A::", "b");

    list = apt_configuration_get_list(configuration, "A::", &length);
    g_assert_cmpuint(length, ==, 2);
    g_assert_cmpstr(list[0], ==, "a");
    g_assert_cmpstr(list[1], ==, "b");
    apt_configuration_unref(configuration);
}

static void test_parser(void)
{
    GError *e = NULL;
    AptConfiguration *c = apt_configuration_new();
    g_assert(apt_configuration_parse_text(c, "Option \"value\";", NULL));
    g_assert_cmpstr(apt_configuration_get(c, "option"), ==, "value");
    g_assert(apt_configuration_parse_text(c, "// This is a comment", NULL));
    g_assert(!apt_configuration_parse_text(c, "This is not a comment", &e));
    g_assert_error(e, APT_CONFIGURATION_ERROR, APT_CONFIGURATION_ERROR_FAILED);
    g_clear_error(&e);

    g_assert(!apt_configuration_parse_text(c, "Option \"value\"", &e));
    g_assert_error(e, APT_CONFIGURATION_ERROR, APT_CONFIGURATION_ERROR_FAILED);
    g_clear_error(&e);
    apt_configuration_unref(c);
}

static void test_dumps_and_parse(void)
{
    GError *e = NULL;
    gchar *text1;
    gchar *text2;
    AptConfiguration *config1 = apt_configuration_new();
    AptConfiguration *config2 = apt_configuration_new();
    apt_configuration_init_defaults(config1, NULL);
    text1 = apt_configuration_dumps(config1);
    /* Initialize configuration 2 */
    g_assert(apt_configuration_parse_text(config2, text1, &e));
    g_assert_no_error(e);
    text2 = apt_configuration_dumps(config2);
    g_assert_cmpstr(text1, ==, text2);

    g_free(text1);
    g_free(text2);
    apt_configuration_unref(config1);
    apt_configuration_unref(config2);
}

int main(int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);
    g_test_add_func("/testconfiguration/defaults", test_defaults);
    g_test_add_func("/testconfiguration/files", test_files);
    g_test_add_func("/testconfiguration/lists", test_lists);
    g_test_add_func("/testconfiguration/parser", test_parser);
    g_test_add_func("/testconfiguration/dumps-parse", test_dumps_and_parse);
    return g_test_run();
}
