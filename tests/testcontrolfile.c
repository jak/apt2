/* testcontrolfile.c - Tests for #AptControlFile.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib.h>
#include <apt/apt.h>
#include <string.h>

static void test_control_section(void)
{
    GList *keys;
    const gchar *raw;
    gsize length;
    const gchar *end;
    /* Starts with a newline to test newline skipping */
    const gchar *section = ("\n"
                            "Package: apt\n"
                            "Version: test\n" "\n" "Body or next section");
    AptControlSection *cs;
    cs = apt_control_section_new(section, strlen(section), &end);
    g_assert(cs != NULL);
    keys = apt_control_section_get_keys(cs);
    g_assert_cmpstr(apt_control_section_get(cs, "Package"), ==, "apt");
    g_assert_cmpstr(apt_control_section_get(cs, "Version"), ==, "test");
    g_assert_cmpstr(apt_control_section_get(cs, ""), ==, NULL);
    /* TODO: This is wrong, it should really be case insensitive. */
    g_assert_cmpstr(apt_control_section_get(cs, "version"), ==, "test");
    g_assert_cmpstr(apt_control_section_get(cs, "package"), ==, "apt");

    raw = apt_control_section_get_raw(cs, "Package", TRUE, &length);
    g_assert(strncmp(raw, "Package: apt", length) == 0);
    raw = apt_control_section_get_raw(cs, "Package", FALSE, &length);
    g_assert(strncmp(raw, "apt", length) == 0);

    g_assert_cmpstr(end, ==, "Body or next section");
    g_assert_cmpuint(g_list_length(keys), ==, 2);
    g_assert(strcmp(keys->data, "Package") == 0 ||
             strcmp(keys->data, "Version") == 0);
    g_list_free(keys);
    apt_control_section_unref(cs);
}

int main(int argc, char *argv[])
{
    g_test_init(&argc, &argv, NULL);
    g_test_add_func("/testcontrolfile/controlsection", test_control_section);

    return g_test_run();
}
