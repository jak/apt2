/* testpolicy.c - Tests for #AptPolicy.
 *
 * Copyright (C) 2010 Julian Andres Klode <jak@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <glib.h>
#include <apt/apt.h>
#include <apt/policy-internal.h>
#include <apt/cache-builder.h>
#include <string.h>

static AptPolicy policy;

static AptPin *pin_from_data(const gchar *data, gsize length, GError **e)
{
    AptControlSection *section;
    AptPin *pin;
    if (e != NULL)
        *e = NULL;
    section = apt_control_section_new(data, length, NULL);
    pin = apt_pin_new(&policy, section, e);
    apt_control_section_unref(section);
    return pin;
}

/* SL:
 * Convert a string into two parameters for a function (string, length).
 */
#define SL(s) s, strlen(s)

static void test_package_file_release(void)
{
    GError *e = NULL;
    AptPin *pin = pin_from_data(SL("Package: *\n"
                                   "Pin: release a=archive, c=component,\n"
                                   "             n=codename, l=label,\n"
                                   "             o=origin, v=version\n"
                                   "Pin-Priority: 134\n\n"), &e);
    g_assert_no_error(e);
    g_assert(pin->packages == NULL);
    g_assert(pin->type == APT_PIN_RELEASE);
    g_assert_cmpstr(pin->pin.release.archive, ==, "archive");
    g_assert_cmpstr(pin->pin.release.codename, ==, "codename");
    g_assert_cmpstr(pin->pin.release.component, ==, "component");
    g_assert_cmpstr(pin->pin.release.label, ==, "label");
    g_assert_cmpstr(pin->pin.release.origin, ==, "origin");
    g_assert_cmpstr(pin->pin.release.version, ==, "version");
    g_assert_cmpint(pin->priority, ==, 134);
    apt_pin_free(pin);
}

static void test_package_release(void)
{
    GError *e = NULL;
    AptPin *pin = pin_from_data(SL("Package: apt\n"
                                   "Pin: release a=archive, c=component,\n"
                                   "             n=codename, l=label,\n"
                                   "             o=origin, v=version\n"
                                   "Pin-Priority: 134\n\n"), &e);
    g_assert_no_error(e);
    g_assert_cmpstr(pin->packages[0], ==, "apt");
    g_assert_cmpstr(pin->packages[1], ==, NULL);
    g_assert(pin->type == APT_PIN_RELEASE);
    g_assert_cmpstr(pin->pin.release.archive, ==, "archive");
    g_assert_cmpstr(pin->pin.release.codename, ==, "codename");
    g_assert_cmpstr(pin->pin.release.component, ==, "component");
    g_assert_cmpstr(pin->pin.release.label, ==, "label");
    g_assert_cmpstr(pin->pin.release.origin, ==, "origin");
    g_assert_cmpstr(pin->pin.release.version, ==, "version");
    g_assert_cmpint(pin->priority, ==, 134);
    apt_pin_free(pin);
}

static void test_multiple_packages(void)
{
    GError *e = NULL;
    AptPin *pin = pin_from_data(SL("Package: apt apt2\n"
                                   "Pin: release a=a\n"
                                   "Pin-Priority: 134\n"), &e);
    g_assert_no_error(e);
    g_assert_cmpstr(pin->packages[0], ==, "apt");
    g_assert_cmpstr(pin->packages[1], ==, "apt2");
    g_assert_cmpstr(pin->packages[2], ==, NULL);
    apt_pin_free(pin);
}

static void test_version(void)
{
    GError *e = NULL;
    /* Check that specific packages works with Pin: version */
    AptPin *pin = pin_from_data(SL("Package: apt\n"
                                   "Pin: version 5.1\n"
                                   "Pin-Priority: 134\n"), &e);
    g_assert_no_error(e);
    g_assert(pin != NULL);
    g_assert(pin->type == APT_PIN_VERSION);
    g_assert_cmpstr(pin->pin.version, ==, "5.1");
    apt_pin_free(pin);
    /* Check that Package: * does not work with Pin: version */
    pin = pin_from_data(SL("Package: *\n"
                           "Pin: version 5.1\n" "Pin-Priority: 134\n"), &e);
    g_assert_error(e, APT_POLICY_ERROR, APT_POLICY_ERROR_INVALID_PIN);
    g_assert(pin == NULL);
    g_error_free(e);
}

static void test_origin(void)
{
    GError *e = NULL;
    /* Check that specific packages work with Pin: origin */
    AptPin *pin = pin_from_data(SL("Package: apt\n"
                                   "Pin: origin origin.example\n"
                                   "Pin-Priority: 134\n"), &e);
    g_assert_no_error(e);
    g_assert(pin != NULL);
    g_assert(pin->type == APT_PIN_ORIGIN);
    g_assert_cmpstr(pin->pin.version, ==, "origin.example");
    apt_pin_free(pin);

    /* Check that Package: * works with Pin: origin as well */
    pin = pin_from_data(SL("Package: *\n"
                           "Pin: origin origin.example\n"
                           "Pin-Priority: 134\n"), &e);
    g_assert_no_error(e);
    g_assert(pin != NULL);
    g_assert(pin->type == APT_PIN_ORIGIN);
    g_assert_cmpstr(pin->pin.version, ==, "origin.example");
    apt_pin_free(pin);
}

static void test_apt_pin_matches_file(void)
{
    time_t mtime = 1274805443;
    AptPin *pin;
    AptCache *cache;
    AptCacheBuilder *builder;
    AptPackageFile *pf;
    AptCachePackageFile *cf;

    builder = apt_cache_builder_new();
    cf = apt_cache_builder_new_file(builder);
    cf->filename = apt_cache_builder_new_string(builder, "filename", -1, TRUE);
    cf->archive = apt_cache_builder_new_string(builder, "archive", -1, TRUE);
    cf->codename = apt_cache_builder_new_string(builder, "codename", -1, TRUE);
    cf->component = apt_cache_builder_new_string(builder, "component", -1,
                                                 TRUE);
    cf->version = apt_cache_builder_new_string(builder, "version", -1, TRUE);
    cf->origin = apt_cache_builder_new_string(builder, "origin", -1, TRUE);
    cf->label = apt_cache_builder_new_string(builder, "label", -1, TRUE);
    cf->site = apt_cache_builder_new_string(builder, "site", -1, TRUE);
    cf->type = apt_cache_builder_new_string(builder, "type", -1, TRUE);
    cf->base_uri = apt_cache_builder_new_string(builder, "archive://uri/", -1,
                                                TRUE);
    cf->mtime = mtime;
    cf->flags = APT_PACKAGE_FILE_NOT_AUTOMATIC;

    cache = apt_cache_builder_end(builder);
    pf = apt_cache_get_package_file(cache, 0);

    pin = g_slice_new(AptPin);
    pin->packages = NULL;
    pin->type = APT_PIN_ORIGIN;
    pin->pin.origin = g_strdup("site");
    g_assert(apt_pin_matches_file(pin, pf));

    g_assert(pin->type == APT_PIN_ORIGIN);
    apt_pin_free(pin);
    pin = g_slice_new0(AptPin);
    pin->packages = NULL;
    pin->type = APT_PIN_RELEASE;

    pin->pin.release.archive = g_strdup("archive");
    g_assert(apt_pin_matches_file(pin, pf));

    pin->pin.release.codename = g_strdup("codename");
    g_assert(apt_pin_matches_file(pin, pf));

    pin->pin.release.component = g_strdup("component");
    g_assert(apt_pin_matches_file(pin, pf));

    pin->pin.release.label = g_strdup("label");
    g_assert(apt_pin_matches_file(pin, pf));

    pin->pin.release.origin = g_strdup("origin");
    g_assert(apt_pin_matches_file(pin, pf));

    pin->pin.release.version = g_strdup("version");
    g_assert(apt_pin_matches_file(pin, pf));

    /* Modify it to not match anymore and retry */
    pin->pin.release.version[0] = 'V';
    g_assert(!apt_pin_matches_file(pin, pf));

    apt_pin_free(pin);
    apt_package_file_unref(pf);
    apt_cache_unref(cache);
}

int main(int argc, char *argv[])
{
    policy.package_regex = PACKAGE_REGEX;
    policy.pin_regex = PIN_REGEX;
    g_test_init(&argc, &argv, NULL);
    g_test_add_func("/testpolicy/packagefile-and-release",
                    test_package_file_release);
    g_test_add_func("/testpolicy/package-and-release", test_package_release);
    g_test_add_func("/testpolicy/multiple-packages", test_multiple_packages);
    g_test_add_func("/testpolicy/version", test_version);
    g_test_add_func("/testpolicy/origin", test_origin);
    g_test_add_func("/testpolicy/match-file", test_apt_pin_matches_file);
    return g_test_run();
}
