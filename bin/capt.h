#include <glib.h>

G_BEGIN_DECLS

int do_import(AptSession *session, int argc, char *argv[]);
int do_vertest(AptSession *session, int argc, char *argv[]);

G_END_DECLS
