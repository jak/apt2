#include <apt-pkg/init.h>
#include <apt-pkg/version.h>
#include <apt-pkg/pkgsystem.h>

#include <apt/apt.h>
#include "capt.h"

int do_vertest(AptSession *session, int /* argc */ , char * /* argv */ [])
{
    GTimer *t;
    AptSystem *system = apt_session_get_system(session);
    GCompareFunc compare_versions = apt_system_get_version_compare_func(system);
    AptCache *cache = apt_session_get_cache(session);
    gsize n = apt_cache_n_packages(cache);
    pkgInitConfig(*_config);
    pkgInitSystem(*_config, _system);
    const gchar **vers = g_newa(const gchar *, n);
    gsize diff = 0, cmp = 0;

    GHashTable *h = g_hash_table_new(g_str_hash, g_str_equal);

    for (gsize i = 0; i < n; i++) {
        AptPackage *pkg = apt_cache_get_package(cache, i);
        vers[i] = apt_package_get_version(pkg);
        // only insert new version strings, reduces the amount of
        // comparisons considerably.
        if (g_hash_table_lookup(h, (gpointer) vers[i]))
            vers[i] = NULL;
        else
            g_hash_table_insert(h, (gpointer) vers[i], (gpointer) vers[i]);
        apt_package_unref(pkg);
    }
    t = g_timer_new();
    for (gsize i = 0; i < n; i++) {
        if (i % 250 == 0)
            g_print("Processing: %.1f%%\r", 100.0 * i / n);
        if (vers[i] == NULL)
            continue;
        for (gsize ii = 0; ii < n; ii++) {
            if (vers[ii] == NULL)
                continue;
            int old = _system->VS->CmpVersion(vers[i], vers[ii]);
            int neo = compare_versions(vers[i], vers[ii]);
            if (old < 0)
                old = -1;
            if (neo < 0)
                neo = -1;
            if (old > 0)
                old = 1;
            if (neo > 0)
                neo = 1;
            if (old != neo) {
                g_printerr("[old=%d,new=%d] a=%s b=%s\n", old, neo, vers[i],
                           vers[ii]);
                g_print("Processing: %.1f%%\r", 100.0 * i / n);
                diff++;
            }
            cmp++;
        }
    }
    g_print("                  \n");

    // We use SUSv2 flag for grouping thousands, mark it as an extension
    // so gcc does not warn us in pedantic mode.
    G_GNUC_EXTENSION g_print("N(Packages): %'" G_GSIZE_FORMAT "\n", n);
    G_GNUC_EXTENSION g_print("NPackages^2: %'" G_GSIZE_FORMAT "\n", n * n);
    G_GNUC_EXTENSION g_print("Comparisons: %'" G_GSIZE_FORMAT "\n", cmp);
    G_GNUC_EXTENSION g_print("Differences: %'" G_GSIZE_FORMAT "\n", diff);
    G_GNUC_EXTENSION g_print("Duration:    %.3f seconds\n",
                             g_timer_elapsed(t, NULL));

    g_timer_destroy(t);
    g_hash_table_unref(h);
    delete _config;
    return diff > 0 ? 1 : 0;
}
