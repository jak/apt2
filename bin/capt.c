#include "config.h"

#include <string.h>
#include <locale.h>

#include <apt/apt.h>
#include <apt/cache-internal.h>

#include <glib/gi18n-lib.h>
#include "capt.h"

static void print_deps(AptPackage *package)
{
    gsize i;
    const gsize n = apt_package_n_dependencies(package);
    AptDependencyType type = APT_DEPENDENCY_INVALID;
    gboolean is_or = FALSE;

    for (i = 0; i < n; i++) {
        AptDependency *dependency = apt_package_get_dependency(package, i);
        AptComparisonType comp = apt_dependency_get_comparison(dependency);

        if (apt_dependency_get_type(dependency) != type) {
            if (type != APT_DEPENDENCY_INVALID)
                g_print("\n");
            type = apt_dependency_get_type(dependency);
            g_print("%s: ", apt_dependency_type_to_string(type));
        } else {
            if (is_or)
                g_print(" | ");
            else
                g_print(", ");
        }
        g_print("%s", apt_dependency_get_name(dependency));

        if (comp != APT_COMPARISON_NONE) {
            g_print(" (%s %s)",
                    apt_comparison_type_to_string(comp),
                    apt_dependency_get_version(dependency));
        }
        is_or = apt_dependency_get_next_is_or(dependency);
        apt_dependency_unref(dependency);
    }
    if (n > 0)
        g_print("\n");
}

static gint do_policy(AptSession *session, int argc, char *argv[])
{
    AptCache *cache;
    AptPolicy *policy;
    gssize pfid;

    AptPackage *pkg;
    AptPackageIter *iter;

    cache = apt_session_get_cache(session);
    policy = apt_session_get_policy(session);

    g_assert(cache != NULL);
    if (argc == 1) {
        g_print(_("Package files:\n"));
        for (pfid = apt_cache_n_packagefiles(cache) - 1; pfid >= 0; pfid--) {
            AptPackageFile *pf = apt_cache_get_package_file(cache, pfid);
            if (apt_package_file_get_archive(pf) == NULL) {
                apt_package_file_unref(pf);
                continue;
            }
            g_print("%4i %s %s/%s\n",
                    apt_policy_get_package_file_priority(policy, pfid),
                    apt_package_file_get_base_uri(pf),
                    apt_package_file_get_archive(pf),
                    apt_package_file_get_component(pf) ?
                    apt_package_file_get_component(pf) : "");

            g_print("     release v=%s,o=%s,a=%s,n=%s,l=%s,c=%s\n",
                    apt_package_file_get_version(pf),
                    apt_package_file_get_origin(pf),
                    apt_package_file_get_archive(pf),
                    apt_package_file_get_codename(pf),
                    apt_package_file_get_label(pf),
                    apt_package_file_get_component(pf));
            if (apt_package_file_get_site(pf) != NULL &&
                *apt_package_file_get_site(pf) != '\0')
                g_print("     origin %s\n", apt_package_file_get_site(pf));

            apt_package_file_unref(pf);
        }
        return 0;
    }

    pkg = apt_policy_get_candidate(policy, argv[1], NULL);
    iter = apt_cache_find(cache, argv[1], APT_CACHE_FIND_BY_BINARY);
    if (iter == NULL) {
        g_printerr(_("W: Unable to locate package %s\n"), argv[1]);
        return 0;
    }
    g_print("%s:\n", argv[1]);
    {
        AptPackageIter *installed = apt_cache_find(cache, argv[1],
                                                   APT_CACHE_FIND_BY_BINARY |
                                                   APT_CACHE_FIND_INSTALLED_ONLY);

        if (installed != NULL) {
            AptPackage *ip = apt_package_iter_next_value(installed);
            g_print(_("  Installed: %s"), apt_package_get_version(ip));
            apt_package_unref(ip);
            while ((ip = apt_package_iter_next_value(installed)) != NULL) {
                g_print(", %s", apt_package_get_version(ip));
                apt_package_unref(ip);
            }
            g_print("\n");
            apt_package_iter_free(installed);
        } else {
            g_print(_("  Installed: (none)\n"));
        }
    }

    g_print(_("  Candidate: %s\n"),
            pkg != NULL ? apt_package_get_version(pkg) : _("(none)"));
    g_print(_("  Version table:\n"));

    if (pkg)
        apt_package_unref(pkg);
    while ((pkg = apt_package_iter_next_value(iter))) {
        AptDescriptionIter *descriptions = apt_package_get_descriptions(pkg);
        AptPackageFile *pf;
        AptDescription *description;

        if (apt_package_get_current_state(pkg) == APT_CURRENT_STATE_INSTALLED)
            g_print(" *** %s %i\n", apt_package_get_version(pkg),
                    apt_policy_get_priority(policy, pkg));
        else
            g_print("     %s %i\n", apt_package_get_version(pkg),
                    apt_policy_get_priority(policy, pkg));

        /* Print the descriptions. */
        while ((description = apt_description_iter_next_value(descriptions))) {
            guint32 pf_id = apt_description_get_package_file_id(description);
            pf = apt_cache_get_package_file(cache, pf_id);

            if (apt_description_get_language(description))
                continue;

            g_print("       %4i %s %s/%s (%s)\n",
                    apt_policy_get_package_file_priority(policy, pf_id),
                    apt_package_file_get_base_uri(pf),
                    apt_package_file_get_archive(pf),
                    apt_package_file_get_component(pf) ?
                    apt_package_file_get_component(pf) : "",
                    apt_package_file_get_flags(pf) & APT_PACKAGE_FILE_IS_TRUSTED
                    ? _("trusted") : _("untrusted"));

            apt_package_file_unref(pf);
            apt_description_unref(description);
        }
        apt_description_iter_free(descriptions);
        apt_package_unref(pkg);
    }

    apt_package_iter_free(iter);
    return 0;
}

static int do_showpkg(AptSession *session, int argc, char *argv[])
{
    int i;
    AptCache *cache = apt_session_get_cache(session);

    for (i = 1; i < argc; i++) {
        AptPackageIter *iter;
        AptPackage *package;

        iter = apt_cache_find(cache, argv[i], APT_CACHE_FIND_BY_BINARY);

        if (iter == NULL) {
            g_printerr(_("W: Unable to locate package %s\n"), argv[i]);
            continue;
        }

        while ((package = apt_package_iter_next_value(iter))) {
            AptDescription *desc;
            AptDescriptionIter *descs = apt_package_get_descriptions(package);
            gchar *s;
            g_print("ID: %d\n", apt_package_get_id(package));
            g_print("Package: %s\n", apt_package_get_name(package));
            g_print("Version: %s\n", apt_package_get_version(package));
            g_print("Architecture: %s\n",
                    apt_package_get_architecture(package));
            g_print("Multi-Arch: %s\n",
                    apt_multi_arch_to_string(apt_package_get_multi_arch
                                             (package)));
            g_print("Size: %s\n", s =
                    g_format_size_for_display(apt_package_get_size(package)));
            g_free(s);
            g_print("Installed-Size: %s\n", s =
                    g_format_size_for_display(apt_package_get_installed_size
                                              (package)));
            g_free(s);
            g_print("Checksum: %s\n", apt_package_get_checksum(package));
            g_print("Priority: %s\n",
                    apt_priority_to_string(apt_package_get_priority(package)));
            g_print("Section: %s\n", apt_package_get_section(package));
            g_print("Source: %s (%s)\n", apt_package_get_source_name(package),
                    apt_package_get_source_version(package));

            print_deps(package);

            g_print("Descriptions:\n");
            while ((desc = apt_description_iter_next_value(descs))) {
                guint8 pfid = apt_description_get_package_file_id(desc);
                AptPackageFile *pf = apt_cache_get_package_file(cache, pfid);
                g_print(" Index: %d (%s)\n", pfid,
                        apt_package_file_get_filename(pf));
                g_print(" Filename: %s\n", apt_description_get_filename(desc));
                g_print(" Description-MD5: %s\n",
                        apt_description_get_md5(desc));
                g_print(" Description-Language: %s\n",
                        apt_description_get_language(desc));
                g_print(" Description-Offset: %" G_GOFFSET_FORMAT "\n",
                        apt_description_get_offset(desc));

                g_print(" .\n");
                apt_description_unref(desc);
                apt_package_file_unref(pf);
            }
            g_print("\n");
            apt_package_unref(package);
            apt_description_iter_free(descs);
        }

        apt_package_iter_free(iter);
    }
    return 0;
}

static int do_show(AptSession *session, int argc, char *argv[])
{
    int i;
    AptCache *cache = apt_session_get_cache(session);

    for (i = 1; i < argc; i++) {
        AptPackageIter *iter;
        AptPackage *package;

        iter = apt_cache_find(cache, argv[i], APT_CACHE_FIND_BY_BINARY);

        if (iter == NULL) {
            g_printerr(_("W: Unable to locate package %s\n"), argv[i]);
            continue;
        }

        while ((package = apt_package_iter_next_value(iter))) {
            AptDescription *desc;
            AptDescriptionIter *descs;

            descs = apt_package_get_descriptions(package);

            while ((desc = apt_description_iter_next_value(descs))) {
                guint8 pfid = apt_description_get_package_file_id(desc);
                AptPackageFile *pf = apt_cache_get_package_file(cache, pfid);
                if (!apt_description_get_language(desc)) {
                    const gchar *filename = apt_package_file_get_filename(pf);
                    goffset offset = apt_description_get_offset(desc);
                    gsize length;
                    AptControlFile *f = apt_control_file_new(filename, NULL);
                    AptControlSection *s = apt_control_file_jump(f, offset);
                    const gchar *sdata = apt_control_section_raw(s, &length);

                    g_print("%.*s\n\n", (int) MIN(INT_MAX, length), sdata);

                    apt_control_section_unref(s);
                    apt_control_file_free(f);
                    apt_package_file_unref(pf);
                    apt_description_unref(desc);
                    break;
                }
                apt_package_file_unref(pf);
                apt_description_unref(desc);
            }
            apt_description_iter_free(descs);
            apt_package_unref(package);
        }

        apt_package_iter_free(iter);
    }
    return 0;
}

static void print_info(const gchar *name, guint n, guint size)
{
    gchar *formatted;
    formatted = g_format_size_for_display(n * size);
    g_print("%-14s %8u (%s)\n", name, n, formatted);
    g_free(formatted);
}

static int do_stats(AptSession *session, int argc, char *argv[])
{
    AptCache *cache = apt_session_get_cache(session);

    (void) argc;
    (void) argv;

    print_info("Header:", sizeof(AptCacheHeader), 1);
    print_info("Characters:",
               cache->header->begin_packages - cache->header->begin_strings, 1);
    print_info("Packages:", cache->header->n_packages, sizeof(AptCachePackage));
    print_info("Provides:", cache->header->n_provides,
               sizeof(AptCacheProvides));
    print_info("Dependencies:", cache->header->n_dependencies,
               sizeof(AptCacheDependency));
    print_info("Descriptions:", cache->header->n_descriptions,
               sizeof(AptCacheDescription));
    print_info("Package files:", cache->header->n_files,
               sizeof(AptCachePackageFile));
    print_info("Group members:", cache->header->n_group_members,
               sizeof(AptCacheGroupMember));
    print_info("Groups:", cache->header->n_groups, sizeof(AptCacheGroup));
    print_info("Total size:", cache->header->end_of_file, 1);

    {
        guint t = 0;            /* sum of chain lengths (total) */
        guint i;
        guint n = 0;            /* number of used buckets */
        guint min = G_MAXUINT;  /* minimum chain length */
        guint max = 0;          /* maximum chain length */
        for (i = 0; i < APT_CACHE_N_BUCKETS; i++) {
            guint k = cache->header->h_buckets[i], c = 0;
            if (k != CINDEX_NONE) {
                n++;
                for (; k != CINDEX_NONE; k = cache->groups[k].h_next)
                    t++, c++;

                min = MIN(min, c);
                max = MAX(max, c);
            }
        }

        g_print("\n");
        g_print("%-14s %5f (%u to %u)\n",
                "Chain length:", 1.0 * t / n, min, max);
        g_print("%-14s %5f (%u of %u)\n", "Load factor:", 1.0 * n / i, n, i);
    }
    return 0;
}

/*
 * apt_configuration_get_any
 * @configuration: An #AptConfiguration.
 * @name: The name of the option including possible suffix.
 * @error: Location to store a #GError
 *
 * Gets the string representation of a value stored in the database for
 * each supported type. This is achieved by adding a suffix to @name
 * which defines the type of the option value. This suffix is separated
 * from the name of the option by a slash. Possible suffixes are 'b'
 * for boolean values, 'd' for directories, 'f' for files, and 'i' for
 * integers. If no such suffix is given, the function just returns the
 * duplicated result of calling apt_configuration_get(). If an invalid
 * suffix is specified, the return value is %NULL and @error is set.
 *
 * When boolean values are selected, the functions returns one of the
 * strings "true" and "false", depending on the evaluation of the value
 * stored at the option.
 *
 * Return value: The string representation of the value, which must be
 *               freed using g_free() when no longer needed; or %NULL
 *               if the type suffix is invalid or the option
 *               does not exist.
 **/
static gchar *apt_configuration_get_any(AptConfiguration *configuration,
                                        const gchar *name, GError **error)
{
    gchar *key;
    gchar *res = NULL;
    gchar type = '\0';
    if (strlen(name) > 2 && name[strlen(name) - 2] == '/') {
        key = g_strndup(name, strlen(name) - 2);
        type = name[strlen(name) - 1];
    } else {
        key = g_strdup(name);
    }

    if (apt_configuration_get(configuration, key) == NULL)
        goto out;
    switch (type) {
    case 'f':
        res = apt_configuration_get_file(configuration, key);
        break;
    case 'd':
        res = apt_configuration_get_file(configuration, key);
        if (res[strlen(res) - 1] != '/') {
            gchar *res_dir = g_strconcat(res, G_DIR_SEPARATOR_S, NULL);
            g_free(res);
            res = res_dir;
        }
        break;
    case 'b':
        if (apt_configuration_get_boolean(configuration, key))
            res = g_strdup("true");
        else
            res = g_strdup("false");
        break;
    case 'i':
        res = g_strdup_printf("%i", apt_configuration_get_int(configuration,
                                                              key));
        break;
    case '\0':
        res = g_strdup(apt_configuration_get(configuration, key));
        break;
    default:
        g_set_error(error, APT_CONFIGURATION_ERROR, 0,
                    _("Invalid type '%c' passed to config-get"), type);
        break;
    }
  out:
    g_free(key);
    return res;
}

static int do_config_get(AptSession *session, int argc, char *argv[])
{
    int i;
    AptConfiguration *configuration = apt_session_get_configuration(session);
    for (i = 1; i < argc; i += 2) {
        GError *error = NULL;
        gchar *v = apt_configuration_get_any(configuration, argv[i + 1],
                                             &error);
        if (v != NULL)
            g_print("%s='%s'\n", argv[i], v);
        else if (error != NULL) {
            g_printerr("E: %s\n", error->message);
            g_error_free(error);
        }
        g_free(v);
    }
    return 0;
}

static int do_config_dump(AptSession *session, int argc, char *argv[])
{
    AptConfiguration *configuration = apt_session_get_configuration(session);
    gchar *dump = apt_configuration_dumps(configuration);

    (void) argc;
    (void) argv;

    g_print("%s\n", dump);
    g_free(dump);
    return 0;
}

static int do_pkgnames(AptSession *session, int argc, char *argv[])
{
    AptCache *cache = apt_session_get_cache(session);
    GHashTable *names = g_hash_table_new(g_str_hash, g_str_equal);
    AptPackageIter *iter = apt_cache_iterator(cache);
    AptPackage *package;

    (void) argc;
    (void) argv;

    while ((package = apt_package_iter_next_value(iter))) {
        if (!g_hash_table_lookup(names, apt_package_get_name(package))) {
            g_print("%s\n", apt_package_get_name(package));
            g_hash_table_insert(names, (gpointer) apt_package_get_name(package),
                                GINT_TO_POINTER(TRUE));
        }
        apt_package_unref(package);
    }
    apt_package_iter_free(iter);
    g_hash_table_unref(names);
    return 0;
}

static int do_madison(AptSession *session, int argc, char *argv[])
{
    int i;
    AptCache *cache = apt_session_get_cache(session);

    for (i = 1; i < argc; i++) {
        AptPackageIter *iter;
        AptPackage *package;

        iter = apt_cache_find(cache, argv[i], APT_CACHE_FIND_BY_BINARY);

        if (iter == NULL) {
            g_printerr(_("W: Unable to locate package %s\n"), argv[i]);
            continue;
        }

        while ((package = apt_package_iter_next_value(iter))) {
            AptDescription *desc;
            AptDescriptionIter *descs;

            descs = apt_package_get_descriptions(package);

            while ((desc = apt_description_iter_next_value(descs))) {
                guint8 pfid = apt_description_get_package_file_id(desc);
                AptPackageFile *pf = apt_cache_get_package_file(cache, pfid);
                if (!apt_description_get_language(desc) &&
                    !(apt_package_file_get_flags(pf) &
                      APT_PACKAGE_FILE_NOT_SOURCE))
                    g_print("%10s | %10s | %s %s/%s | %s\n",
                            apt_package_get_name(package),
                            apt_package_get_version(package),
                            apt_package_file_get_base_uri(pf),
                            apt_package_file_get_archive(pf),
                            apt_package_file_get_component(pf),
                            apt_package_get_architecture(package));
                apt_package_file_unref(pf);
                apt_description_unref(desc);
            }
            apt_description_iter_free(descs);
            apt_package_unref(package);
        }

        apt_package_iter_free(iter);
    }
    return 0;
}

static int do_lock(AptSession *session, int argc, char *argv[])
{
    GError *error = NULL;
    AptSystem *system = apt_session_get_system(session);

    (void) argc;
    (void) argv;

    if (!apt_system_lock(system, &error)) {
        g_printerr("E: %s\n", error->message);
        g_clear_error(&error);
        return 1;
    } else if (!apt_system_unlock(system, &error)) {
        g_printerr("E: %s\n", error->message);
        g_clear_error(&error);
        return 2;
    }
    return 0;
}

static int do_version(AptSession *session, int argc, char *argv[])
{
    (void) session;
    (void) argc;
    (void) argv;
    g_print("capt: %s\n", PACKAGE_VERSION);
    g_print("apt-2.0: %s\n", apt_get_version());
    g_print("glib-2.0: %d.%d.%d\n", glib_major_version, glib_minor_version,
            glib_micro_version);
    return 0;
}

/*
 * TODO: Handle multiple arguments differently
 * TODO: Handle versions correctly.
 * TODO: Handle OR.
 */
static int do_why(AptSession *session, int argc, char *argv[])
{
    int i;
    gboolean is_not = (strcmp(argv[0], "why-not") == 0);
    AptCache *cache = apt_session_get_cache(session);

    for (i = 1; i < argc; i++) {
        AptPackageIter *iter;
        AptPackage *package;

        iter = apt_cache_find(cache, argv[i], APT_CACHE_FIND_BY_DEPENDS |
                              APT_CACHE_FIND_INSTALLED_ONLY);

        while ((package = apt_package_iter_next_value(iter))) {
            gsize di;
            for (di = 0; di < apt_package_n_dependencies(package); di++) {
                AptDependency *dep = apt_package_get_dependency(package, di);
                AptDependencyType type = apt_dependency_get_type(dep);
                if (strcmp(apt_dependency_get_name(dep), argv[i]) == 0 &&
                    (is_not ? apt_dependency_type_rate(type) < 0
                     : apt_dependency_type_rate(type) > 0))
                    g_print("%s %s %s\n", apt_package_get_name(package),
                            apt_dependency_type_to_string(type), argv[i]);
                apt_dependency_unref(dep);
            }
            apt_package_unref(package);
        }
        apt_package_iter_free(iter);
    }
    return 0;
}

typedef struct Command {
    const gchar *command;
    const gchar *description;
    gint (*func) (AptSession *session, int argc, char *argv[]);
} Command;

static const Command commands[] = {
    {"config-dump", N_("Dump configuration"), do_config_dump},
    {"config-get", N_("Get configuration values"), do_config_get},
    {"lock", N_("Test locking"), do_lock},
    {"madison", N_("Show available sources of packages"), do_madison},
    {"pkgnames", N_("Print all package names"), do_pkgnames},
    {"policy", N_("Show the pinning of packages"), do_policy},
    {"show", N_("Show one or more packages (uses more than cache)"), do_show},
    {"showpkg", N_("Show one or more packages (only using cache)"), do_showpkg},
    {"stats", N_("Show cache statistics"), do_stats},
    {"version", N_("Show the version of capt and used libraries"), do_version},
    {"vertest", N_("Test version comparison against APT 0.X"), do_vertest},
    {"why", N_("Show why the package is installed."), do_why},
    {"why-not", N_("Show why the package cannot be installed."), do_why},
    {NULL, NULL, NULL}
};

static gchar *commands_describe()
{
    int i;
    GString *string = g_string_new_len(NULL, G_N_ELEMENTS(commands) * 40);
    g_string_append(string, _("Commands:\n"));

    for (i = 0; commands[i].command != NULL; i++)
        g_string_append_printf(string, "  %-29s %s\n", commands[i].command,
                               _(commands[i].description));

    return g_string_free(string, FALSE);
}

int main(int argc, char *argv[])
{
    gint retcode = 0;
    GError *error = NULL;
    AptSession *session;

    setlocale(LC_ALL, "");

    session = apt_session_new(&error);
    if (session == NULL) {
        if (error) {
            g_printerr("E: %s\n", error->message);
            g_error_free(error);
        }
        return 1;
    }

    {
        GError *error = NULL;
        gchar *description = commands_describe();
        AptConfiguration *config = apt_session_get_configuration(session);
        GOptionContext *options = g_option_context_new("COMMAND [arguments]");
        g_option_context_set_main_group(options,
                                        apt_configuration_get_option_group
                                        (config));
        g_option_context_set_description(options, description);
        g_option_context_set_translation_domain(options, GETTEXT_PACKAGE);

        if (!g_option_context_parse(options, &argc, &argv, &error)) {
            g_printerr("E: %s\n", error->message);
            g_error_free(error);
            retcode = 1;
        }
        if (argc < 2) {
            gchar *help = g_option_context_get_help(options, TRUE, NULL);
            g_printerr("E: %s\n\n", _("No command given"));
            g_printerr("%s", help);
            g_free(help);
            retcode = 1;
        }

        g_free(description);
        g_option_context_free(options);
        if (retcode != 0)
            goto end;
    }

    {
        int i;
        for (i = 0; commands[i].command != NULL; i++)
            if (strcmp(argv[1], commands[i].command) == 0) {
                retcode = commands[i].func(session, argc - 1, argv + 1);
                goto end;
            }
    }

    g_printerr(_("E: Unknown command '%s'\n"), argv[1]);
    retcode = 1;

  end:
    apt_session_unref(session);
    return retcode;
}
